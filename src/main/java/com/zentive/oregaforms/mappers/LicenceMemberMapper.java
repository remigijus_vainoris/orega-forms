package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.LicenceMember;

@Component
public class LicenceMemberMapper implements Mapper<LicenceMember>
{
    @Override
    public LicenceMember fromJson(JsonNode jsonNode)
    {
        LicenceMember licenceMember = new LicenceMember();

        licenceMember.setId(findValueAsString(jsonNode, "licencememberid"));
        licenceMember.setName(findValueAsString(jsonNode, "licencememberid") + " - Licence Member");

        licenceMember.setContactId(findValueAsString(jsonNode, "contact"));
        licenceMember.setEndDate(findValueAsString(jsonNode, "enddate"));
        licenceMember.setStartDate(findValueAsString(jsonNode, "startdate"));
        licenceMember.setLicenceItemId(findValueAsString(jsonNode, "licenceitem"));

        mapCommonValues(jsonNode, licenceMember);

        return licenceMember;
    }
}
