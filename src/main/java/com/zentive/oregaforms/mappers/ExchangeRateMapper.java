package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.ExchangeRate;

@Component
public class ExchangeRateMapper implements Mapper<ExchangeRate>
{
    @Override
    public ExchangeRate fromJson(JsonNode jsonNode)
    {
        ExchangeRate exchangeRate = new ExchangeRate();

        exchangeRate.setId(findValueAsString(jsonNode, "exchangerateid"));
        exchangeRate.setName(findValueAsString(jsonNode, "currencyname"));

        exchangeRate.setCurrencyCode(findValueAsString(jsonNode, "currencycode"));
        exchangeRate.setExchangeRate(findValueAsString(jsonNode, "exchangerate"));

        mapCommonValues(jsonNode, exchangeRate);

        return exchangeRate;
    }
}
