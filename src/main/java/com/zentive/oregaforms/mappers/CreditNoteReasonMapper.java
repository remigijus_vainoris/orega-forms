package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.CreditNoteReason;

@Component
public class CreditNoteReasonMapper implements Mapper<CreditNoteReason>
{
    @Override
    public CreditNoteReason fromJson(JsonNode jsonNode)
    {
        CreditNoteReason creditNoteReason = new CreditNoteReason();

        creditNoteReason.setId(findValueAsString(jsonNode, "creditnotereasonid"));
        creditNoteReason.setName(findValueAsString(jsonNode, "creditnotereasonname"));

        mapCommonValues(jsonNode, creditNoteReason);

        return creditNoteReason;
    }
}
