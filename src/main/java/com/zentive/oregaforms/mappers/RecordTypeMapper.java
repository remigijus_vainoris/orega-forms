package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.RecordType;

@Component
public class RecordTypeMapper implements Mapper<RecordType>
{
    @Override
    public RecordType fromJson(JsonNode jsonNode)
    {
        RecordType recordType = new RecordType();

        recordType.setId(findValueAsString(jsonNode, "recordtypeid"));
        recordType.setName(findValueAsString(jsonNode, "recordtypename"));

        mapCommonValues(jsonNode, recordType);

        return recordType;
    }
}
