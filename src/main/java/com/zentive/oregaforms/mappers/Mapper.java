package com.zentive.oregaforms.mappers;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.BaseEntity;

public interface Mapper<T extends BaseEntity>
{
    T fromJson(JsonNode jsonNode);

    default void mapCommonValues(JsonNode jsonNode, T object)
    {
        object.setCreatedBy(findValueAsString(jsonNode, "createdby"));
        object.setCreationDate(findValueAsString(jsonNode, "creationdate"));
        object.setUpdatedBy(findValueAsString(jsonNode, "updatedby"));
        object.setUpdateDate(findValueAsString(jsonNode, "updatedate"));
    }

    default String findValueAsString(JsonNode node, String name)
    {
        if (node == null)
            return null;

        JsonNode valueNode = node.findValue(name);

        return valueNode != null ? valueNode.asText() : null;
    }
}