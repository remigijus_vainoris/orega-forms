package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.OnlineTransaction;

@Component
public class OnlineTransactionMapper implements Mapper<OnlineTransaction>
{
    @Override
    public OnlineTransaction fromJson(JsonNode jsonNode)
    {
        OnlineTransaction onlineTransaction = new OnlineTransaction();

        onlineTransaction.setId(findValueAsString(jsonNode, "paymenttransactionid"));
        onlineTransaction.setName(findValueAsString(jsonNode, "paymenttransactionname"));

        onlineTransaction.setAmount(findValueAsString(jsonNode, "amount"));
        onlineTransaction.setDescription(findValueAsString(jsonNode, "description"));
        onlineTransaction.setEmail(findValueAsString(jsonNode, "email"));
        onlineTransaction.setFirstName(findValueAsString(jsonNode, "firstname"));
        onlineTransaction.setLastName(findValueAsString(jsonNode, "lastname"));
        onlineTransaction.setStatus(findValueAsString(jsonNode, "status"));
        onlineTransaction.setTelephone(findValueAsString(jsonNode, "telephone"));
        onlineTransaction.setTitle(findValueAsString(jsonNode, "title"));
        onlineTransaction.setTransactionDate(findValueAsString(jsonNode, "transactiondate"));

        mapCommonValues(jsonNode, onlineTransaction);

        return onlineTransaction;
    }
}
