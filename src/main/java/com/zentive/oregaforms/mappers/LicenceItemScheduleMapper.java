package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.LicenceItemSchedule;

@Component
public class LicenceItemScheduleMapper implements Mapper<LicenceItemSchedule>
{
    @Override
    public LicenceItemSchedule fromJson(JsonNode jsonNode)
    {
        LicenceItemSchedule licenceItemSchedule = new LicenceItemSchedule();

        licenceItemSchedule.setId(findValueAsString(jsonNode, "licenceitemscheduleid"));
        licenceItemSchedule.setName(findValueAsString(jsonNode, "licenceitemscheduleid") + " - Licence Schedule");

        licenceItemSchedule.setEndDate(findValueAsString(jsonNode, "enddate"));
        licenceItemSchedule.setForcePrice(findValueAsString(jsonNode, "forceprice"));
        licenceItemSchedule.setFullAmount(findValueAsString(jsonNode, "fullamount"));
        licenceItemSchedule.setInvoiceNow(findValueAsString(jsonNode, "invoicenow"));
        licenceItemSchedule.setInvoiced(findValueAsString(jsonNode, "invoiced"));
        licenceItemSchedule.setLicenceItemId(findValueAsString(jsonNode, "licenceitem"));
        licenceItemSchedule.setListPrice(findValueAsString(jsonNode, "listprice"));
        licenceItemSchedule.setDiscountFromList(findValueAsString(jsonNode, "discountfromlist"));
        licenceItemSchedule.setProRata(findValueAsString(jsonNode, "prorata"));
        licenceItemSchedule.setProductId(findValueAsString(jsonNode, "product"));
        licenceItemSchedule.setProduct(findValueAsString(jsonNode, "outputvalue_product"));
        licenceItemSchedule.setQuantity(findValueAsString(jsonNode, "quantity"));
        licenceItemSchedule.setReferenceText(findValueAsString(jsonNode, "referencetext"));
        licenceItemSchedule.setScheduleId(findValueAsString(jsonNode, "schedule"));
        licenceItemSchedule.setSchedule(findValueAsString(jsonNode, "outputvalue_schedule"));
        licenceItemSchedule.setStartDate(findValueAsString(jsonNode, "startdate"));
        licenceItemSchedule.setUnitPrice(findValueAsString(jsonNode, "unitprice"));

        mapCommonValues(jsonNode, licenceItemSchedule);

        return licenceItemSchedule;
    }
}
