package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.MovementHistory;

@Component
public class MovementHistoryMapper implements Mapper<MovementHistory>
{
    @Override
    public MovementHistory fromJson(JsonNode jsonNode)
    {
        MovementHistory movementHistory = new MovementHistory();

        movementHistory.setId(findValueAsString(jsonNode, "movementid"));
        movementHistory.setName(findValueAsString(jsonNode, "movementid") + " - Movement History");

        movementHistory.setAccountId(findValueAsString(jsonNode, "account"));
        movementHistory.setAccount(findValueAsString(jsonNode, "outputvalue_account"));
        movementHistory.setCheckInOut(findValueAsString(jsonNode, "checkinout"));
        movementHistory.setLocationId(findValueAsString(jsonNode, "location"));
        movementHistory.setLocation(findValueAsString(jsonNode, "outputvalue_location"));
        movementHistory.setMovementTypeId(findValueAsString(jsonNode, "movementtype"));
        movementHistory.setMovementType(findValueAsString(jsonNode, "outputvalue_movementtype"));
        movementHistory.setOffice(findValueAsString(jsonNode, "office"));
        movementHistory.setOfficeSize(findValueAsString(jsonNode, "officesize"));
        movementHistory.setProductId(findValueAsString(jsonNode, "product"));
        movementHistory.setProduct(findValueAsString(jsonNode, "outputvalue_product"));
        movementHistory.setRentalAmount(findValueAsString(jsonNode, "rentalamount"));
        movementHistory.setSalespersonId(findValueAsString(jsonNode, "salesperson"));
        movementHistory.setSalesperson(findValueAsString(jsonNode, "outputvalue_salesperson"));
        movementHistory.setStartDate(findValueAsString(jsonNode, "startdate"));
        movementHistory.setTerm(findValueAsString(jsonNode, "term"));
        movementHistory.setWorkstations(findValueAsString(jsonNode, "workstations"));

        mapCommonValues(jsonNode, movementHistory);

        return movementHistory;
    }
}
