package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.ScheduleType;

@Component
public class ScheduleTypeMapper implements Mapper<ScheduleType>
{
    @Override
    public ScheduleType fromJson(JsonNode jsonNode)
    {
        ScheduleType scheduleType = new ScheduleType();

        scheduleType.setId(findValueAsString(jsonNode, "scheduletypeid"));
        scheduleType.setName(findValueAsString(jsonNode, "scheduletypename"));

        scheduleType.setInterval(findValueAsString(jsonNode, "interval"));
        scheduleType.setIntervalNumber(findValueAsString(jsonNode, "intervalnumber"));
        scheduleType.setNumberOfDays(findValueAsString(jsonNode, "noofdays"));
        scheduleType.setNumberOfMonths(findValueAsString(jsonNode, "noofmonths"));
        scheduleType.setSortOrder(findValueAsString(jsonNode, "sortorder"));

        mapCommonValues(jsonNode, scheduleType);

        return scheduleType;
    }
}
