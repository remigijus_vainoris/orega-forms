package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.InvoiceLine;

@Component
public class InvoiceLineMapper implements Mapper<InvoiceLine>
{
    @Override
    public InvoiceLine fromJson(JsonNode jsonNode)
    {
        InvoiceLine invoiceLine = new InvoiceLine();

        invoiceLine.setId(findValueAsString(jsonNode, "draftinvoicelineid"));
        invoiceLine.setName(findValueAsString(jsonNode, "draftinvoicelineid") + " - Invoice Line");

        invoiceLine.setAccountId(findValueAsString(jsonNode, "accountsid"));
        invoiceLine.setBundledProduct(findValueAsString(jsonNode, "bundledproduct"));
        invoiceLine.setDiscountFromList(findValueAsString(jsonNode, "discountfromlist"));
        invoiceLine.setFrom(findValueAsString(jsonNode, "from"));
        invoiceLine.setGlCode(findValueAsString(jsonNode, "glcode"));
        invoiceLine.setInvoiceId(findValueAsString(jsonNode, "invoice"));
        invoiceLine.setListPrice(findValueAsString(jsonNode, "listprice"));
        invoiceLine.setLocationId(findValueAsString(jsonNode, "location"));
        invoiceLine.setLocation(findValueAsString(jsonNode, "outputvalue_location"));
        invoiceLine.setMarkup(findValueAsString(jsonNode, "markup"));
        invoiceLine.setMarkupType(findValueAsString(jsonNode, "markuptype"));
        invoiceLine.setOrder(findValueAsString(jsonNode, "order"));
        invoiceLine.setOriginalBundle(findValueAsString(jsonNode, "originalbundle"));
        invoiceLine.setProductId(findValueAsString(jsonNode, "product"));
        invoiceLine.setProduct(findValueAsString(jsonNode, "outputvalue_product"));
        invoiceLine.setProductGroupId(findValueAsString(jsonNode, "productgroup"));
        invoiceLine.setProductGroup(findValueAsString(jsonNode, "outputvalue_productgroup"));
        invoiceLine.setQuantity(findValueAsString(jsonNode, "quantity"));
        invoiceLine.setReferenceText(findValueAsString(jsonNode, "referencetext"));
        invoiceLine.setTaxId(findValueAsString(jsonNode, "tax"));
        invoiceLine.setTax(findValueAsString(jsonNode, "outputvalue_taxname"));
        invoiceLine.setTaxCode(findValueAsString(jsonNode, "taxcode"));
        invoiceLine.setTaxRate(findValueAsString(jsonNode, "taxrate"));
        invoiceLine.setTo(findValueAsString(jsonNode, "to"));

        mapCommonValues(jsonNode, invoiceLine);

        return invoiceLine;
    }
}
