package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.ContactType;

@Component
public class ContactTypeMapper implements Mapper<ContactType>
{
    @Override
    public ContactType fromJson(JsonNode jsonNode)
    {
        ContactType contactType = new ContactType();

        contactType.setId(findValueAsString(jsonNode, "contacttypeid"));
        contactType.setName(findValueAsString(jsonNode, "contacttypename"));

        mapCommonValues(jsonNode, contactType);

        return contactType;
    }
}
