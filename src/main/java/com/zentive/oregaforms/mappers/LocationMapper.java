package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Location;

@Component
public class LocationMapper implements Mapper<Location>
{
    @Override
    public Location fromJson(JsonNode jsonNode)
    {
        Location location = new Location();

        location.setId(findValueAsString(jsonNode, "locationid"));
        location.setName(findValueAsString(jsonNode, "locationname"));

        location.setOnlineAlias(findValueAsString(jsonNode, "onlinealias"));
        location.setEmail(findValueAsString(jsonNode, "email"));
        location.setMap(findValueAsString(jsonNode, "map"));
        location.setCurrency(findValueAsString(jsonNode, "currency"));

        mapCommonValues(jsonNode, location);

        return location;
    }
}