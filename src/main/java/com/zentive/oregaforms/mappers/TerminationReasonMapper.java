package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.TerminationReason;

@Component
public class TerminationReasonMapper implements Mapper<TerminationReason>
{
    @Override
    public TerminationReason fromJson(JsonNode jsonNode)
    {
        TerminationReason terminationReason = new TerminationReason();

        terminationReason.setId(findValueAsString(jsonNode, "terminationresonsid"));
        terminationReason.setName(findValueAsString(jsonNode, "terminationreason"));

        terminationReason.setReason(findValueAsString(jsonNode, "terminationreason"));

        mapCommonValues(jsonNode, terminationReason);

        return terminationReason;
    }
}
