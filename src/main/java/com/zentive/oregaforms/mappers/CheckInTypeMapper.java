package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.CheckInType;

@Component
public class CheckInTypeMapper implements Mapper<CheckInType>
{
    @Override
    public CheckInType fromJson(JsonNode jsonNode)
    {
        CheckInType checkInType = new CheckInType();

        checkInType.setId(findValueAsString(jsonNode, "checkintypesid"));
        checkInType.setName(findValueAsString(jsonNode, "checkintype"));

        mapCommonValues(jsonNode, checkInType);

        return checkInType;
    }
}
