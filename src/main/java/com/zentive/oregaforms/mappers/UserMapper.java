package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.User;

@Component
public class UserMapper implements Mapper<User>
{
    @Override
    public User fromJson(JsonNode jsonNode)
    {
        User user = new User();

        user.setId(findValueAsString(jsonNode, "userid"));
        user.setName(findValueAsString(jsonNode, "fullname"));

        user.setApprovalPricePercent(findValueAsString(jsonNode, "approvalpricepercent"));
        user.setCanApproveLicences(findValueAsString(jsonNode, "canapprovelicences"));
        user.setCanCounterSign(findValueAsString(jsonNode, "cancountersign"));
        user.setCanSeeProspects(findValueAsString(jsonNode, "canseeprospects"));
        user.setChecklistDate(findValueAsString(jsonNode, "checklistdate"));
        user.setChecklistTasks(findValueAsString(jsonNode, "checklisttasks"));
        user.setChecklistTasksComplete(findValueAsString(jsonNode, "checklisttaskscomplete"));
        user.setCountry(findValueAsString(jsonNode, "country"));
        user.setCounty(findValueAsString(jsonNode, "county"));
        user.setCurrencyId(findValueAsString(jsonNode, "currencyid"));
        user.setCurrencyCode(findValueAsString(jsonNode, "currencycode"));
        user.setCurrentLocation(findValueAsString(jsonNode, "currentlocation"));
        user.setDiscontinued(findValueAsString(jsonNode, "discontinued"));
        user.setEmailAddress(findValueAsString(jsonNode, "emailaddress"));
        user.setFirstName(findValueAsString(jsonNode, "firstname"));
        user.setFullName(findValueAsString(jsonNode, "fullname"));
        user.setJeffEnabled(findValueAsString(jsonNode, "jeffenabled"));
        user.setJeffUsername(findValueAsString(jsonNode, "jeffusername"));
        user.setJobFunction(findValueAsString(jsonNode, "jobfunction"));
        user.setLastName(findValueAsString(jsonNode, "lastname"));
        user.setLocationIds(findValueAsString(jsonNode, "locationids"));
        user.setLocations(findValueAsString(jsonNode, "loactions"));
        user.setNtAccount(findValueAsString(jsonNode, "ntaccount"));
        user.setPictureURL(findValueAsString(jsonNode, "pictureurl"));
        user.setUserAccessLevel(findValueAsString(jsonNode, "useraccesslevel"));
        user.setShowChecklist(findValueAsString(jsonNode, "showchecklist"));
        user.setTier3(findValueAsString(jsonNode, "tier3"));
        user.setTitle(findValueAsString(jsonNode, "title"));
        user.setTown(findValueAsString(jsonNode, "town"));
        user.setUserGroup(findValueAsString(jsonNode, "usergroup"));
        user.setUsername(findValueAsString(jsonNode, "username"));

        mapCommonValues(jsonNode, user);

        return user;
    }
}
