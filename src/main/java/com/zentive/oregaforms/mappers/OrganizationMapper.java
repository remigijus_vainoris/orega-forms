package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Organization;

@Component
public class OrganizationMapper implements Mapper<Organization>
{
    @Override
    public Organization fromJson(JsonNode jsonNode)
    {
        Organization organization = new Organization();

        organization.setId(findValueAsString(jsonNode, "organizationid"));
        organization.setName(findValueAsString(jsonNode, "organizationname"));

        organization.setAba(findValueAsString(jsonNode, "aba"));
        organization.setAchAccountName(findValueAsString(jsonNode, "achaccountname"));
        organization.setAchAccountNo(findValueAsString(jsonNode, "achaccountno"));
        organization.setAchBankAddress(findValueAsString(jsonNode, "achbankaddress"));
        organization.setAchBankName(findValueAsString(jsonNode, "achbankname"));
        organization.setVirtualAchURL(findValueAsString(jsonNode, "virtualachurl"));
        organization.setAddressLine1(findValueAsString(jsonNode, "addressline1"));
        organization.setLglAddress1(findValueAsString(jsonNode, "lgladdress_1"));
        organization.setAddressLine2(findValueAsString(jsonNode, "addressline2"));
        organization.setLglAddress3(findValueAsString(jsonNode, "lgladdress_3"));
        organization.setAddressLine3(findValueAsString(jsonNode, "addressline3"));
        organization.setAccountName(findValueAsString(jsonNode, "accountname"));
        organization.setAccountNo(findValueAsString(jsonNode, "accountno"));
        organization.setBankAddress(findValueAsString(jsonNode, "bankaddress"));
        organization.setBankName(findValueAsString(jsonNode, "bankname"));
        organization.setSortCode(findValueAsString(jsonNode, "sortcode"));
        organization.setCompanyNo(findValueAsString(jsonNode, "companyno"));
        organization.setLglCountry(findValueAsString(jsonNode, "lglcountry"));
        organization.setLglCounty(findValueAsString(jsonNode, "lglcounty"));
        organization.setVirtualCreditCardURL(findValueAsString(jsonNode, "virtualcreditcardurl"));
        organization.setDdFifthDigit(findValueAsString(jsonNode, "ddfifthdigit"));
        organization.setDdFirstDigit(findValueAsString(jsonNode, "ddfirstdigit"));
        organization.setDdForthDigit(findValueAsString(jsonNode, "ddforthdigit"));
        organization.setDdOriginator1(findValueAsString(jsonNode, "ddoriginator1"));
        organization.setDdOriginator2(findValueAsString(jsonNode, "ddoriginator2"));
        organization.setDdOriginator3(findValueAsString(jsonNode, "ddoriginator3"));
        organization.setDdSecondDigit(findValueAsString(jsonNode, "ddseconddigit"));
        organization.setDdSixthDigit(findValueAsString(jsonNode, "ddsixthdigit"));
        organization.setDdThirdDigit(findValueAsString(jsonNode, "ddthirddigit"));
        organization.setEarliestInvoiceDate(findValueAsString(jsonNode, "earliestinvoicedate"));
        organization.setExportFileTag(findValueAsString(jsonNode, "exportfiletag"));
        organization.setFullAddress(findValueAsString(jsonNode, "fulladdress"));
        organization.setIban(findValueAsString(jsonNode, "iban"));
        organization.setLogoURL(findValueAsString(jsonNode, "logourl"));
        organization.setMerchantId(findValueAsString(jsonNode, "merchantid"));
        organization.setLegalEntityCode(findValueAsString(jsonNode, "legalentitycode"));
        organization.setPaymentProcessor(findValueAsString(jsonNode, "paymentprocessor"));
        organization.setLglPostcode(findValueAsString(jsonNode, "lglpostcode"));
        organization.setProcessorCurrencyCode(findValueAsString(jsonNode, "processorcurrencycode"));
        organization.setEnableRecurringPayments(findValueAsString(jsonNode, "enablerecurringpayments"));
        organization.setProcessorSignature(findValueAsString(jsonNode, "processorsignature"));
        organization.setProcessorUseSandbox(findValueAsString(jsonNode, "processorusesandbox"));
        organization.setProcessorUserId(findValueAsString(jsonNode, "processoruserid"));
        organization.setRecordType(findValueAsString(jsonNode, "recordtype"));
        organization.setSingleLineAddress(findValueAsString(jsonNode, "singlelineaddress"));
        organization.setSwiftCode(findValueAsString(jsonNode, "swiftcode"));
        organization.setVatNumber(findValueAsString(jsonNode, "vatnumber"));
        organization.setLglTown(findValueAsString(jsonNode, "lgltown"));

        mapCommonValues(jsonNode, organization);

        return organization;
    }
}
