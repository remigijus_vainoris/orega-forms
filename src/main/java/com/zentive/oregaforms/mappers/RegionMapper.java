package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Region;

@Component
public class RegionMapper implements Mapper<Region>
{
    @Override
    public Region fromJson(JsonNode jsonNode)
    {
        Region region = new Region();

        region.setId(findValueAsString(jsonNode, "regionid"));
        region.setName(findValueAsString(jsonNode, "regionname"));

        mapCommonValues(jsonNode, region);

        return region;
    }
}
