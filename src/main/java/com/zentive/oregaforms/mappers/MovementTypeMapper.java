package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.MovementType;

@Component
public class MovementTypeMapper implements Mapper<MovementType>
{
    @Override
    public MovementType fromJson(JsonNode jsonNode)
    {
        MovementType movementType = new MovementType();

        movementType.setId(findValueAsString(jsonNode, "movementtypeid"));
        movementType.setName(findValueAsString(jsonNode, "movementtypename"));

        mapCommonValues(jsonNode, movementType);

        return movementType;
    }
}
