package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Town;

@Component
public class TownMapper implements Mapper<Town>
{
    @Override
    public Town fromJson(JsonNode jsonNode)
    {
        Town town = new Town();

        town.setId(findValueAsString(jsonNode, "townsid"));
        town.setName(findValueAsString(jsonNode, "town"));

        mapCommonValues(jsonNode, town);

        return town;
    }
}
