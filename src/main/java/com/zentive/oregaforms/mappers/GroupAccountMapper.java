package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.GroupAccount;

@Component
public class GroupAccountMapper implements Mapper<GroupAccount>
{
    @Override
    public GroupAccount fromJson(JsonNode jsonNode)
    {
        GroupAccount groupAccount = new GroupAccount();

        groupAccount.setId(findValueAsString(jsonNode, "groupaccountid"));
        groupAccount.setName(findValueAsString(jsonNode, "groupaccountname"));

        mapCommonValues(jsonNode, groupAccount);

        return groupAccount;
    }
}
