package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.AccountCode;

@Component
public class AccountCodeMapper implements Mapper<AccountCode>
{
    @Override
    public AccountCode fromJson(JsonNode jsonNode)
    {
        AccountCode accountCode = new AccountCode();

        accountCode.setId(findValueAsString(jsonNode, "accountcodeid"));
        accountCode.setName(findValueAsString(jsonNode, "accountcode"));

        accountCode.setAccountId(findValueAsString(jsonNode, "account"));
        accountCode.setAccount(findValueAsString(jsonNode, "outputvalue_account"));
        accountCode.setAccountCode(findValueAsString(jsonNode, "accountcode"));
        accountCode.setDeviceImport(findValueAsString(jsonNode, "deviceimport"));

        mapCommonValues(jsonNode, accountCode);

        return accountCode;
    }
}
