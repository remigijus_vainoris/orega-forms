package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.LicenceItem;

@Component
public class LicenceItemMapper implements Mapper<LicenceItem>
{
    @Override
    public LicenceItem fromJson(JsonNode jsonNode)
    {
        LicenceItem licenceItem = new LicenceItem();

        licenceItem.setId(findValueAsString(jsonNode, "licenceitemid"));
        licenceItem.setName(findValueAsString(jsonNode, "licenceitemid") + " - Licence Item");

        licenceItem.setAutoCharge(findValueAsString(jsonNode, "autocharge"));
        licenceItem.setAutoEscalate(findValueAsString(jsonNode, "autoescalate"));
        licenceItem.setBillinAdvance(findValueAsString(jsonNode, "billinadvance"));
        licenceItem.setAnniversary(findValueAsString(jsonNode, "anniversary"));
        licenceItem.setBundledProduct(findValueAsString(jsonNode, "bundledproduct"));
        licenceItem.setMandatorySqftCalc(findValueAsString(jsonNode, "mandatorysqftcalc"));
        licenceItem.setMandatoryToVirtualClients(findValueAsString(jsonNode, "mandatorytovirtualclients"));
        licenceItem.setDeposit(findValueAsString(jsonNode, "deposit"));
        licenceItem.setDepositHeld(findValueAsString(jsonNode, "depositheld"));
        licenceItem.setDepositReturned(findValueAsString(jsonNode, "depositreturned"));
        licenceItem.setDiscountPercent(findValueAsString(jsonNode, "discountpercent"));
        licenceItem.setDiscountAmount(findValueAsString(jsonNode, "discountamount"));
        licenceItem.setEndDate(findValueAsString(jsonNode, "enddate"));
        licenceItem.setScheduleId(findValueAsString(jsonNode, "schedule_id"));
        licenceItem.setSchedule(findValueAsString(jsonNode, "outputvalue_schedule_id"));
        licenceItem.setLicenceId(findValueAsString(jsonNode, "licence"));
        licenceItem.setLicence(findValueAsString(jsonNode, "outputvalue_licence"));
        licenceItem.setListPrice(findValueAsString(jsonNode, "listprice"));
        licenceItem.setListPriceTotal(findValueAsString(jsonNode, "listpricetotal"));
        licenceItem.setMandatoryCharge(findValueAsString(jsonNode, "mandatorycharge"));
        licenceItem.setMandatoryChargeType(findValueAsString(jsonNode, "mandatorychargetype"));
        licenceItem.setOfficeSize(findValueAsString(jsonNode, "officesize"));
        licenceItem.setMandatoryChargePercentOfAll(findValueAsString(jsonNode, "mandatorychargepercentofall"));
        licenceItem.setMandatoryPercentOfRent(findValueAsString(jsonNode, "mandatorypercentofrent"));
        licenceItem.setProRata2ndPayment(findValueAsString(jsonNode, "prorata2ndpayment"));
        licenceItem.setProductId(findValueAsString(jsonNode, "product"));
        licenceItem.setProduct(findValueAsString(jsonNode, "outputvalue_product"));
        licenceItem.setQuantity(findValueAsString(jsonNode, "quantity"));
        licenceItem.setReferenceText(findValueAsString(jsonNode, "referencetext"));
        licenceItem.setRetainerProductId(findValueAsString(jsonNode, "retainerproduct"));
        licenceItem.setRetainerProduct(findValueAsString(jsonNode, "outputvalue_retainerproduct"));
        licenceItem.setRetainerReferenceText(findValueAsString(jsonNode, "retainerreferencetext"));
        licenceItem.setRetainerTaxId(findValueAsString(jsonNode, "retainertax"));
        licenceItem.setRetainerTax(findValueAsString(jsonNode, "outputvalue_retainertax"));
        licenceItem.setStartDate(findValueAsString(jsonNode, "startdate"));
        licenceItem.setSubTotal(findValueAsString(jsonNode, "subtotal"));
        licenceItem.setTaxId(findValueAsString(jsonNode, "tax"));
        licenceItem.setTax(findValueAsString(jsonNode, "outputvalue_tax"));
        licenceItem.setTerminated(findValueAsString(jsonNode, "terminated"));
        licenceItem.setTotal(findValueAsString(jsonNode, "total"));
        licenceItem.setTotalDeposit(findValueAsString(jsonNode, "totaldeposit"));
        licenceItem.setType(findValueAsString(jsonNode, "type"));
        licenceItem.setUnitPrice(findValueAsString(jsonNode, "unitprice"));
        licenceItem.setCanEditMandatoryCharge(findValueAsString(jsonNode, "caneditmandatorycharge"));
        licenceItem.setWorkstations(findValueAsString(jsonNode, "workstations"));

        mapCommonValues(jsonNode, licenceItem);

        return licenceItem;
    }
}
