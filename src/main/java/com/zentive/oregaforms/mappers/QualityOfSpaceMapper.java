package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.QualityOfSpace;

@Component
public class QualityOfSpaceMapper implements Mapper<QualityOfSpace>
{
    @Override
    public QualityOfSpace fromJson(JsonNode jsonNode)
    {
        QualityOfSpace qualityOfSpace = new QualityOfSpace();

        qualityOfSpace.setId(findValueAsString(jsonNode, "qualityofspaceid"));
        qualityOfSpace.setName(findValueAsString(jsonNode, "qualityofspacename"));

        mapCommonValues(jsonNode, qualityOfSpace);

        return qualityOfSpace;
    }
}
