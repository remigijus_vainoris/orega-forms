package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Invoice;

@Component
public class InvoiceMapper implements Mapper<Invoice>
{
    @Override
    public Invoice fromJson(JsonNode jsonNode)
    {
        Invoice invoice = new Invoice();

        invoice.setId(findValueAsString(jsonNode, "invoiceid"));
        invoice.setName(findValueAsString(jsonNode, "invoicenumber"));

        invoice.setAccountId(findValueAsString(jsonNode, "accountname"));
        invoice.setAccount(findValueAsString(jsonNode, "outputvalue_accountname"));
        invoice.setApproved(findValueAsString(jsonNode, "approved"));
        invoice.setBfwd(findValueAsString(jsonNode, "bfwd"));
        invoice.setClientTypeId(findValueAsString(jsonNode, "clienttype"));
        invoice.setClientType(findValueAsString(jsonNode, "outputvalue_clienttype"));
        invoice.setInvoiceDescription(findValueAsString(jsonNode, "invoicedescription"));
        invoice.setDueDate(findValueAsString(jsonNode, "duedate"));
        invoice.setEmailInvoices(findValueAsString(jsonNode, "emailinvoices"));
        invoice.setInvoiceEmailAddress(findValueAsString(jsonNode, "invoiceemailaddress"));
        invoice.setEmailAddress(findValueAsString(jsonNode, "emailaddress"));
        invoice.setEndingBalance(findValueAsString(jsonNode, "endingbalance"));
        invoice.setGross(findValueAsString(jsonNode, "gross"));
        invoice.setInvoiceDate(findValueAsString(jsonNode, "invoicedate"));
        invoice.setInvoiceNumber(findValueAsString(jsonNode, "invoicenumber"));
        invoice.setInvoiceTemplateId(findValueAsString(jsonNode, "invoicetemplate"));
        invoice.setInvoiceTemplate(findValueAsString(jsonNode, "outputvalue_invoicetemplate"));
        invoice.setInvoiceText(findValueAsString(jsonNode, "invoicetext"));
        invoice.setInvoiceText1(findValueAsString(jsonNode, "invoicetext1"));
        invoice.setLocationId(findValueAsString(jsonNode, "location"));
        invoice.setLocation(findValueAsString(jsonNode, "outputvalue_location"));
        invoice.setNet(findValueAsString(jsonNode, "net"));
        invoice.setOrganizationId(findValueAsString(jsonNode, "organization"));
        invoice.setOrganization(findValueAsString(jsonNode, "outputvalue_organization"));
        invoice.setPaid(findValueAsString(jsonNode, "paid"));
        invoice.setPaymentsCredits(findValueAsString(jsonNode, "paymentscredits"));
        invoice.setRecordType(findValueAsString(jsonNode, "recordtype"));
        invoice.setStatus(findValueAsString(jsonNode, "status"));
        invoice.setTax(findValueAsString(jsonNode, "tax"));

        mapCommonValues(jsonNode, invoice);

        return invoice;
    }
}
