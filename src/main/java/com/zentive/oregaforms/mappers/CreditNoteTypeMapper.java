package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.CreditNoteType;

@Component
public class CreditNoteTypeMapper implements Mapper<CreditNoteType>
{
    @Override
    public CreditNoteType fromJson(JsonNode jsonNode)
    {
        CreditNoteType creditNoteType = new CreditNoteType();

        creditNoteType.setId(findValueAsString(jsonNode, "creditnotetypeid"));
        creditNoteType.setName(findValueAsString(jsonNode, "creditnotetypename"));

        mapCommonValues(jsonNode, creditNoteType);

        return creditNoteType;
    }
}
