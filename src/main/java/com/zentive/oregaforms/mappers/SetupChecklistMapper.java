package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.SetupChecklist;

@Component
public class SetupChecklistMapper implements Mapper<SetupChecklist>
{
    @Override
    public SetupChecklist fromJson(JsonNode jsonNode)
    {
        SetupChecklist setupChecklist = new SetupChecklist();

        setupChecklist.setId(findValueAsString(jsonNode, "setupchecklistid"));
        setupChecklist.setName(findValueAsString(jsonNode, "editbussontext"));

        setupChecklist.setChecklistGroup(findValueAsString(jsonNode, "checklistgroup"));
        setupChecklist.setComplete(findValueAsString(jsonNode, "complete"));
        setupChecklist.setDescription(findValueAsString(jsonNode, "description"));
        setupChecklist.setEditButtonText(findValueAsString(jsonNode, "editbuttontext"));
        setupChecklist.setIconUrl(findValueAsString(jsonNode, "iconurl"));
        setupChecklist.setLinkTo(findValueAsString(jsonNode, "linkto"));
        setupChecklist.setMoreInfo(findValueAsString(jsonNode, "moreinfo"));
        setupChecklist.setNotRightNow(findValueAsString(jsonNode, "notrightnow"));
        setupChecklist.setSortOrder(findValueAsString(jsonNode, "sortorder"));
        setupChecklist.setSpa(findValueAsString(jsonNode, "spa"));
        setupChecklist.setSubject(findValueAsString(jsonNode, "subject"));
        setupChecklist.setSubtitle(findValueAsString(jsonNode, "subtitle"));

        mapCommonValues(jsonNode, setupChecklist);

        return setupChecklist;
    }
}
