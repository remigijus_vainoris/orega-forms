package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.CreditNoteLine;

@Component
public class CreditNoteLineMapper implements Mapper<CreditNoteLine>
{
    @Override
    public CreditNoteLine fromJson(JsonNode jsonNode)
    {
        CreditNoteLine creditNoteLine = new CreditNoteLine();

        creditNoteLine.setId(findValueAsString(jsonNode, "creditnotelineid"));
        creditNoteLine.setName(findValueAsString(jsonNode, "creditnotelineid") + " - Credit Note Line");

        creditNoteLine.setBundledProduct(findValueAsString(jsonNode, "bundledproduct"));
        creditNoteLine.setCreditNoteId(findValueAsString(jsonNode, "creditnote"));
        creditNoteLine.setDiscountFromList(findValueAsString(jsonNode, "discountfromlist"));
        creditNoteLine.setFrom(findValueAsString(jsonNode, "from"));
        creditNoteLine.setListPrice(findValueAsString(jsonNode, "listprice"));
        creditNoteLine.setMarkup(findValueAsString(jsonNode, "markup"));
        creditNoteLine.setMarkupType(findValueAsString(jsonNode, "markuptype"));
        creditNoteLine.setOrder(findValueAsString(jsonNode, "order"));
        creditNoteLine.setProductId(findValueAsString(jsonNode, "product"));
        creditNoteLine.setProduct(findValueAsString(jsonNode, "outputvalue_product"));
        creditNoteLine.setProductGroupId(findValueAsString(jsonNode, "productgroup"));
        creditNoteLine.setProductGroup(findValueAsString(jsonNode, "outputvalue_productgroup"));
        creditNoteLine.setQuantity(findValueAsString(jsonNode, "quantity"));
        creditNoteLine.setReferenceText(findValueAsString(jsonNode, "referencetext"));
        creditNoteLine.setTaxId(findValueAsString(jsonNode, "tax"));
        creditNoteLine.setTaxRate(findValueAsString(jsonNode, "taxrate"));
        creditNoteLine.setTo(findValueAsString(jsonNode, "to"));
        creditNoteLine.setUnitPrice(findValueAsString(jsonNode, "unitprice"));

        mapCommonValues(jsonNode, creditNoteLine);

        return creditNoteLine;
    }
}
