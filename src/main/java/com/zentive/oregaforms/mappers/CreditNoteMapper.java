package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.CreditNote;

@Component
public class CreditNoteMapper implements Mapper<CreditNote>
{
    @Override
    public CreditNote fromJson(JsonNode jsonNode)
    {
        CreditNote creditNote = new CreditNote();

        creditNote.setId(findValueAsString(jsonNode, "creditnoteid"));
        creditNote.setName(findValueAsString(jsonNode, "creditnotenumber"));

        creditNote.setAccountId(findValueAsString(jsonNode, "accountname"));
        creditNote.setApproved(findValueAsString(jsonNode, "approved"));
        creditNote.setComment(findValueAsString(jsonNode, "creditnotecomment"));
        creditNote.setDate(findValueAsString(jsonNode, "creditnotedate"));
        creditNote.setNumber(findValueAsString(jsonNode, "creditnotenumber"));
        creditNote.setReasonId(findValueAsString(jsonNode, "creditnotereason"));
        creditNote.setText(findValueAsString(jsonNode, "creditnotetext"));
        creditNote.setTypeId(findValueAsString(jsonNode, "creditnotetype"));
        creditNote.setGross(findValueAsString(jsonNode, "gross"));
        creditNote.setLocationId(findValueAsString(jsonNode, "location"));
        creditNote.setNet(findValueAsString(jsonNode, "net"));
        creditNote.setStatus(findValueAsString(jsonNode, "status"));
        creditNote.setTax(findValueAsString(jsonNode, "tax"));

        mapCommonValues(jsonNode, creditNote);

        return creditNote;
    }
}
