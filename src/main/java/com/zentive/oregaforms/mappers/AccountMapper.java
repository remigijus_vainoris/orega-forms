package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Account;

@Component
public class AccountMapper implements Mapper<Account>
{
    @Override
    public Account fromJson(JsonNode jsonNode)
    {
        Account account = new Account();

        account.setId(findValueAsString(jsonNode, "accountid"));
        account.setName(findValueAsString(jsonNode, "clientname"));

        account.setClientName(findValueAsString(jsonNode, "clientname"));
        account.setAccountTypeId(findValueAsString(jsonNode, "accounttype_id"));
        account.setAccountType(findValueAsString(jsonNode, "outputvalue_accounttype_id"));
        account.setBalance(findValueAsString(jsonNode, "balance"));
        account.setBroker(findValueAsString(jsonNode, "broker"));
        account.setAgentContact(findValueAsString(jsonNode, "agentcontact"));
        account.setBrokerFirstName(findValueAsString(jsonNode, "brokerfirstname"));
        account.setBrokerLastName(findValueAsString(jsonNode, "brokerlastname"));
        account.setCampaignId(findValueAsString(jsonNode, "campaign_id"));
        account.setChargeVAT(findValueAsString(jsonNode, "chargevat"));
        account.setClientTypeId(findValueAsString(jsonNode, "clienttype"));
        account.setClientType(findValueAsString(jsonNode, "outputvalue_clienttype"));
        account.setCompanyLogoURL(findValueAsString(jsonNode, "companylogourl"));
        account.setEarliestTransactionDateAllowed(findValueAsString(jsonNode, "earliesttransactiondateallowed"));
        account.setEmailInvoice(findValueAsString(jsonNode, "emailinvoice"));
        account.setEndDate(findValueAsString(jsonNode, "enddate"));
        account.setFullAddress(findValueAsString(jsonNode, "fulladdress"));
        account.setHasActiveLicence(findValueAsString(jsonNode, "hasactivelicence"));
        account.setIncludeBfwd(findValueAsString(jsonNode, "includebfwd"));
        account.setShowcAllDetails(findValueAsString(jsonNode, "showcalldetails"));
        account.setIncludeItemisedBill(findValueAsString(jsonNode, "includeitemisedbill"));
        account.setIncludeStatement(findValueAsString(jsonNode, "includestatement"));
        account.setIsExternal(findValueAsString(jsonNode, "isexternal"));
        account.setIsResidential(findValueAsString(jsonNode, "isresidential"));
        account.setIsVirtual(findValueAsString(jsonNode, "isvirtual"));
        account.setLeaddadiAgent(findValueAsString(jsonNode, "leaddadiagent"));
        account.setLeaddadiId(findValueAsString(jsonNode, "leaddadiid"));
        account.setLocation(findValueAsString(jsonNode, "location"));
        account.setMeetingPackageId(findValueAsString(jsonNode, "meetingpackageid"));
        account.setOnAccount(findValueAsString(jsonNode, "onaccount"));
        account.setOnlineBill(findValueAsString(jsonNode, "onlinebill"));
        account.setOrganizationId(findValueAsString(jsonNode, "organization"));
        account.setOrganization(findValueAsString(jsonNode, "outputvalue_organization"));
        account.setOverrideDiscount(findValueAsString(jsonNode, "overridediscount"));
        account.setUserId(findValueAsString(jsonNode, "user_id"));
        account.setPrintInvoice(findValueAsString(jsonNode, "printinvoice"));
        account.setRecordTypeId(findValueAsString(jsonNode, "recordtype"));
        account.setRecordType(findValueAsString(jsonNode, "outputvalue_recordtype"));
        account.setRegisteredAddressSingleLine(findValueAsString(jsonNode, "registeredaddresssingleline"));
        account.setRegisteredFullAddress(findValueAsString(jsonNode, "registeredfulladdress"));
        account.setSingleLineAddress(findValueAsString(jsonNode, "singlelineaddress"));
        account.setStartDate(findValueAsString(jsonNode, "startdate"));
        account.setAddressLine1(findValueAsString(jsonNode, "addressline1"));
        account.setAddressLine2(findValueAsString(jsonNode, "addressline2"));
        account.setAddressLine3(findValueAsString(jsonNode, "addressline3"));
        account.setCounty(findValueAsString(jsonNode, "county"));
        account.setPostcode(findValueAsString(jsonNode, "postcode"));
        account.setCountry(findValueAsString(jsonNode, "country"));

        mapCommonValues(jsonNode, account);

        return account;
    }
}