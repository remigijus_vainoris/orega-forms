package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.CheckListItem;

@Component
public class CheckListItemMapper implements Mapper<CheckListItem>
{
    @Override
    public CheckListItem fromJson(JsonNode jsonNode)
    {

        CheckListItem checkListItem = new CheckListItem();

        checkListItem.setId(findValueAsString(jsonNode, "checklistitemsid"));
        checkListItem.setName(findValueAsString(jsonNode, "checklistitem"));

        mapCommonValues(jsonNode, checkListItem);

        return checkListItem;
    }
}
