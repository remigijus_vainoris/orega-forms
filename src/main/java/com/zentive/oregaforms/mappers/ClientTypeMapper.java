package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.ClientType;

@Component
public class ClientTypeMapper implements Mapper<ClientType>
{
    @Override
    public ClientType fromJson(JsonNode jsonNode)
    {
        ClientType clientType = new ClientType();

        clientType.setId(findValueAsString(jsonNode, "clienttypeid"));
        clientType.setName(findValueAsString(jsonNode, "clienttypename"));

        clientType.setChargeExternalRates(findValueAsString(jsonNode, "chargeexternalrates"));
        clientType.setClientTypeAndOrganization(findValueAsString(jsonNode, "clienttypeandorganization"));
        clientType.setOrganizationId(findValueAsString(jsonNode, "organization"));
        clientType.setWebResCreditCard(findValueAsString(jsonNode, "webrescreditcard"));
        clientType.setResidentialClassification(findValueAsString(jsonNode, "residentialclassification"));
        clientType.setUseInvoicePrefix(findValueAsString(jsonNode, "useinvoiceprefix"));
        clientType.setVirtualClassification(findValueAsString(jsonNode, "virtualclassification"));

        mapCommonValues(jsonNode, clientType);

        return clientType;
    }
}
