package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.InvoiceSummary;

@Component
public class InvoiceSummaryMapper implements Mapper<InvoiceSummary>
{
    @Override
    public InvoiceSummary fromJson(JsonNode jsonNode)
    {
        InvoiceSummary invoiceSummary = new InvoiceSummary();

        invoiceSummary.setId(findValueAsString(jsonNode, "invoicesummaryid"));
        invoiceSummary.setName(findValueAsString(jsonNode, "invoicesummaryname"));

        invoiceSummary.setCreditNoteId(findValueAsString(jsonNode, "creditnoteid"));
        invoiceSummary.setDraftCreditNoteId(findValueAsString(jsonNode, "draftcreditnoteid"));
        invoiceSummary.setDraftInvoiceId(findValueAsString(jsonNode, "draftinvoiceid"));
        invoiceSummary.setInvoiceId(findValueAsString(jsonNode, "invoiceid"));
        invoiceSummary.setOrder(findValueAsString(jsonNode, "order"));
        invoiceSummary.setRecordType(findValueAsString(jsonNode, "recordtype"));
        invoiceSummary.setSummaryTotal(findValueAsString(jsonNode, "summarytotal"));

        mapCommonValues(jsonNode, invoiceSummary);

        return invoiceSummary;

    }
}
