package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.LicenceCheckListItem;

@Component
public class LicenceCheckListItemMapper implements Mapper<LicenceCheckListItem>
{
    @Override
    public LicenceCheckListItem fromJson(JsonNode jsonNode)
    {
        LicenceCheckListItem licenceCheckListItem = new LicenceCheckListItem();

        licenceCheckListItem.setId(findValueAsString(jsonNode, "licencechecklistitemid"));
        licenceCheckListItem.setName(findValueAsString(jsonNode, "licencechecklistitemid") + " - Licence Check List Item");

        licenceCheckListItem.setChecked(findValueAsString(jsonNode, "checked"));
        licenceCheckListItem.setCheckedBy(findValueAsString(jsonNode, "checkedby"));
        licenceCheckListItem.setLicenceId(findValueAsString(jsonNode, "licence"));
        licenceCheckListItem.setLicence(findValueAsString(jsonNode, "outputvalue_licence"));
        licenceCheckListItem.setCheckListItemId(findValueAsString(jsonNode, "checklistitem"));

        mapCommonValues(jsonNode, licenceCheckListItem);

        return licenceCheckListItem;
    }
}
