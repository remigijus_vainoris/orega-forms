package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.AgreementType;

@Component
public class AgreementTypeMapper implements Mapper<AgreementType>
{
    @Override
    public AgreementType fromJson(JsonNode jsonNode)
    {
        AgreementType agreementType = new AgreementType();

        agreementType.setId(findValueAsString(jsonNode, "agreementtypesid"));
        agreementType.setName(findValueAsString(jsonNode, "agreementtype"));

        mapCommonValues(jsonNode, agreementType);

        return agreementType;
    }
}
