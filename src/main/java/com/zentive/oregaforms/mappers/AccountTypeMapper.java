package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.AccountType;

@Component
public class AccountTypeMapper implements Mapper<AccountType>
{
    @Override
    public AccountType fromJson(JsonNode jsonNode)
    {
        AccountType accountType = new AccountType();

        accountType.setId(findValueAsString(jsonNode, "accounttypeid"));
        accountType.setName(findValueAsString(jsonNode, "accounttypename"));

        mapCommonValues(jsonNode, accountType);

        return accountType;
    }
}
