package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Industry;

@Component
public class IndustryMapper implements Mapper<Industry>
{
    @Override
    public Industry fromJson(JsonNode jsonNode)
    {
        Industry industry = new Industry();

        industry.setId(findValueAsString(jsonNode, "industryid"));
        industry.setName(findValueAsString(jsonNode, "industry"));

        mapCommonValues(jsonNode, industry);

        return industry;
    }
}
