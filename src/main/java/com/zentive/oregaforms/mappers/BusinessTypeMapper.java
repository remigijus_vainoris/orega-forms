package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.BusinessType;

@Component
public class BusinessTypeMapper implements Mapper<BusinessType>
{
    @Override
    public BusinessType fromJson(JsonNode jsonNode)
    {
        BusinessType businessType = new BusinessType();

        businessType.setId(findValueAsString(jsonNode, "businesstypeid"));
        businessType.setName(findValueAsString(jsonNode, "businesstypename"));

        mapCommonValues(jsonNode, businessType);

        return businessType;
    }
}
