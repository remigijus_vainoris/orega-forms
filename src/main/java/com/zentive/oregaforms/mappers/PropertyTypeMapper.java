package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.PropertyType;

@Component
public class PropertyTypeMapper implements Mapper<PropertyType>
{
    @Override
    public PropertyType fromJson(JsonNode jsonNode)
    {
        PropertyType propertyType = new PropertyType();

        propertyType.setId(findValueAsString(jsonNode, "propertytypesid"));
        propertyType.setName(findValueAsString(jsonNode, "propertytype"));

        mapCommonValues(jsonNode, propertyType);

        return propertyType;
    }
}
