package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.JobTitle;

@Component
public class JobTitleMapper implements Mapper<JobTitle>
{
    @Override
    public JobTitle fromJson(JsonNode jsonNode)
    {
        JobTitle jobTitle = new JobTitle();

        jobTitle.setId(findValueAsString(jsonNode, "jobtitlesid"));
        jobTitle.setName(findValueAsString(jsonNode, "jobtitle"));

        mapCommonValues(jsonNode, jobTitle);

        return jobTitle;
    }
}
