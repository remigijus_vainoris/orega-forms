package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Contact;

@Component
public class ContactMapper implements Mapper<Contact>
{
    @Override
    public Contact fromJson(JsonNode jsonNode)
    {
        Contact contact = new Contact();

        contact.setId(findValueAsString(jsonNode, "contactid"));
        contact.setName(findValueAsString(jsonNode, "fullname"));

        contact.setAddressId(findValueAsString(jsonNode, "address_id"));
        contact.setAddress(findValueAsString(jsonNode, "outputvalue_address_id"));
        contact.setCheckedIn(findValueAsString(jsonNode, "checkedin"));
        contact.setClientTypeId(findValueAsString(jsonNode, "clienttype"));
        contact.setClientType(findValueAsString(jsonNode, "outputvalue_clienttype"));
        contact.setCommunityProfileActive(findValueAsString(jsonNode, "communityprofileactive"));
        contact.setCompanyLogoURL(findValueAsString(jsonNode, "companylogourl"));
        contact.setFullName(findValueAsString(jsonNode, "fullname"));
        contact.setEmailAddress(findValueAsString(jsonNode, "emailaddress"));
        contact.setOptOut(findValueAsString(jsonNode, "optout"));
        contact.setFirstName(findValueAsString(jsonNode, "firstname"));
        contact.setHasActiveLicence(findValueAsString(jsonNode, "hasactivelicence"));
        contact.setPrivatePhoneNo(findValueAsString(jsonNode, "privatephoneno"));
        contact.setContactImageURL(findValueAsString(jsonNode, "contactimageurl"));
        contact.setInvitedFromPortal(findValueAsString(jsonNode, "invitedfromportal"));
        contact.setLastName(findValueAsString(jsonNode, "lastname"));
        contact.setLeadId(findValueAsString(jsonNode, "leadname"));
        contact.setLead(findValueAsString(jsonNode, "outputvalue_leadname"));
        contact.setLocationId(findValueAsString(jsonNode, "locationname"));
        contact.setLocation(findValueAsString(jsonNode, "outputvalue_locationname"));
        contact.setNewsletter(findValueAsString(jsonNode, "newsletter"));
        contact.setNumberBeingFollowed(findValueAsString(jsonNode, "numberbeingfollowed"));
        contact.setNumberOfFollowers(findValueAsString(jsonNode, "numberoffollowers"));
        contact.setPortalAccessLevel(findValueAsString(jsonNode, "portalaccesslevel"));
        contact.setProfilePercentComplete(findValueAsString(jsonNode, "profilepercentcomplete"));
        contact.setCompanyProfileActive(findValueAsString(jsonNode, "companyprofileactive"));
        contact.setReceiveShoutOutEmails(findValueAsString(jsonNode, "receiveshoutoutemails"));
        contact.setReceiveStatusUpdateEmails(findValueAsString(jsonNode, "receivestatusupdateemails"));
        contact.setRemainingHours(findValueAsString(jsonNode, "remaininghours"));
        contact.setPhoneNo(findValueAsString(jsonNode, "phoneno"));
        contact.setTitle(findValueAsString(jsonNode, "title"));

        mapCommonValues(jsonNode, contact);

        return contact;
    }
}
