package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.TransferReason;

@Component
public class TransferReasonMapper implements Mapper<TransferReason>
{
    @Override
    public TransferReason fromJson(JsonNode jsonNode)
    {
        TransferReason transferReason = new TransferReason();

        transferReason.setId(findValueAsString(jsonNode, "transferreasonsid"));
        transferReason.setName(findValueAsString(jsonNode, "transferreason"));

        transferReason.setReason(findValueAsString(jsonNode, "transferreason"));

        mapCommonValues(jsonNode, transferReason);

        return transferReason;
    }
}
