package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.NonStandardTerm;

@Component
public class NonStandardTermMapper implements Mapper<NonStandardTerm>
{
    @Override
    public NonStandardTerm fromJson(JsonNode jsonNode)
    {
        NonStandardTerm nonStandardTerm = new NonStandardTerm();

        nonStandardTerm.setId(findValueAsString(jsonNode, "nonstandardtermsid"));
        nonStandardTerm.setName(findValueAsString(jsonNode, "nonstandardterm"));

        nonStandardTerm.setTerm(findValueAsString(jsonNode, "nonstandardterm"));

        mapCommonValues(jsonNode, nonStandardTerm);

        return nonStandardTerm;
    }
}
