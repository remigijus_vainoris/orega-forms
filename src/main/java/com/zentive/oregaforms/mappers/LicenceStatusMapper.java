package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.LicenceStatus;

@Component
public class LicenceStatusMapper implements Mapper<LicenceStatus>
{
    @Override
    public LicenceStatus fromJson(JsonNode jsonNode)
    {

        LicenceStatus licenceStatus = new LicenceStatus();

        licenceStatus.setId(findValueAsString(jsonNode, "licencestatusid"));
        licenceStatus.setName(findValueAsString(jsonNode, "licencestatusname"));

        mapCommonValues(jsonNode, licenceStatus);

        return licenceStatus;
    }
}
