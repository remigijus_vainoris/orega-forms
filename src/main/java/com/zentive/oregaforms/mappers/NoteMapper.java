package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Note;

@Component
public class NoteMapper implements Mapper<Note>
{
    @Override
    public Note fromJson(JsonNode jsonNode)
    {
        Note note = new Note();

        note.setId(findValueAsString(jsonNode, "noteid"));
        note.setName(findValueAsString(jsonNode, "subject"));

        note.setAccountId(findValueAsString(jsonNode, "accountid"));
        note.setAccount(findValueAsString(jsonNode, "outputvalue_accountid"));
        note.setComments(findValueAsString(jsonNode, "comments"));
        note.setContactId(findValueAsString(jsonNode, "contactid"));
        note.setFilename(findValueAsString(jsonNode, "filename"));
        note.setLeadId(findValueAsString(jsonNode, "leadid"));
        note.setLead(findValueAsString(jsonNode, "outputvalue_leadid"));
        note.setOpportunityId(findValueAsString(jsonNode, "opportunityid"));
        note.setOpportunity(findValueAsString(jsonNode, "outputvalue_opportunityid"));
        note.setSubject(findValueAsString(jsonNode, "subject"));

        mapCommonValues(jsonNode, note);

        return note;
    }
}
