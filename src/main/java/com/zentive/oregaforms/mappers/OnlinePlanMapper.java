package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.OnlinePlan;

@Component
public class OnlinePlanMapper implements Mapper<OnlinePlan>
{
    @Override
    public OnlinePlan fromJson(JsonNode jsonNode)
    {
        OnlinePlan onlinePlan = new OnlinePlan();

        onlinePlan.setId(findValueAsString(jsonNode, "onlineplanid"));
        onlinePlan.setName(findValueAsString(jsonNode, "onlineplanname"));

        onlinePlan.setChoosePackageText(findValueAsString(jsonNode, "choosepackagetext"));
        onlinePlan.setChoosePackageTitle(findValueAsString(jsonNode, "choosepackagetitle"));
        onlinePlan.setDescription(findValueAsString(jsonNode, "description"));
        onlinePlan.setPlanType(findValueAsString(jsonNode, "plantype"));

        mapCommonValues(jsonNode, onlinePlan);

        return onlinePlan;
    }
}
