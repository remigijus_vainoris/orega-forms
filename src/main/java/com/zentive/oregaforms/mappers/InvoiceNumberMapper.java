package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.InvoiceNumber;

@Component
public class InvoiceNumberMapper implements Mapper<InvoiceNumber>
{
    @Override
    public InvoiceNumber fromJson(JsonNode jsonNode)
    {
        InvoiceNumber invoiceNumber = new InvoiceNumber();

        invoiceNumber.setId(findValueAsString(jsonNode, "invoicenumberid"));
        invoiceNumber.setName(findValueAsString(jsonNode, "invoicenumber"));

        invoiceNumber.setNumber(findValueAsString(jsonNode, "invoicenumber"));
        invoiceNumber.setNumberPrefix(findValueAsString(jsonNode, "invoiceprefix"));

        mapCommonValues(jsonNode, invoiceNumber);

        return invoiceNumber;
    }
}
