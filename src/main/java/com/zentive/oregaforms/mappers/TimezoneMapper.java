package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Timezone;

@Component
public class TimezoneMapper implements Mapper<Timezone>
{
    @Override
    public Timezone fromJson(JsonNode jsonNode)
    {
        Timezone timezone = new Timezone();

        timezone.setId(findValueAsString(jsonNode, "timezoneid"));
        timezone.setName(findValueAsString(jsonNode, "timezonename"));

        timezone.setTimezoneOffset(findValueAsString(jsonNode, "timezneoffset"));

        mapCommonValues(jsonNode, timezone);

        return timezone;
    }
}
