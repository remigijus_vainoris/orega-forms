package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.County;

@Component
public class CountyMapper implements Mapper<County>
{
    @Override
    public County fromJson(JsonNode jsonNode)
    {
        County county = new County();

        county.setId(findValueAsString(jsonNode, "countiesid"));
        county.setName(findValueAsString(jsonNode, "county"));

        mapCommonValues(jsonNode, county);

        return county;
    }
}
