package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.BreakDate;

@Component
public class BreakDateMapper implements Mapper<BreakDate>
{
    @Override
    public BreakDate fromJson(JsonNode jsonNode)
    {
        BreakDate breakDate = new BreakDate();

        breakDate.setId(findValueAsString(jsonNode, "breakdateid"));
        breakDate.setName(findValueAsString(jsonNode, "breakdate"));

        breakDate.setBreakDate(findValueAsString(jsonNode, "breakdate"));
        breakDate.setInterval(findValueAsString(jsonNode, "interval"));
        breakDate.setLicenceId(findValueAsString(jsonNode, "licence"));
        breakDate.setLicence(findValueAsString(jsonNode, "outputvalue_licence"));

        mapCommonValues(jsonNode, breakDate);

        return breakDate;
    }
}
