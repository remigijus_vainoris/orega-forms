package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.GLCode;

@Component
public class GLCodeMapper implements Mapper<GLCode>
{
    @Override
    public GLCode fromJson(JsonNode jsonNode)
    {
        GLCode glCode = new GLCode();

        glCode.setId(findValueAsString(jsonNode, "glcodeid"));
        glCode.setName(findValueAsString(jsonNode, "glcodename"));

        glCode.setClientRetainer(findValueAsString(jsonNode, "clientretainer"));
        glCode.setControlAccount(findValueAsString(jsonNode, "controlaccount"));
        glCode.setDisableDelete(findValueAsString(jsonNode, "disabledelete"));
        glCode.setForfeitCode(findValueAsString(jsonNode, "forfeitcode"));
        glCode.setGoCardless(findValueAsString(jsonNode, "gocardless"));
        glCode.setGoCardlessFee(findValueAsString(jsonNode, "gocardlessfee"));
        glCode.setOrganizationId(findValueAsString(jsonNode, "organization"));
        glCode.setRetainerLiability(findValueAsString(jsonNode, "retainerliability"));

        mapCommonValues(jsonNode, glCode);

        return glCode;
    }
}
