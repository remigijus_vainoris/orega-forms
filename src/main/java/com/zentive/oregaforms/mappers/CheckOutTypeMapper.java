package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.CheckOutType;

@Component
public class CheckOutTypeMapper implements Mapper<CheckOutType>
{
    @Override
    public CheckOutType fromJson(JsonNode jsonNode)
    {
        CheckOutType checkOutType = new CheckOutType();

        checkOutType.setId(findValueAsString(jsonNode, "checkouttypesid"));
        checkOutType.setName(findValueAsString(jsonNode, "checkouttype"));

        mapCommonValues(jsonNode, checkOutType);

        return checkOutType;
    }
}
