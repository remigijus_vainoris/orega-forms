package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.AssignedClientType;

@Component
public class AssignedClientTypeMapper implements Mapper<AssignedClientType>
{
    @Override
    public AssignedClientType fromJson(JsonNode jsonNode)
    {
        AssignedClientType assignedClientType = new AssignedClientType();

        assignedClientType.setId(findValueAsString(jsonNode, "assignedclienttypeid"));
        assignedClientType.setName(findValueAsString(jsonNode, "outputvalue_clienttype"));

        assignedClientType.setClientType(findValueAsString(jsonNode, "clienttype"));
        assignedClientType.setLocation(findValueAsString(jsonNode, "location"));
        assignedClientType.setOrganizationName(findValueAsString(jsonNode, "organizationname"));

        mapCommonValues(jsonNode, assignedClientType);

        return assignedClientType;
    }
}
