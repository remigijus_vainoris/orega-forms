package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.CreditCard;

@Component
public class CreditCardMapper implements Mapper<CreditCard>
{
    @Override
    public CreditCard fromJson(JsonNode jsonNode)
    {
        CreditCard creditCard = new CreditCard();

        creditCard.setId(findValueAsString(jsonNode, "creditcardid"));
        creditCard.setName(findValueAsString(jsonNode, "description"));

        creditCard.setDescription(findValueAsString(jsonNode, "description"));

        mapCommonValues(jsonNode, creditCard);

        return creditCard;
    }
}
