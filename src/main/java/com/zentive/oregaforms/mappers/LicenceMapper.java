package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Licence;

@Component
public class LicenceMapper implements Mapper<Licence>
{
    @Override
    public Licence fromJson(JsonNode jsonNode)
    {
        Licence licence = new Licence();

        licence.setId(findValueAsString(jsonNode, "licenceid"));
        licence.setName(findValueAsString(jsonNode, "licencename"));

        licence.setAccountId(findValueAsString(jsonNode, "accountname"));
        licence.setAccount(findValueAsString(jsonNode, "outputvalue_accountname"));
        licence.setWorkstations(findValueAsString(jsonNode, "workstations"));
        licence.setAdditionalDeposit(findValueAsString(jsonNode, "additionaldeposit"));
        licence.setAdditionalProductNames(findValueAsString(jsonNode, "additionalproductnames"));
        licence.setAdditionalProductPrices(findValueAsString(jsonNode, "additionalproductprices"));
        licence.setAdditionalProductQuantities(findValueAsString(jsonNode, "additionalproductquantities"));
        licence.setAdditionalProductTotalPrices(findValueAsString(jsonNode, "additionalproducttotalprices"));
        licence.setAddressLine1(findValueAsString(jsonNode, "addressline1"));
        licence.setAddressLine2(findValueAsString(jsonNode, "addressline2"));
        licence.setAddressLine3(findValueAsString(jsonNode, "addressline3"));
        licence.setAgreementNo(findValueAsString(jsonNode, "agreementno"));
        licence.setAgreementType(findValueAsString(jsonNode, "agreementtype"));
        licence.setApprovedBy(findValueAsString(jsonNode, "approvedby"));
        licence.setAutoRenew(findValueAsString(jsonNode, "autorenew"));
        licence.setContactId(findValueAsString(jsonNode, "contactid"));
        licence.setContact(findValueAsString(jsonNode, "outputvalue_contactid"));
        licence.setBillingPattern(findValueAsString(jsonNode, "billingpattern"));
        licence.setClientTypeId(findValueAsString(jsonNode, "clienttype"));
        licence.setClientType(findValueAsString(jsonNode, "outputvalue_clienttype"));
        licence.setCompanyNo(findValueAsString(jsonNode, "companyno"));
        licence.setContractDiscount(findValueAsString(jsonNode, "contractdiscount"));
        licence.setContractListPrice(findValueAsString(jsonNode, "contractlistprice"));
        licence.setContractServiceNames(findValueAsString(jsonNode, "contractservicenames"));
        licence.setContractServicePrices(findValueAsString(jsonNode, "contractserviceprices"));
        licence.setContractServiceQuantities(findValueAsString(jsonNode, "contractservicequantities"));
        licence.setContractServiceTotalPrices(findValueAsString(jsonNode, "contractservicetotalprices"));
        licence.setContractValue(findValueAsString(jsonNode, "contractvalue"));
        licence.setCounterSignUser(findValueAsString(jsonNode, "countersignuser"));
        licence.setCountry(findValueAsString(jsonNode, "country"));
        licence.setCounty(findValueAsString(jsonNode, "county"));
        licence.setDepositHeld(findValueAsString(jsonNode, "depositheld"));
        licence.setDirectDebit(findValueAsString(jsonNode, "directdebit"));
        licence.setEmailAddress(findValueAsString(jsonNode, "emailaddress"));
        licence.setEscalateBy(findValueAsString(jsonNode, "escalateby"));
        licence.setEscalationMonths(findValueAsString(jsonNode, "escalationmonths"));
        licence.setExpansionValue(findValueAsString(jsonNode, "expansionvalue"));
        licence.setFaxNo(findValueAsString(jsonNode, "faxno"));
        licence.setIndustry(findValueAsString(jsonNode, "indeustry"));
        licence.setLeadSourceId(findValueAsString(jsonNode, "leadsource"));
        licence.setLeadSource(findValueAsString(jsonNode, "outputvalue_leadsource"));
        licence.setLicenceTemplateId(findValueAsString(jsonNode, "licencetemplate"));
        licence.setLicenceTemplate(findValueAsString(jsonNode, "outputvalue_licencetemplate"));
        licence.setLicenceType(findValueAsString(jsonNode, "licencetype"));
        licence.setLocationId(findValueAsString(jsonNode, "locationname"));
        licence.setLocation(findValueAsString(jsonNode, "outputvalue_locationname"));
        licence.setMandatoryChargesLoaded(findValueAsString(jsonNode, "mandatorychargesloaded"));
        licence.setMeetingPackageId(findValueAsString(jsonNode, "meetingpackageid"));
        licence.setMobileNo(findValueAsString(jsonNode, "mobileno"));
        licence.setNewMember(findValueAsString(jsonNode, "newmember"));
        licence.setNoticeGiven(findValueAsString(jsonNode, "noticegiven"));
        licence.setNoticePeriod(findValueAsString(jsonNode, "noticeperiod"));
        licence.setMonthlyOffice(findValueAsString(jsonNode, "monthlyoffice"));
        licence.setMonthlyOfficeTax(findValueAsString(jsonNode, "monthlyofficetax"));
        licence.setOffices(findValueAsString(jsonNode, "offices"));
        licence.setOnboardingEmailSent(findValueAsString(jsonNode, "onboardingemailsent"));
        licence.setOpportunityId(findValueAsString(jsonNode, "opportunity"));
        licence.setOpportunityTypeId(findValueAsString(jsonNode, "opportunitytype"));
        licence.setOpportunityType(findValueAsString(jsonNode, "outputvalue_opportunitytype"));
        licence.setOriginalLicenceAgreementId(findValueAsString(jsonNode, "originallicenceagreementid"));
        licence.setPostcode(findValueAsString(jsonNode, "postcode"));
        licence.setItems(findValueAsString(jsonNode, "items"));
        licence.setQuoteId(findValueAsString(jsonNode, "quoteid"));
        licence.setQuoteOptionId(findValueAsString(jsonNode, "quoteoptionid"));
        licence.setRegisteredAddressLine1(findValueAsString(jsonNode, "registeredaddressline1"));
        licence.setRegisteredAddressLine2(findValueAsString(jsonNode, "registeredaddressline2"));
        licence.setRegisteredAddressLine3(findValueAsString(jsonNode, "registeredaddressline3"));
        licence.setRegisteredCountry(findValueAsString(jsonNode, "registeredcountry"));
        licence.setRegisteredCounty(findValueAsString(jsonNode, "registeredcounty"));
        licence.setRegisteredFaxNo(findValueAsString(jsonNode, "registeredfaxno"));
        licence.setRegisteredPostcode(findValueAsString(jsonNode, "registeredpostcode"));
        licence.setRegisteredPhoneNo(findValueAsString(jsonNode, "registeredphoneno"));
        licence.setRegisteredTown(findValueAsString(jsonNode, "registeredtown"));
        licence.setRenewalTerm(findValueAsString(jsonNode, "renewalterm"));
        licence.setRenewed(findValueAsString(jsonNode, "renewed"));
        licence.setRenewing(findValueAsString(jsonNode, "renewing"));
        licence.setRentUplift(findValueAsString(jsonNode, "rentuplift"));
        licence.setRentScheduleMonths(findValueAsString(jsonNode, "rentschedulemonths"));
        licence.setRentSchedulePrices(findValueAsString(jsonNode, "rentscheduleprices"));
        licence.setRentValue(findValueAsString(jsonNode, "rentvalue"));
        licence.setRollingLicence(findValueAsString(jsonNode, "rollinglicence"));
        licence.setRollingBreakDates(findValueAsString(jsonNode, "rollingbreakdates"));
        licence.setSalesPersonId(findValueAsString(jsonNode, "salesperson"));
        licence.setSalesperson(findValueAsString(jsonNode, "outputvalue_salesperson"));
        licence.setScheduleId(findValueAsString(jsonNode, "schedule"));
        licence.setSchedule(findValueAsString(jsonNode, "outputvalue_schedule"));
        licence.setServiceUplift(findValueAsString(jsonNode, "serviceuplift"));
        licence.setMonthlyService(findValueAsString(jsonNode, "monthlyservice"));
        licence.setMonthlyServiceTax(findValueAsString(jsonNode, "monthlyservicetax"));
        licence.setServiceValue(findValueAsString(jsonNode, "servicevalue"));
        licence.setServices(findValueAsString(jsonNode, "services"));
        licence.setShowBreakDates(findValueAsString(jsonNode, "showbreakdates"));
        licence.setSpecialTerms(findValueAsString(jsonNode, "specialterms"));
        licence.setStartDate(findValueAsString(jsonNode, "startdate"));
        licence.setLicenceStatusId(findValueAsString(jsonNode, "licencestatusid"));
        licence.setLicenceStatus(findValueAsString(jsonNode, "outputvalue_licencestatusid"));
        licence.setSumInsured(findValueAsString(jsonNode, "suminsured"));
        licence.setSuretyAddress1(findValueAsString(jsonNode, "suretyaddress1"));
        licence.setSuretyAddress2(findValueAsString(jsonNode, "suretyaddress2"));
        licence.setSuretyAddress3(findValueAsString(jsonNode, "suretyaddress3"));
        licence.setSuretyCountry(findValueAsString(jsonNode, "suretycountry"));
        licence.setSuretyCounty(findValueAsString(jsonNode, "suretycounty"));
        licence.setSuretyEmail(findValueAsString(jsonNode, "suretyemail"));
        licence.setSuretyMobile(findValueAsString(jsonNode, "suretymobile"));
        licence.setSuretyPostcode(findValueAsString(jsonNode, "suretypostcode"));
        licence.setSuretyRequired(findValueAsString(jsonNode, "suretyrequired"));
        licence.setSuretyTelephone(findValueAsString(jsonNode, "suretytelephone"));
        licence.setSuretyTown(findValueAsString(jsonNode, "suretytown"));
        licence.setTaxNumber(findValueAsString(jsonNode, "taxnumber"));
        licence.setPhoneNo(findValueAsString(jsonNode, "phoneno"));
        licence.setTermMonths(findValueAsString(jsonNode, "termmonths"));
        licence.setTermDays(findValueAsString(jsonNode, "termdays"));
        licence.setEndDate(findValueAsString(jsonNode, "enddate"));
        licence.setMonthlyTotal(findValueAsString(jsonNode, "monthlytotal"));
        licence.setMonthlyGross(findValueAsString(jsonNode, "monthlygross"));
        licence.setMonthlyTax(findValueAsString(jsonNode, "monthlytax"));
        licence.setTown(findValueAsString(jsonNode, "town"));
        licence.setTransferred(findValueAsString(jsonNode, "transferred"));
        licence.setTransferring(findValueAsString(jsonNode, "transferring"));
        licence.setValueOfGoods(findValueAsString(jsonNode, "valueofgoods"));
        licence.setVirtualUplift(findValueAsString(jsonNode, "virtualuplift"));
        licence.setMonthlyVirtual(findValueAsString(jsonNode, "monthlyvirtual"));
        licence.setMonthlyVirtualTax(findValueAsString(jsonNode, "monthlyvirtualtax"));
        licence.setVirtualValue(findValueAsString(jsonNode, "virtualvalue"));

        mapCommonValues(jsonNode, licence);

        return licence;
    }
}
