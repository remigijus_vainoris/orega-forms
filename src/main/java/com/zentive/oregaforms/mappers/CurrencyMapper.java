package com.zentive.oregaforms.mappers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.models.Currency;

@Component
public class CurrencyMapper implements Mapper<Currency>
{
    @Override
    public Currency fromJson(JsonNode jsonNode)
    {
        Currency currency = new Currency();

        currency.setId(findValueAsString(jsonNode, "currencyid"));
        currency.setName(findValueAsString(jsonNode, "currencyname"));

        currency.setCurrencyCode(findValueAsString(jsonNode, "currencycode"));
        currency.setCurrencyFormat(findValueAsString(jsonNode, "currencyformat"));
        currency.setNumberOfDecimalDigits(findValueAsString(jsonNode, "noofdecdigits"));

        mapCommonValues(jsonNode, currency);

        return currency;
    }
}
