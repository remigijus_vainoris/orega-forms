package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.Timezone;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class TimezoneServiceImpl extends BaseServiceImpl implements JsonReadService<Timezone>
{
    private final Mapper<Timezone> timezoneMapper;

    public TimezoneServiceImpl(OperateAPI operateAPI, Mapper<Timezone> timezoneMapper)
    {
        super(operateAPI);
        this.timezoneMapper = timezoneMapper;
    }

    @Override
    public List<Timezone> getAll()
    {
        List<Timezone> timezones = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllTimezones());

        if (response != null)
        {
            JsonNode timezonesNode = response.get("timezones");

            if (timezonesNode != null)
                timezonesNode.iterator().forEachRemaining(timezone -> timezones.add(timezoneMapper.fromJson(timezone)));
        }

        return timezones;
    }

    @Override
    public Timezone getById(String id)
    {
        Timezone timezone = null;

        JsonNode response = getResponse(operateAPI.getTimezone(id));

        if (response != null)
        {
            JsonNode timezoneNode = response.get("timezones");

            if (timezoneNode != null)
                timezone = timezoneMapper.fromJson(timezoneNode.get(0));
        }

        return timezone;
    }
}
