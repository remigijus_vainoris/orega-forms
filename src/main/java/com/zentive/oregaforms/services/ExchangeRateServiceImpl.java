package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.ExchangeRate;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class ExchangeRateServiceImpl extends BaseServiceImpl implements JsonReadService<ExchangeRate>
{
    private final Mapper<ExchangeRate> exchangeRateMapper;

    public ExchangeRateServiceImpl(OperateAPI operateAPI, Mapper<ExchangeRate> exchangeRateMapper)
    {
        super(operateAPI);
        this.exchangeRateMapper = exchangeRateMapper;
    }

    @Override
    public List<ExchangeRate> getAll()
    {
        List<ExchangeRate> exchangeRates = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllExchangeRates());

        if (response != null)
        {
            JsonNode exchangeRatesNode = response.get("exchangerates");

            if (exchangeRatesNode != null)
                exchangeRatesNode.iterator().forEachRemaining(exchangeRate -> exchangeRates.add(exchangeRateMapper.fromJson(exchangeRate)));
        }

        return exchangeRates;
    }

    @Override
    public ExchangeRate getById(String id)
    {
        ExchangeRate exchangeRate = null;

        JsonNode response = getResponse(operateAPI.getExchangeRate(id));

        if (response != null)
        {
            JsonNode exchangeRateNode = response.get("exchangerates");

            if (exchangeRateNode != null)
                exchangeRate = exchangeRateMapper.fromJson(exchangeRateNode.get(0));
        }

        return exchangeRate;
    }
}
