package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.CreditCard;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class CreditCardServiceImpl extends BaseServiceImpl implements JsonReadService<CreditCard>
{
    private final Mapper<CreditCard> creditCardMapper;

    public CreditCardServiceImpl(OperateAPI operateAPI, Mapper<CreditCard> creditCardMapper)
    {
        super(operateAPI);
        this.creditCardMapper = creditCardMapper;
    }

    @Override
    public List<CreditCard> getAll()
    {
        List<CreditCard> creditCards = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllCreditCards());

        if (response != null)
        {
            JsonNode creditCardsNode = response.get("creditcards");

            if (creditCardsNode != null)
                creditCardsNode.iterator().forEachRemaining(creditCard -> creditCards.add(creditCardMapper.fromJson(creditCard)));
        }

        return creditCards;
    }

    @Override
    public CreditCard getById(String id)
    {
        CreditCard creditCard = null;

        JsonNode response = getResponse(operateAPI.getCreditCard(id));

        if (response != null)
        {
            JsonNode creditCardNode = response.get("creditcards");

            if (creditCardNode != null)
                creditCard = creditCardMapper.fromJson(creditCardNode.get(0));
        }

        return creditCard;
    }
}
