package com.zentive.oregaforms.services.licence;

import java.util.List;

import com.zentive.oregaforms.models.LicenceItemSchedule;
import com.zentive.oregaforms.services.JsonReadService;

public interface LicenceItemScheduleService extends JsonReadService<LicenceItemSchedule>
{
    List<LicenceItemSchedule> getAllByLicenceItem(String licenceItemId);
}
