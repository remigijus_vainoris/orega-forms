package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.LicenceItemSchedule;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class LicenceItemScheduleServiceImpl extends BaseServiceImpl implements LicenceItemScheduleService
{
    private final Mapper<LicenceItemSchedule> licenceItemScheduleMapper;

    public LicenceItemScheduleServiceImpl(OperateAPI operateAPI, Mapper<LicenceItemSchedule> licenceItemScheduleMapper)
    {
        super(operateAPI);
        this.licenceItemScheduleMapper = licenceItemScheduleMapper;
    }

    @Override
    public List<LicenceItemSchedule> getAllByLicenceItem(String licenceItemId)
    {
        return getLicenceItemSchedulesFromResposne(getResponse(operateAPI.getAllLicenceItemSchedulesByLicenceItem(licenceItemId)));
    }

    @Override
    public List<LicenceItemSchedule> getAll()
    {
        return getLicenceItemSchedulesFromResposne(getResponse(operateAPI.getAllLicenceItemSchedules()));
    }

    @Override
    public LicenceItemSchedule getById(String id)
    {
        LicenceItemSchedule licenceItemSchedule = null;

        JsonNode response = getResponse(operateAPI.getLicenceItemSchedule(id));

        if (response != null)
        {
            JsonNode licenceItemScheduleNode = response.get("licenceitemschedules");

            if (licenceItemScheduleNode != null)
                licenceItemSchedule = licenceItemScheduleMapper.fromJson(licenceItemScheduleNode.get(0));
        }

        return licenceItemSchedule;
    }

    private List<LicenceItemSchedule> getLicenceItemSchedulesFromResposne(JsonNode response)
    {
        List<LicenceItemSchedule> licenceItemSchedules = new ArrayList<>();

        if (response != null)
        {
            JsonNode licenceItemSchedulesNode = response.get("licenceitemschedules");

            if (licenceItemSchedulesNode != null)
                licenceItemSchedulesNode.iterator().forEachRemaining(
                        licenceItemSchedule -> licenceItemSchedules.add(licenceItemScheduleMapper.fromJson(licenceItemSchedule)));
        }

        return licenceItemSchedules;
    }
}
