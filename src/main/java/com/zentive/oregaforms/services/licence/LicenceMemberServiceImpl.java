package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.LicenceMember;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class LicenceMemberServiceImpl extends BaseServiceImpl implements LicenceMemberService
{
    private final Mapper<LicenceMember> licenceMemberMapper;

    public LicenceMemberServiceImpl(OperateAPI operateAPI, Mapper<LicenceMember> licenceMemberMapper)
    {
        super(operateAPI);
        this.licenceMemberMapper = licenceMemberMapper;
    }

    @Override
    public List<LicenceMember> getAllByLicenceItem(String licenceItemId)
    {
        return getLicenceMembersFromResponse(getResponse(operateAPI.getAllLicenceMembersByLicenceItem(licenceItemId)));
    }

    @Override
    public List<LicenceMember> getAll()
    {
        return getLicenceMembersFromResponse(getResponse(operateAPI.getAllLicenceMembers()));
    }

    @Override
    public LicenceMember getById(String id)
    {
        LicenceMember licenceMember = null;

        JsonNode response = getResponse(operateAPI.getLicenceMember(id));

        if (response != null)
        {
            JsonNode licenceMemberNode = response.get("licencemembers");

            if (licenceMemberNode != null)
                licenceMember = licenceMemberMapper.fromJson(licenceMemberNode.get(0));
        }

        return licenceMember;
    }

    private List<LicenceMember> getLicenceMembersFromResponse(JsonNode response)
    {
        List<LicenceMember> licenceMembers = new ArrayList<>();

        if (response != null)
        {
            JsonNode licenceMembersNode = response.get("licencemembers");

            if (licenceMembersNode != null)
                licenceMembersNode.iterator().forEachRemaining(licenceMember -> licenceMembers.add(licenceMemberMapper.fromJson(licenceMember)));
        }

        return licenceMembers;
    }
}
