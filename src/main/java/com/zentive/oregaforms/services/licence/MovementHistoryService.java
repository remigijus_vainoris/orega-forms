package com.zentive.oregaforms.services.licence;

import java.util.List;

import com.zentive.oregaforms.models.MovementHistory;
import com.zentive.oregaforms.services.JsonReadService;

public interface MovementHistoryService extends JsonReadService<MovementHistory>
{
    List<MovementHistory> getAllByLocation(String locationId);
}
