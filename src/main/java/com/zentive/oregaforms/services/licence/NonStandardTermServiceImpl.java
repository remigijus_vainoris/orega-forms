package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.NonStandardTerm;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class NonStandardTermServiceImpl extends BaseServiceImpl implements JsonReadService<NonStandardTerm>
{
    private final Mapper<NonStandardTerm> nonStandardTermMapper;

    public NonStandardTermServiceImpl(OperateAPI operateAPI, Mapper<NonStandardTerm> nonStandardTermMapper)
    {
        super(operateAPI);
        this.nonStandardTermMapper = nonStandardTermMapper;
    }

    @Override
    public List<NonStandardTerm> getAll()
    {
        List<NonStandardTerm> nonStandardTerms = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllNonStandardTerms());

        if (response != null)
        {
            JsonNode nonStandardTermsNode = response.get("lookupnonstandardtermss");

            if (nonStandardTermsNode != null)
                nonStandardTermsNode.iterator().forEachRemaining(nonStandardTerm -> nonStandardTerms.add(nonStandardTermMapper.fromJson(nonStandardTerm)));
        }

        return nonStandardTerms;
    }

    @Override
    public NonStandardTerm getById(String id)
    {
        NonStandardTerm nonStandardTerm = null;

        JsonNode response = getResponse(operateAPI.getNonStandardTerm(id));

        if (response != null)
        {
            JsonNode nonStandardTermNode = response.get("lookupnonstandardtermss");

            if (nonStandardTermNode != null)
                nonStandardTerm = nonStandardTermMapper.fromJson(nonStandardTermNode.get(0));
        }

        return nonStandardTerm;
    }
}
