package com.zentive.oregaforms.services.licence;

import java.util.List;

import com.zentive.oregaforms.models.LicenceCheckListItem;
import com.zentive.oregaforms.services.JsonReadService;

public interface LicenceCheckListItemService extends JsonReadService<LicenceCheckListItem>
{
    List<LicenceCheckListItem> getAllByLicence(String licenceId);
}
