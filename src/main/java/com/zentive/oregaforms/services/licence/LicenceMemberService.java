package com.zentive.oregaforms.services.licence;

import java.util.List;

import com.zentive.oregaforms.models.LicenceMember;
import com.zentive.oregaforms.services.JsonReadService;

public interface LicenceMemberService extends JsonReadService<LicenceMember>
{
    List<LicenceMember> getAllByLicenceItem(String licenceItemId);
}
