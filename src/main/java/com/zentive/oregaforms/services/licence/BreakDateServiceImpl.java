package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.BreakDate;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class BreakDateServiceImpl extends BaseServiceImpl implements BreakDateService
{
    private final Mapper<BreakDate> breakDateMapper;

    public BreakDateServiceImpl(OperateAPI operateAPI, Mapper<BreakDate> breakDateMapper)
    {
        super(operateAPI);
        this.breakDateMapper = breakDateMapper;
    }

    @Override
    public List<BreakDate> getAllByLicence(String licenceId)
    {
        return getBreakDatesFromResponse(getResponse(operateAPI.getAllBreakDatesByLicence(licenceId)));
    }

    @Override
    public List<BreakDate> getAll()
    {
        return getBreakDatesFromResponse(getResponse(operateAPI.getAllBreakDates()));
    }

    @Override
    public BreakDate getById(String id)
    {
        BreakDate breakDate = null;

        JsonNode response = getResponse(operateAPI.getBreadDate(id));

        if (response != null)
        {
            JsonNode breakDateNode = response.get("breakdates");

            if (breakDateNode != null)
                breakDate = breakDateMapper.fromJson(breakDateNode.get(0));
        }

        return breakDate;
    }

    private List<BreakDate> getBreakDatesFromResponse(JsonNode response)
    {
        List<BreakDate> breakDates = new ArrayList<>();

        if (response != null)
        {
            JsonNode breakDatesNode = response.get("breakdates");

            if (breakDates != null)
                breakDatesNode.iterator().forEachRemaining(breakDate -> breakDates.add(breakDateMapper.fromJson(breakDate)));
        }

        return breakDates;
    }
}
