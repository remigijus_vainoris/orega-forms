package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.MovementType;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class MovementTypeServiceImpl extends BaseServiceImpl implements JsonReadService<MovementType>
{
    private final Mapper<MovementType> movementTypeMapper;

    public MovementTypeServiceImpl(OperateAPI operateAPI, Mapper<MovementType> movementTypeMapper)
    {
        super(operateAPI);
        this.movementTypeMapper = movementTypeMapper;
    }

    @Override
    public List<MovementType> getAll()
    {
        List<MovementType> movementTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllMovementTypes());

        if (response != null)
        {
            JsonNode movementTypesNode = response.get("movementtypes");

            if (movementTypesNode != null)
                movementTypesNode.iterator().forEachRemaining(movementType -> movementTypes.add(movementTypeMapper.fromJson(movementType)));
        }

        return movementTypes;
    }

    @Override
    public MovementType getById(String id)
    {
        MovementType movementType = null;

        JsonNode response = getResponse(operateAPI.getMovementType(id));

        if (response != null)
        {
            JsonNode movementTypeNode = response.get("movementtypes");

            if (movementTypeNode != null)
                movementType = movementTypeMapper.fromJson(movementTypeNode.get(0));
        }

        return movementType;
    }
}
