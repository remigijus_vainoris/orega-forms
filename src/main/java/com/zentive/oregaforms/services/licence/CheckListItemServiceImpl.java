package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.CheckListItem;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class CheckListItemServiceImpl extends BaseServiceImpl implements JsonReadService<CheckListItem>
{
    private final Mapper<CheckListItem> checkListItemMapper;

    public CheckListItemServiceImpl(OperateAPI operateAPI, Mapper<CheckListItem> checkListItemMapper)
    {
        super(operateAPI);
        this.checkListItemMapper = checkListItemMapper;
    }

    @Override
    public List<CheckListItem> getAll()
    {
        List<CheckListItem> checkListItems = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllCheckListItems());

        if (response != null)
        {
            JsonNode checkListItemsNode = response.get("lookupchecklistitems");

            if (checkListItemsNode != null)
                checkListItemsNode.iterator().forEachRemaining(checkListItem -> checkListItems.add(checkListItemMapper.fromJson(checkListItem)));
        }

        return checkListItems;
    }

    @Override
    public CheckListItem getById(String id)
    {
        CheckListItem checkListItem = null;

        JsonNode response = getResponse(operateAPI.getCheckListItem(id));

        if (response != null)
        {
            JsonNode checkListItemNode = response.get("lookupchecklistitems");

            if (checkListItemNode != null)
                checkListItem = checkListItemMapper.fromJson(checkListItemNode.get(0));
        }

        return checkListItem;
    }
}
