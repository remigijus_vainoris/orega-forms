package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.MovementHistory;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class MovementHistoryServiceImpl extends BaseServiceImpl implements MovementHistoryService
{
    private final Mapper<MovementHistory> movementHistoryMapper;

    public MovementHistoryServiceImpl(OperateAPI operateAPI, Mapper<MovementHistory> movementHistoryMapper)
    {
        super(operateAPI);
        this.movementHistoryMapper = movementHistoryMapper;
    }

    @Override
    public List<MovementHistory> getAllByLocation(String locationId)
    {
        return getMovementHistoriesFromResponse(getResponse(operateAPI.getAllMovementHistoriesByLocation(locationId)));
    }

    @Override
    public List<MovementHistory> getAll()
    {
        return getMovementHistoriesFromResponse(getResponse(operateAPI.getAllMovementHistories()));
    }

    @Override
    public MovementHistory getById(String id)
    {
        MovementHistory movementHistory = null;

        JsonNode response = getResponse(operateAPI.getMovementHistory(id));

        if (response != null)
        {
            JsonNode movementHistoryNode = response.get("movements");

            if (movementHistoryNode != null)
                movementHistory = movementHistoryMapper.fromJson(movementHistoryNode.get(0));
        }

        return movementHistory;
    }

    private List<MovementHistory> getMovementHistoriesFromResponse(JsonNode response)
    {
        List<MovementHistory> movementHistories = new ArrayList<>();

        if (response != null)
        {
            JsonNode movementHistoriesNode = response.get("movements");

            if (movementHistoriesNode != null)
                movementHistoriesNode.iterator().forEachRemaining(movementHistory -> movementHistories.add(movementHistoryMapper.fromJson(movementHistory)));
        }

        return movementHistories;
    }
}
