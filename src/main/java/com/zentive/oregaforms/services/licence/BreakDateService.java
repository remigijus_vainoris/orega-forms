package com.zentive.oregaforms.services.licence;

import java.util.List;

import com.zentive.oregaforms.models.BreakDate;
import com.zentive.oregaforms.services.JsonReadService;

public interface BreakDateService extends JsonReadService<BreakDate>
{
    List<BreakDate> getAllByLicence(String licenceId);
}
