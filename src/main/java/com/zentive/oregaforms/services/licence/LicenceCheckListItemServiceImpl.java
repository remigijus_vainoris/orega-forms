package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.LicenceCheckListItem;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class LicenceCheckListItemServiceImpl extends BaseServiceImpl implements LicenceCheckListItemService
{
    private final Mapper<LicenceCheckListItem> licenceCheckListItemMapper;

    public LicenceCheckListItemServiceImpl(OperateAPI operateAPI, Mapper<LicenceCheckListItem> licenceCheckListItemMapper)
    {
        super(operateAPI);
        this.licenceCheckListItemMapper = licenceCheckListItemMapper;
    }

    @Override
    public List<LicenceCheckListItem> getAllByLicence(String licenceId)
    {
        return getLicenceCheckListItemsFromResponse(getResponse(operateAPI.getAllLicenceCheckListItemsByLicence(licenceId)));
    }

    @Override
    public List<LicenceCheckListItem> getAll()
    {
        return getLicenceCheckListItemsFromResponse(getResponse(operateAPI.getAllLicenceCheckListItems()));
    }

    @Override
    public LicenceCheckListItem getById(String id)
    {
        LicenceCheckListItem licenceCheckListItem = null;

        JsonNode response = getResponse(operateAPI.getLicenceCheckListItem(id));

        if (response != null)
        {
            JsonNode licenceCheckListItemNode = response.get("licencechecklistitems");

            if (licenceCheckListItemNode != null)
                licenceCheckListItem = licenceCheckListItemMapper.fromJson(licenceCheckListItemNode.get(0));
        }

        return licenceCheckListItem;
    }

    private List<LicenceCheckListItem> getLicenceCheckListItemsFromResponse(JsonNode response)
    {
        List<LicenceCheckListItem> licenceCheckListItems = new ArrayList<>();

        if (response != null)
        {
            JsonNode licenceCheckListItemsNode = response.get("licencechecklistitems");

            if (licenceCheckListItemsNode != null)
                licenceCheckListItemsNode.iterator().forEachRemaining(licenceCheckListItem -> licenceCheckListItems.add(licenceCheckListItemMapper.fromJson(licenceCheckListItem)));
        }

        return licenceCheckListItems;
    }
}
