package com.zentive.oregaforms.services.licence;

import java.util.List;

import com.zentive.oregaforms.models.Licence;
import com.zentive.oregaforms.services.JsonReadService;

public interface LicenceService extends JsonReadService<Licence>
{
    List<Licence> getAllByAccount(String accountId);

    List<Licence> getAllByLocation(String locationId);
}
