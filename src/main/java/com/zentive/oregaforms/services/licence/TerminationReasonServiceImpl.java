package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.TerminationReason;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class TerminationReasonServiceImpl extends BaseServiceImpl implements JsonReadService<TerminationReason>
{
    private final Mapper<TerminationReason> terminationReasonMapper;

    public TerminationReasonServiceImpl(OperateAPI operateAPI, Mapper<TerminationReason> terminationReasonMapper)
    {
        super(operateAPI);
        this.terminationReasonMapper = terminationReasonMapper;
    }

    @Override
    public List<TerminationReason> getAll()
    {
        List<TerminationReason> terminationReasons = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllTerminationReasons());

        if (response != null)
        {
            JsonNode terminationReasonsNode = response.get("lookupterminationresonss");

            if (terminationReasonsNode != null)
                terminationReasonsNode.iterator().forEachRemaining(terminationReason -> terminationReasons.add(terminationReasonMapper.fromJson(terminationReason)));
        }

        return terminationReasons;
    }

    @Override
    public TerminationReason getById(String id)
    {
        TerminationReason terminationReason = null;

        JsonNode response = getResponse(operateAPI.getTerminationReason(id));

        if (response != null)
        {
            JsonNode terminationReasonNode = response.get("lookupterminationresonss");

            if (terminationReasonNode != null)
                terminationReason = terminationReasonMapper.fromJson(terminationReasonNode.get(0));
        }

        return terminationReason;
    }
}
