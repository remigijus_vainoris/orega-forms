package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.LicenceStatus;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class LicenceStatusServiceImpl extends BaseServiceImpl implements JsonReadService<LicenceStatus>
{
    private final Mapper<LicenceStatus> licenceStatusMapper;

    public LicenceStatusServiceImpl(OperateAPI operateAPI, Mapper<LicenceStatus> licenceStatusMapper)
    {
        super(operateAPI);
        this.licenceStatusMapper = licenceStatusMapper;
    }

    @Override
    public List<LicenceStatus> getAll()
    {
        List<LicenceStatus> licenceStatuses = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllLicenceStatuses());

        if (response != null)
        {
            JsonNode licenceStatusesNode = response.get("licencestatuss");

            if (licenceStatusesNode != null)
                licenceStatusesNode.iterator().forEachRemaining(licenceStatus -> licenceStatuses.add(licenceStatusMapper.fromJson(licenceStatus)));
        }

        return licenceStatuses;
    }

    @Override
    public LicenceStatus getById(String id)
    {
        LicenceStatus licenceStatus = null;

        JsonNode response = getResponse(operateAPI.getLicenceStatus(id));

        if (response != null)
        {
            JsonNode licenceStatusNode = response.get("licencestatuss");

            if (licenceStatusNode != null)
                licenceStatus = licenceStatusMapper.fromJson(licenceStatusNode.get(0));
        }

        return licenceStatus;
    }
}
