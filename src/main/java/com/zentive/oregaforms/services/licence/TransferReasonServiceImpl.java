package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.TransferReason;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class TransferReasonServiceImpl extends BaseServiceImpl implements JsonReadService<TransferReason>
{
    private final Mapper<TransferReason> transferReasonMapper;

    public TransferReasonServiceImpl(OperateAPI operateAPI, Mapper<TransferReason> transferReasonMapper)
    {
        super(operateAPI);
        this.transferReasonMapper = transferReasonMapper;
    }

    @Override
    public List<TransferReason> getAll()
    {
        List<TransferReason> transferReasons = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllTransferReasons());

        if (response != null)
        {
            JsonNode transferReasonsNode = response.get("lookuptransferreasonss");

            if (transferReasonsNode != null)
                transferReasonsNode.iterator().forEachRemaining(transferReason -> transferReasons.add(transferReasonMapper.fromJson(transferReason)));
        }

        return transferReasons;
    }

    @Override
    public TransferReason getById(String id)
    {
        TransferReason transferReason = null;

        JsonNode response = getResponse(operateAPI.getTransferReason(id));

        if (response != null)
        {
            JsonNode transferReasonNode = response.get("lookuptransferreasonss");

            if (transferReasonNode != null)
                transferReason = transferReasonMapper.fromJson(transferReasonNode.get(0));
        }

        return transferReason;
    }
}
