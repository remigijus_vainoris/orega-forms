package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.LicenceItem;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class LicenceItemServiceImpl extends BaseServiceImpl implements LicenceItemService
{
    private final Mapper<LicenceItem> licenceItemMapper;

    public LicenceItemServiceImpl(OperateAPI operateAPI, Mapper<LicenceItem> licenceItemMapper)
    {
        super(operateAPI);
        this.licenceItemMapper = licenceItemMapper;
    }

    @Override
    public List<LicenceItem> getAllByLicence(String licenceId)
    {
        return getLicenceItemsFromResponse(getResponse(operateAPI.getAllLicenceItemsByLicence(licenceId)));
    }

    @Override
    public List<LicenceItem> getAll()
    {

        return getLicenceItemsFromResponse(getResponse(operateAPI.getAllLicenceItems()));
    }

    @Override
    public LicenceItem getById(String id)
    {
        LicenceItem licenceItem = null;

        JsonNode response = getResponse(operateAPI.getLicenceItem(id));

        if (response != null)
        {
            JsonNode licenceItemNode = response.get("licenceitems");

            if (licenceItemNode != null)
                licenceItem = licenceItemMapper.fromJson(licenceItemNode.get(0));
        }

        return licenceItem;
    }

    private List<LicenceItem> getLicenceItemsFromResponse(JsonNode response)
    {
        List<LicenceItem> licenceItems = new ArrayList<>();

        if (response != null)
        {
            JsonNode licenceItemsNode = response.get("licenceitems");

            if (licenceItemsNode != null)
                licenceItemsNode.iterator().forEachRemaining(licenceItem -> licenceItems.add(licenceItemMapper.fromJson(licenceItem)));
        }

        return licenceItems;
    }
}
