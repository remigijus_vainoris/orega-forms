package com.zentive.oregaforms.services.licence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.Licence;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class LicenceServiceImpl extends BaseServiceImpl implements LicenceService
{
    private final Mapper<Licence> licenceMapper;

    public LicenceServiceImpl(OperateAPI operateAPI, Mapper<Licence> licenceMapper)
    {
        super(operateAPI);
        this.licenceMapper = licenceMapper;
    }

    @Override
    public List<Licence> getAllByAccount(String accountId)
    {
        return getLicencesFromResponse(getResponse(operateAPI.getAllLicencesByAccount(accountId)));
    }

    @Override
    public List<Licence> getAllByLocation(String locationId)
    {
        return getLicencesFromResponse(getResponse(operateAPI.getAllLicencesByLocation(locationId)));
    }

    @Override
    public List<Licence> getAll()
    {
        return getLicencesFromResponse(getResponse(operateAPI.getAllLicences()));
    }

    @Override
    public Licence getById(String id)
    {
        Licence licence = null;

        JsonNode response = getResponse(operateAPI.getLicence(id));

        if (response != null)
        {
            JsonNode licenceNode = response.get("licences");

            if (licenceNode != null)
                licence = licenceMapper.fromJson(licenceNode.get(0));
        }

        return licence;
    }

    private List<Licence> getLicencesFromResponse(JsonNode response)
    {
        List<Licence> licences = new ArrayList<>();

        if (response != null)
        {
            JsonNode licencesNode = response.get("licences");

            if (licencesNode != null)
                licencesNode.iterator().forEachRemaining(licence -> licences.add(licenceMapper.fromJson(licence)));
        }

        return licences;
    }
}
