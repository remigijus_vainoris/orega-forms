package com.zentive.oregaforms.services.licence;

import java.util.List;

import com.zentive.oregaforms.models.LicenceItem;
import com.zentive.oregaforms.services.JsonReadService;

public interface LicenceItemService extends JsonReadService<LicenceItem>
{
    List<LicenceItem> getAllByLicence(String licenceId);
}
