package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.User;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class UserServiceImpl extends BaseServiceImpl implements JsonReadService<User>
{
    private final Mapper<User> userMapper;

    public UserServiceImpl(OperateAPI operateAPI, Mapper<User> userMapper)
    {
        super(operateAPI);
        this.userMapper = userMapper;
    }

    @Override
    public List<User> getAll()
    {
        List<User> users = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllUsers());

        if (response != null)
        {
            JsonNode usersNode = response.get("users");

            if (usersNode != null)
                usersNode.iterator().forEachRemaining(user -> users.add(userMapper.fromJson(user)));
        }

        return users;
    }

    @Override
    public User getById(String id)
    {
        User user = null;

        JsonNode response = getResponse(operateAPI.getUser(id));

        if (response != null)
        {
            JsonNode userNode = response.get("users");

            if (userNode != null)
                user = userMapper.fromJson(userNode.get(0));
        }

        return user;
    }
}
