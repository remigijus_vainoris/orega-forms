package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.PropertyType;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class PropertyTypeServiceImpl extends BaseServiceImpl implements JsonReadService<PropertyType>
{
    private final Mapper<PropertyType> propertyTypeMapper;

    public PropertyTypeServiceImpl(OperateAPI operateAPI, Mapper<PropertyType> propertyTypeMapper)
    {
        super(operateAPI);
        this.propertyTypeMapper = propertyTypeMapper;
    }

    @Override
    public List<PropertyType> getAll()
    {
        List<PropertyType> propertyTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllPropertyTypes());

        if (response != null)
        {
            JsonNode propertyTypesNode = response.get("lookuppropertytypess");

            if (propertyTypesNode != null)
                propertyTypesNode.iterator().forEachRemaining(propertyType -> propertyTypes.add(propertyTypeMapper.fromJson(propertyType)));
        }

        return propertyTypes;
    }

    @Override
    public PropertyType getById(String id)
    {
        PropertyType propertyType = null;

        JsonNode response = getResponse(operateAPI.getPropertyType(id));

        if (response != null)
        {
            JsonNode propertyTypeNode = response.get("lookuppropertytypess");

            if (propertyTypeNode != null)
                propertyType = propertyTypeMapper.fromJson(propertyTypeNode.get(0));
        }

        return propertyType;
    }
}
