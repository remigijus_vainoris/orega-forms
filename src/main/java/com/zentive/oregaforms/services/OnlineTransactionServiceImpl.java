package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.OnlineTransaction;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class OnlineTransactionServiceImpl extends BaseServiceImpl implements JsonReadService<OnlineTransaction>
{
    private final Mapper<OnlineTransaction> onlineTransactionMapper;

    public OnlineTransactionServiceImpl(OperateAPI operateAPI, Mapper<OnlineTransaction> onlineTransactionMapper)
    {
        super(operateAPI);
        this.onlineTransactionMapper = onlineTransactionMapper;
    }

    @Override
    public List<OnlineTransaction> getAll()
    {
        List<OnlineTransaction> onlineTransactions = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getOnlineTransactions());

        if (response != null)
        {
            JsonNode onlineTransactionsNode = response.get("portaltransactionss");

            if (onlineTransactionsNode != null)
                onlineTransactionsNode.iterator().forEachRemaining(onlineTransaction -> onlineTransactions.add(onlineTransactionMapper.fromJson(onlineTransaction)));
        }

        return onlineTransactions;
    }

    @Override
    public OnlineTransaction getById(String id)
    {
        OnlineTransaction onlineTransaction = null;

        JsonNode response = getResponse(operateAPI.getOnlineTransaction(id));

        if (response != null)
        {
            JsonNode onlineTransactionNode = response.get("portaltransactionss");

            if (onlineTransactionNode != null)
                onlineTransaction = onlineTransactionMapper.fromJson(onlineTransactionNode.get(0));
        }

        return onlineTransaction;
    }
}
