package com.zentive.oregaforms.services;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

public interface JsonReadService<T>
{
    List<T> getAll();

    T getById(String id);
}