package com.zentive.oregaforms.services.creditnotes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.CreditNoteType;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class CreditNoteTypeServiceImpl extends BaseServiceImpl implements JsonReadService<CreditNoteType>
{
    private final Mapper<CreditNoteType> creditNoteTypeMapper;

    public CreditNoteTypeServiceImpl(OperateAPI operateAPI, Mapper<CreditNoteType> creditNoteTypeMapper)
    {
        super(operateAPI);
        this.creditNoteTypeMapper = creditNoteTypeMapper;
    }

    @Override
    public List<CreditNoteType> getAll()
    {
        List<CreditNoteType> creditNoteTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllCreditNoteTypes());

        if (response != null)
        {
            JsonNode creditNoteTypesNode = response.get("lookupcreditnotetypes");

            if (creditNoteTypesNode != null)
                creditNoteTypesNode.iterator().forEachRemaining(creditNoteType -> creditNoteTypes.add(
                        creditNoteTypeMapper.fromJson(creditNoteType)));
        }

        return creditNoteTypes;
    }

    @Override
    public CreditNoteType getById(String id)
    {
        CreditNoteType creditNoteType = null;

        JsonNode response = getResponse(operateAPI.getCreditNoteType(id));

        if (response != null)
        {
            JsonNode creditNoteTypeNode = response.get("lookupcreditnotetypes");

            if (creditNoteTypeNode != null)
                creditNoteType = creditNoteTypeMapper.fromJson(creditNoteTypeNode.get(0));
        }

        return creditNoteType;
    }
}
