package com.zentive.oregaforms.services.creditnotes;

import java.util.List;

import com.zentive.oregaforms.models.CreditNote;
import com.zentive.oregaforms.services.JsonReadService;

public interface CreditNoteService extends JsonReadService<CreditNote>
{
    List<CreditNote> getAllByAccount(String accountId);

    List<CreditNote> getAllByLocation(String locationId);

    List<CreditNote> getAllByAccountAndLocation(String accountId, String locationId);
}
