package com.zentive.oregaforms.services.creditnotes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.CreditNoteLine;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class CreditNoteLineServiceImpl extends BaseServiceImpl implements CreditNoteLineService
{
    private final Mapper<CreditNoteLine> creditNoteLineMapper;

    public CreditNoteLineServiceImpl(OperateAPI operateAPI, Mapper<CreditNoteLine> creditNoteLineMapper)
    {
        super(operateAPI);
        this.creditNoteLineMapper = creditNoteLineMapper;
    }

    @Override
    public List<CreditNoteLine> getAllByCreditNote(String creditNoteId)
    {
        return getCreditNoteLinesFromResponse(getResponse(operateAPI.getAllCreditNoteLinesByCreditNote(creditNoteId)));
    }

    @Override
    public List<CreditNoteLine> getAll()
    {
        return getCreditNoteLinesFromResponse(getResponse(operateAPI.getAllCreditNoteLines()));
    }

    @Override
    public CreditNoteLine getById(String id)
    {
        CreditNoteLine creditNoteLine = null;

        JsonNode response = getResponse(operateAPI.getCreditNoteLine(id));

        if (response != null)
        {
            JsonNode creditNoteLineNode = response.get("creditnotelines");

            if (creditNoteLineNode != null)
                creditNoteLine = creditNoteLineMapper.fromJson(creditNoteLineNode.get(0));
        }

        return creditNoteLine;
    }

    private List<CreditNoteLine> getCreditNoteLinesFromResponse(JsonNode response)
    {
        List<CreditNoteLine> creditNoteLines = new ArrayList<>();

        if (response != null)
        {
            JsonNode creditNoteLinesNode = response.get("creditnotelines");

            if (creditNoteLinesNode != null)
                creditNoteLinesNode.iterator().forEachRemaining(creditNoteLine -> creditNoteLines.add(creditNoteLineMapper.fromJson(creditNoteLine)));
        }

        return creditNoteLines;
    }
}
