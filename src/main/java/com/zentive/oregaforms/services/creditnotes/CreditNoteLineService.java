package com.zentive.oregaforms.services.creditnotes;

import java.util.List;

import com.zentive.oregaforms.models.CreditNoteLine;
import com.zentive.oregaforms.services.JsonReadService;

public interface CreditNoteLineService extends JsonReadService<CreditNoteLine>
{
    List<CreditNoteLine> getAllByCreditNote(String creditNoteId);
}
