package com.zentive.oregaforms.services.creditnotes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.CreditNoteReason;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class CreditNoteReasonServiceImpl extends BaseServiceImpl implements JsonReadService<CreditNoteReason>
{
    private final Mapper<CreditNoteReason> creditNoteReasonMapper;

    public CreditNoteReasonServiceImpl(OperateAPI operateAPI, Mapper<CreditNoteReason> creditNoteReasonMapper)
    {
        super(operateAPI);
        this.creditNoteReasonMapper = creditNoteReasonMapper;
    }

    @Override
    public List<CreditNoteReason> getAll()
    {
        List<CreditNoteReason> creditNoteReasons = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllCreditNoteReasons());

        if (response != null)
        {
            JsonNode creditNoteReasonsNode = response.get("lookupcreditnotereasons");

            if (creditNoteReasonsNode != null)
                creditNoteReasonsNode.iterator().forEachRemaining(creditNoteReason -> creditNoteReasons.add(creditNoteReasonMapper.fromJson(creditNoteReason)));
        }

        return creditNoteReasons;
    }

    @Override
    public CreditNoteReason getById(String id)
    {
        CreditNoteReason creditNoteReason = null;

        JsonNode response = getResponse(operateAPI.getCreditNoteReason(id));

        if (response != null)
        {
            JsonNode creditNoteReasonNode = response.get("lookupcreditnotereasons");

            if (creditNoteReasonNode != null)
                creditNoteReason = creditNoteReasonMapper.fromJson(creditNoteReasonNode.get(0));
        }

        return creditNoteReason;
    }
}
