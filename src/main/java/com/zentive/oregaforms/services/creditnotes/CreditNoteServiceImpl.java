package com.zentive.oregaforms.services.creditnotes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.CreditNote;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class CreditNoteServiceImpl extends BaseServiceImpl implements CreditNoteService
{
    private final Mapper<CreditNote> creditNoteMapper;

    public CreditNoteServiceImpl(OperateAPI operateAPI, Mapper<CreditNote> creditNoteMapper)
    {
        super(operateAPI);
        this.creditNoteMapper = creditNoteMapper;
    }

    @Override
    public List<CreditNote> getAll()
    {
        return getCreditNotesFromResponse(getResponse(operateAPI.getAllCreditNotes()));
    }

    @Override
    public CreditNote getById(String id)
    {
        CreditNote creditNote = null;

        JsonNode response = getResponse(operateAPI.getCreditNote(id));

        if (response != null)
        {
            JsonNode creditNoteNode = response.get("creditnotes");

            if (creditNoteNode != null)
                creditNote = creditNoteMapper.fromJson(creditNoteNode.get(0));
        }

        return creditNote;
    }

    @Override
    public List<CreditNote> getAllByAccount(String accountId)
    {
        return getCreditNotesFromResponse(getResponse(operateAPI.getAllCreditNotesByAccount(accountId)));
    }

    @Override
    public List<CreditNote> getAllByLocation(String locationId)
    {
        return getCreditNotesFromResponse(getResponse(operateAPI.getAllCreditNotesByLocation(locationId)));
    }

    @Override
    public List<CreditNote> getAllByAccountAndLocation(String accountId, String locationId)
    {
        return getCreditNotesFromResponse(getResponse(operateAPI.getAllCreditNotesByAccountAndLocation(accountId, locationId)));
    }

    private List<CreditNote> getCreditNotesFromResponse(JsonNode response)
    {
        List<CreditNote> creditNotes = new ArrayList<>();

        if (response != null)
        {
            JsonNode creditNotesNode = response.get("creditnotes");

            if (creditNotesNode != null)
                creditNotesNode.iterator().forEachRemaining(creditNote -> creditNotes.add(creditNoteMapper.fromJson(creditNote)));
        }

        return creditNotes;
    }
}
