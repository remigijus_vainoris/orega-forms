package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.Industry;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class IndustryServiceImpl extends BaseServiceImpl implements JsonReadService<Industry>
{
    private final Mapper<Industry> industryMapper;

    public IndustryServiceImpl(OperateAPI operateAPI, Mapper<Industry> industryMapper)
    {
        super(operateAPI);
        this.industryMapper = industryMapper;
    }

    @Override
    public List<Industry> getAll()
    {
        List<Industry> industries = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllIndustries());

        if (response != null)
        {
            JsonNode industriesNode = response.get("industrys");

            if (industriesNode != null)
                industriesNode.iterator().forEachRemaining(industry -> industries.add(industryMapper.fromJson(industry)));
        }

        return industries;
    }

    @Override
    public Industry getById(String id)
    {
        Industry industry = null;

        JsonNode response = getResponse(operateAPI.getIndustry(id));

        if (response != null)
        {
            JsonNode industryNode = response.get("industrys");

            if (industryNode != null)
                industry = industryMapper.fromJson(industryNode.get(0));
        }

        return industry;
    }
}
