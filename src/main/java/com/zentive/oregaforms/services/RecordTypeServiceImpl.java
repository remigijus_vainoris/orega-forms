package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.RecordType;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class RecordTypeServiceImpl extends BaseServiceImpl implements JsonReadService<RecordType>
{
    private final Mapper<RecordType> recordTypeMapper;

    public RecordTypeServiceImpl(OperateAPI operateAPI, Mapper<RecordType> recordTypeMapper)
    {
        super(operateAPI);
        this.recordTypeMapper = recordTypeMapper;
    }

    @Override
    public List<RecordType> getAll()
    {
        List<RecordType> recordTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllRecordTypes());

        if (response != null)
        {
            JsonNode recordTypesNode = response.get("recordtypes");

            if (recordTypesNode != null)
                recordTypesNode.iterator().forEachRemaining(recordType -> recordTypes.add(recordTypeMapper.fromJson(recordType)));
        }

        return recordTypes;
    }

    @Override
    public RecordType getById(String id)
    {
        RecordType recordType = null;

        JsonNode response = getResponse(operateAPI.getRecordType(id));

        if (response != null)
        {
            JsonNode recordTypeNode = response.get("recordtypes");

            if (recordTypeNode != null)
                recordType = recordTypeMapper.fromJson(recordTypeNode.get(0));
        }

        return recordType;
    }
}
