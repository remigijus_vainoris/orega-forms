package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.GroupAccount;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class GroupAccountServiceImpl extends BaseServiceImpl implements JsonReadService<GroupAccount>
{
    private final Mapper<GroupAccount> groupAccountMapper;

    public GroupAccountServiceImpl(OperateAPI operateAPI, Mapper<GroupAccount> groupAccountMapper)
    {
        super(operateAPI);
        this.groupAccountMapper = groupAccountMapper;
    }

    @Override
    public List<GroupAccount> getAll()
    {
        List<GroupAccount> groupAccounts = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllGroupAccounts());

        if (response != null)
        {
            JsonNode groupAccountsNode = response.get("groupaccounts");

            if (groupAccountsNode != null)
                groupAccountsNode.iterator().forEachRemaining(groupAccount -> groupAccounts.add(groupAccountMapper.fromJson(groupAccount)));
        }

        return groupAccounts;
    }

    @Override
    public GroupAccount getById(String id)
    {
        GroupAccount groupAccount = null;

        JsonNode response = getResponse(operateAPI.getGroupAccount(id));

        if (response != null)
        {
            JsonNode groupAccountNode = response.get("groupaccounts");

            if (groupAccountNode != null)
                groupAccount = groupAccountMapper.fromJson(groupAccountNode.get(0));
        }

        return groupAccount;
    }
}
