package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.Currency;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class CurrencyServiceImpl extends BaseServiceImpl implements JsonReadService<Currency>
{
    private final Mapper<Currency> currencyMapper;

    public CurrencyServiceImpl(OperateAPI operateAPI, Mapper<Currency> currencyMapper)
    {
        super(operateAPI);
        this.currencyMapper = currencyMapper;
    }

    @Override
    public List<Currency> getAll()
    {
        List<Currency> currencies = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllCurrencies());

        if (response != null)
        {
            JsonNode currenciesNode = response.get("currencys");

            if (currenciesNode != null)
                currenciesNode.iterator().forEachRemaining(currency -> currencies.add(currencyMapper.fromJson(currency)));
        }

        return currencies;
    }

    @Override
    public Currency getById(String id)
    {
        Currency currency = null;

        JsonNode response = getResponse(operateAPI.getCurrency(id));

        if (response != null)
        {
            JsonNode currencyNode = response.get("currencys");

            if (currencyNode != null)
                currency = currencyMapper.fromJson(currencyNode.get(0));
        }

        return currency;
    }
}
