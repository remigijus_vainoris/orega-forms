package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.CheckInType;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class CheckInTypeServiceImpl extends BaseServiceImpl implements JsonReadService<CheckInType>
{
    private final Mapper<CheckInType> checkInTypeMapper;

    public CheckInTypeServiceImpl(OperateAPI operateAPI, Mapper<CheckInType> checkInTypeMapper)
    {
        super(operateAPI);
        this.checkInTypeMapper = checkInTypeMapper;
    }

    @Override
    public List<CheckInType> getAll()
    {
        List<CheckInType> checkInTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllCheckInTypes());

        if (response != null)
        {
            JsonNode checkInTypesNode = response.get("lookupcheckintypess");

            if (checkInTypesNode != null)
                checkInTypesNode.iterator().forEachRemaining(checkInType -> checkInTypes.add(checkInTypeMapper.fromJson(checkInType)));
        }

        return checkInTypes;
    }

    @Override
    public CheckInType getById(String id)
    {
        CheckInType checkInType = null;

        JsonNode response = getResponse(operateAPI.getCheckInType(id));

        if (response != null)
        {
            JsonNode checkInTypeNode = response.get("lookupcheckintypess");

            if (checkInTypeNode != null)
                checkInType = checkInTypeMapper.fromJson(checkInTypeNode.get(0));
        }

        return checkInType;
    }
}
