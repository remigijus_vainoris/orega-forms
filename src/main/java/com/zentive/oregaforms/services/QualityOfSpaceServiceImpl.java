package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.QualityOfSpace;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class QualityOfSpaceServiceImpl extends BaseServiceImpl implements JsonReadService<QualityOfSpace>
{
    private final Mapper<QualityOfSpace> qualityOfSpaceMapper;

    public QualityOfSpaceServiceImpl(OperateAPI operateAPI, Mapper<QualityOfSpace> qualityOfSpaceMapper)
    {
        super(operateAPI);
        this.qualityOfSpaceMapper = qualityOfSpaceMapper;
    }

    @Override
    public List<QualityOfSpace> getAll()
    {
        List<QualityOfSpace> qualityOfSpaces = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllQualityOfSpaces());

        if (response != null)
        {
            JsonNode qualityOfSpacesNode = response.get("qualityofspaces");

            if (qualityOfSpacesNode != null)
                qualityOfSpacesNode.iterator().forEachRemaining(qualityOfSpace -> qualityOfSpaces.add(qualityOfSpaceMapper.fromJson(qualityOfSpace)));
        }

        return qualityOfSpaces;
    }

    @Override
    public QualityOfSpace getById(String id)
    {
        QualityOfSpace qualityOfSpace = null;

        JsonNode response = getResponse(operateAPI.getQualityOfSpace(id));

        if (response != null)
        {
            JsonNode qualityOfSpaceNode = response.get("qualityofspaces");

            if (qualityOfSpaceNode != null)
                qualityOfSpace = qualityOfSpaceMapper.fromJson(qualityOfSpaceNode.get(0));
        }

        return qualityOfSpace;
    }
}
