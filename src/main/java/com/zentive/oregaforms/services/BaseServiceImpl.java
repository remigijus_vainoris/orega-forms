package com.zentive.oregaforms.services;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public abstract class BaseServiceImpl
{
    protected final OperateAPI operateAPI;

    protected BaseServiceImpl(OperateAPI operateAPI)
    {
        this.operateAPI = operateAPI;
    }

    protected JsonNode getResponse(JsonNode result)
    {
        return result != null ? result.get("response") : null;
    }
}
