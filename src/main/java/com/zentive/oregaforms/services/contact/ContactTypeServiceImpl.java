package com.zentive.oregaforms.services.contact;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.ContactType;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class ContactTypeServiceImpl extends BaseServiceImpl implements JsonReadService<ContactType>
{
    private final Mapper<ContactType> contactTypeMapper;

    public ContactTypeServiceImpl(OperateAPI operateAPI, Mapper<ContactType> contactTypeMapper)
    {
        super(operateAPI);
        this.contactTypeMapper = contactTypeMapper;
    }

    @Override
    public List<ContactType> getAll()
    {
        List<ContactType> contactTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllContactTypes());

        if (response != null)
        {
            JsonNode contactTypesNode = response.get("contacttypes");

            if (contactTypesNode != null)
                contactTypesNode.iterator().forEachRemaining(contactType -> contactTypes.add(contactTypeMapper.fromJson(contactType)));
        }

        return contactTypes;
    }

    @Override
    public ContactType getById(String id)
    {
        ContactType contactType = null;

        JsonNode response = getResponse(operateAPI.getContactType(id));

        if (response != null)
        {
            JsonNode contactTypeNode = response.get("contacttypes");

            if (contactTypeNode != null)
                contactType = contactTypeMapper.fromJson(contactTypeNode.get(0));
        }

        return contactType;
    }
}
