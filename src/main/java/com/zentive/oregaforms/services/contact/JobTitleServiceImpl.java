package com.zentive.oregaforms.services.contact;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.JobTitle;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class JobTitleServiceImpl extends BaseServiceImpl implements JsonReadService<JobTitle>
{
    private final Mapper<JobTitle> jobTitleMapper;

    public JobTitleServiceImpl(OperateAPI operateAPI, Mapper<JobTitle> jobTitleMapper)
    {
        super(operateAPI);
        this.jobTitleMapper = jobTitleMapper;
    }

    @Override
    public List<JobTitle> getAll()
    {
        List<JobTitle> jobTitles = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllJobTitles());

        if (response != null)
        {
            JsonNode jobTitlesNode = response.get("lookupjobtitless");

            if (jobTitlesNode != null)
                jobTitlesNode.iterator().forEachRemaining(jobTitle -> jobTitles.add(jobTitleMapper.fromJson(jobTitle)));
        }

        return jobTitles;
    }

    @Override
    public JobTitle getById(String id)
    {
        JobTitle jobTitle = null;

        JsonNode response = getResponse(operateAPI.getJobTitle(id));

        if (response != null)
        {
            JsonNode jobTitleNode = response.get("lookupjobtitless");

            if (jobTitleNode != null)
                jobTitle = jobTitleMapper.fromJson(jobTitleNode.get(0));
        }

        return jobTitle;
    }
}
