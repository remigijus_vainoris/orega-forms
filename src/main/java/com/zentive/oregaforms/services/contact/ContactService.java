package com.zentive.oregaforms.services.contact;

import java.util.List;

import com.zentive.oregaforms.models.Contact;
import com.zentive.oregaforms.services.JsonReadService;

public interface ContactService extends JsonReadService<Contact>
{
    List<Contact> getAllByAccount(String accountId);

    List<Contact> getAllByLocation(String locationId);
}
