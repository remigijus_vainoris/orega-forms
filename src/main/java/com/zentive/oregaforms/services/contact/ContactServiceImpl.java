package com.zentive.oregaforms.services.contact;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.Contact;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class ContactServiceImpl extends BaseServiceImpl implements ContactService
{
    private final Mapper<Contact> contactMapper;

    public ContactServiceImpl(OperateAPI operateAPI, Mapper<Contact> contactMapper)
    {
        super(operateAPI);
        this.contactMapper = contactMapper;
    }

    public List<Contact> getAllByAccount(String accountId)
    {
        return getContactsResponse(getResponse(operateAPI.getAllContactsByAccount(accountId)));
    }

    @Override
    public List<Contact> getAllByLocation(String locationId)
    {
        return getContactsResponse(getResponse(operateAPI.getAllContactsByLocation(locationId)));
    }

    private List<Contact> getContactsResponse(JsonNode response)
    {
        List<Contact> contacts = new ArrayList<>();

        if (response != null)
        {
            JsonNode contactsNode = response.get("contacts");

            if (contactsNode != null)
                contactsNode.iterator().forEachRemaining(contact -> contacts.add(contactMapper.fromJson(contact)));
        }

        return contacts;
    }

    @Override
    public List<Contact> getAll()
    {
        return getContactsResponse(getResponse(operateAPI.getAllContacts()));
    }

    @Override
    public Contact getById(String id)
    {
        Contact contact = null;

        JsonNode response = getResponse(operateAPI.getContact(id));

        if (response != null)
        {
            JsonNode contactNode = response.get("contacts");

            if (contactNode != null)
                contact = contactMapper.fromJson(contactNode.get(0));
        }

        return contact;
    }
}
