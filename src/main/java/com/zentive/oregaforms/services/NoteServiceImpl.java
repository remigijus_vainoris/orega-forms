package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.Note;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class NoteServiceImpl extends BaseServiceImpl implements JsonReadService<Note>
{
    private final Mapper<Note> noteMapper;

    public NoteServiceImpl(OperateAPI operateAPI, Mapper<Note> noteMapper)
    {
        super(operateAPI);
        this.noteMapper = noteMapper;
    }

    @Override
    public List<Note> getAll()
    {
        List<Note> notes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllNotes());

        if (response != null)
        {
            JsonNode notesNode = response.get("notes");

            if (notesNode != null)
                notesNode.iterator().forEachRemaining(note -> notes.add(noteMapper.fromJson(note)));
        }

        return notes;
    }

    @Override
    public Note getById(String id)
    {
        Note note = null;

        JsonNode response = getResponse(operateAPI.getNote(id));

        if (response != null)
        {
            JsonNode noteNode = response.get("notes");

            if (noteNode != null)
                note = noteMapper.fromJson(noteNode.get(0));
        }

        return note;
    }
}
