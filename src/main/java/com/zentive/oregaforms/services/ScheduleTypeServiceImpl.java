package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.ScheduleType;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class ScheduleTypeServiceImpl extends BaseServiceImpl implements JsonReadService<ScheduleType>
{
    private final Mapper<ScheduleType> scheduleTypeMapper;

    public ScheduleTypeServiceImpl(OperateAPI operateAPI, Mapper<ScheduleType> scheduleTypeMapper)
    {
        super(operateAPI);
        this.scheduleTypeMapper = scheduleTypeMapper;
    }

    @Override
    public List<ScheduleType> getAll()
    {
        List<ScheduleType> scheduleTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllScheduleTypes());

        if (response != null)
        {
            JsonNode scheduleTypesNode = response.get("scheduletypes");

            if (scheduleTypesNode != null)
                scheduleTypesNode.iterator().forEachRemaining(scheduleType -> scheduleTypes.add(scheduleTypeMapper.fromJson(scheduleType)));
        }

        return scheduleTypes;
    }

    @Override
    public ScheduleType getById(String id)
    {
        ScheduleType scheduleType = null;

        JsonNode response = getResponse(operateAPI.getScheduleType(id));

        if (response != null)
        {
            JsonNode scheduleTypeNode = response.get("scheduletypes");

            if (scheduleTypeNode != null)
                scheduleType = scheduleTypeMapper.fromJson(scheduleTypeNode.get(0));
        }

        return scheduleType;
    }
}
