package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.County;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class CountyServiceImpl extends BaseServiceImpl implements JsonReadService<County>
{
    private final Mapper<County> countyMapper;

    public CountyServiceImpl(OperateAPI operateAPI, Mapper<County> countyMapper)
    {
        super(operateAPI);
        this.countyMapper = countyMapper;
    }

    @Override
    public List<County> getAll()
    {
        List<County> counties = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllCounties());

        if (response != null)
        {
            JsonNode countiesNode = response.get("lookupcountiess");

            if (countiesNode != null)
                countiesNode.iterator().forEachRemaining(county -> counties.add(countyMapper.fromJson(county)));
        }

        return counties;
    }

    @Override
    public County getById(String id)
    {
        County county = null;

        JsonNode response = getResponse(operateAPI.getCounty(id));

        if (response != null)
        {
            JsonNode countyNode = response.get("lookupcountiess");

            if (countyNode != null)
                county = countyMapper.fromJson(countyNode.get(0));
        }

        return county;
    }
}
