package com.zentive.oregaforms.services.invoice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.Invoice;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class InvoiceServiceImpl extends BaseServiceImpl implements InvoiceService
{
    private final Mapper<Invoice> invoiceMapper;

    public InvoiceServiceImpl(OperateAPI operateAPI, Mapper<Invoice> invoiceMapper)
    {
        super(operateAPI);
        this.invoiceMapper = invoiceMapper;
    }

    @Override
    public List<Invoice> getAllByAccount(String customerId)
    {
        return getInvoicesFromResponse(getResponse(operateAPI.getAllInvoicesByAccount(customerId)));
    }

    @Override
    public List<Invoice> getAll()
    {
        return getInvoicesFromResponse(getResponse(operateAPI.getAllInvoices()));
    }

    @Override
    public Invoice getById(String id)
    {
        Invoice invoice = null;

        JsonNode response = getResponse(operateAPI.getInvoice(id));

        if (response != null)
        {
            JsonNode invoiceNode = response.get("invoices");

            if (invoiceNode != null)
                invoice = invoiceMapper.fromJson(invoiceNode.get(0));
        }

        return invoice;
    }

    private List<Invoice> getInvoicesFromResponse(JsonNode response)
    {
        List<Invoice> invoices = new ArrayList<>();

        if (response != null)
        {
            JsonNode invoicesNode = response.get("invoices");

            if (invoicesNode != null)
                invoicesNode.iterator().forEachRemaining(invoice -> invoices.add(invoiceMapper.fromJson(invoice)));
        }

        return invoices;
    }

}
