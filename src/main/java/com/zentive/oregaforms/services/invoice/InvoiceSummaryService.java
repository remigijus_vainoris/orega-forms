package com.zentive.oregaforms.services.invoice;

import java.util.List;

import com.zentive.oregaforms.models.InvoiceSummary;
import com.zentive.oregaforms.services.JsonReadService;

public interface InvoiceSummaryService extends JsonReadService<InvoiceSummary>
{
    List<InvoiceSummary> getAllByInvoice(String invoiceId);
}
