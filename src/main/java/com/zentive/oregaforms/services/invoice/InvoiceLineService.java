package com.zentive.oregaforms.services.invoice;

import java.util.List;

import com.zentive.oregaforms.models.InvoiceLine;
import com.zentive.oregaforms.services.JsonReadService;

public interface InvoiceLineService extends JsonReadService<InvoiceLine>
{
    List<InvoiceLine> getAllByInvoice(String invoiceId);

    List<InvoiceLine> getAllByLocation(String locationId);

    List<InvoiceLine> getAllByAccount(String accountId);
}
