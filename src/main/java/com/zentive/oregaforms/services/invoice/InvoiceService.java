package com.zentive.oregaforms.services.invoice;

import java.util.List;

import com.zentive.oregaforms.models.Invoice;
import com.zentive.oregaforms.services.JsonReadService;

public interface InvoiceService extends JsonReadService<Invoice>
{
    List<Invoice> getAllByAccount(String customerId);
}
