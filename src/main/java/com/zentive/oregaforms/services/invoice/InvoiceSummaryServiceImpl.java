package com.zentive.oregaforms.services.invoice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.InvoiceSummary;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class InvoiceSummaryServiceImpl extends BaseServiceImpl implements InvoiceSummaryService
{
    private final Mapper<InvoiceSummary> invoiceSummaryMapper;

    public InvoiceSummaryServiceImpl(OperateAPI operateAPI, Mapper<InvoiceSummary> invoiceSummaryMapper)
    {
        super(operateAPI);
        this.invoiceSummaryMapper = invoiceSummaryMapper;
    }

    @Override
    public List<InvoiceSummary> getAllByInvoice(String invoiceId)
    {
        return getInvoiceSummariesFromResponse(getResponse(operateAPI.getAllInvoiceSummariesByInvoice(invoiceId)));
    }

    @Override
    public List<InvoiceSummary> getAll()
    {
        return getInvoiceSummariesFromResponse(getResponse(operateAPI.getAllInvoiceSummaries()));
    }

    @Override
    public InvoiceSummary getById(String id)
    {
        InvoiceSummary invoiceSummary = null;

        JsonNode response = getResponse(operateAPI.getInvoiceSummary(id));

        if (response != null)
        {
            JsonNode invoiceSummaryNode = response.get("invoicesummarys");

            if (invoiceSummaryNode != null)
                invoiceSummary = invoiceSummaryMapper.fromJson(invoiceSummaryNode.get(0));
        }

        return invoiceSummary;
    }

    private List<InvoiceSummary> getInvoiceSummariesFromResponse(JsonNode response)
    {
        List<InvoiceSummary> invoiceSummaries = new ArrayList<>();

        if (response != null)
        {
            JsonNode invoiceSummariesNode = response.get("invoicesummarys");

            if (invoiceSummariesNode != null)
                invoiceSummariesNode.iterator().forEachRemaining(invoiceSummary -> invoiceSummaries.add(invoiceSummaryMapper.fromJson(invoiceSummary)));
        }

        return invoiceSummaries;
    }
}
