package com.zentive.oregaforms.services.invoice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.InvoiceNumber;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class InvoiceNumberServiceImpl extends BaseServiceImpl implements JsonReadService<InvoiceNumber>
{
    private final Mapper<InvoiceNumber> invoiceNumberMapper;

    public InvoiceNumberServiceImpl(OperateAPI operateAPI, Mapper<InvoiceNumber> invoiceNumberMapper)
    {
        super(operateAPI);
        this.invoiceNumberMapper = invoiceNumberMapper;
    }

    @Override
    public List<InvoiceNumber> getAll()
    {
        List<InvoiceNumber> invoiceNumbers = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllInvoiceNumbers());

        if (response != null)
        {
            JsonNode invoiceNumbersNode = response.get("invoicenumbers");

            if (invoiceNumbersNode != null)
                invoiceNumbersNode.iterator().forEachRemaining(invoiceNumber -> invoiceNumbers.add(invoiceNumberMapper.fromJson(invoiceNumber)));
        }

        return invoiceNumbers;
    }

    @Override
    public InvoiceNumber getById(String id)
    {
        InvoiceNumber invoiceNumber = null;

        JsonNode response = getResponse(operateAPI.getInvoiceNumber(id));

        if (response != null)
        {
            JsonNode invoiceNumberNode = response.get("invoicenumbers");

            if (invoiceNumberNode != null)
                invoiceNumber = invoiceNumberMapper.fromJson(invoiceNumberNode.get(0));
        }

        return invoiceNumber;
    }
}
