package com.zentive.oregaforms.services.invoice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.InvoiceLine;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class InvoiceLineServiceImpl extends BaseServiceImpl implements InvoiceLineService
{
    private final Mapper<InvoiceLine> invoiceLineMapper;

    public InvoiceLineServiceImpl(OperateAPI operateAPI, Mapper<InvoiceLine> invoiceLineMapper)
    {
        super(operateAPI);
        this.invoiceLineMapper = invoiceLineMapper;
    }

    @Override
    public List<InvoiceLine> getAllByInvoice(String invoiceId)
    {
        return getInvoiceLinesFromResponse(getResponse(operateAPI.getAllInvoiceLinesByInvoice(invoiceId)));
    }

    @Override
    public List<InvoiceLine> getAllByLocation(String locationId)
    {
        return getInvoiceLinesFromResponse(getResponse(operateAPI.getAllInvoiceLinesByLocation(locationId)));
    }

    @Override
    public List<InvoiceLine> getAllByAccount(String accountId)
    {
        return getInvoiceLinesFromResponse(getResponse(operateAPI.getAllInvoiceLinesByAccount(accountId)));
    }

    @Override
    public List<InvoiceLine> getAll()
    {
        return getInvoiceLinesFromResponse(getResponse(operateAPI.getAllInvoiceLines()));
    }

    @Override
    public InvoiceLine getById(String id)
    {
        InvoiceLine invoiceLine = null;

        JsonNode response = getResponse(operateAPI.getAllInvoiceLine(id));

        if (response != null)
        {
            JsonNode invoiceLineNode = response.get("invoicelines");

            if (invoiceLineNode != null)
                invoiceLine = invoiceLineMapper.fromJson(invoiceLineNode.get(0));
        }

        return invoiceLine;
    }

    private List<InvoiceLine> getInvoiceLinesFromResponse(JsonNode response)
    {
        List<InvoiceLine> invoiceLines = new ArrayList<>();

        if (response != null)
        {
            JsonNode invoiceLinesNode = response.get("invoicelines");

            if (invoiceLinesNode != null)
                invoiceLinesNode.iterator().forEachRemaining(invoiceLine -> invoiceLines.add(invoiceLineMapper.fromJson(invoiceLine)));
        }

        return invoiceLines;
    }
}
