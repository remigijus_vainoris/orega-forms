package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.OnlinePlan;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class OnlinePlanServiceImpl extends BaseServiceImpl implements JsonReadService<OnlinePlan>
{
    private final Mapper<OnlinePlan> onlinePlanMapper;

    public OnlinePlanServiceImpl(OperateAPI operateAPI, Mapper<OnlinePlan> onlinePlanMapper)
    {
        super(operateAPI);
        this.onlinePlanMapper = onlinePlanMapper;
    }

    @Override
    public List<OnlinePlan> getAll()
    {
        List<OnlinePlan> onlinePlans = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllOnlinePlans());

        if (response != null)
        {
            JsonNode onlinePlansNode = response.get("onlineplans");

            if (onlinePlansNode != null)
                onlinePlansNode.iterator().forEachRemaining(onlinePlan -> onlinePlans.add(onlinePlanMapper.fromJson(onlinePlan)));
        }

        return onlinePlans;
    }

    @Override
    public OnlinePlan getById(String id)
    {
        OnlinePlan onlinePlan = null;

        JsonNode response = getResponse(operateAPI.getOnlinePlan(id));

        if (response != null)
        {
            JsonNode onlinePlanNode = response.get("onlineplans");

            if (onlinePlanNode != null)
                onlinePlan = onlinePlanMapper.fromJson(onlinePlanNode.get(0));
        }

        return onlinePlan;
    }
}
