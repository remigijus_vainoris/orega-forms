package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.AgreementType;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class AgreementTypeServiceImpl extends BaseServiceImpl implements JsonReadService<AgreementType>
{
    private final Mapper<AgreementType> agreementTypeMapper;

    public AgreementTypeServiceImpl(OperateAPI operateAPI, Mapper<AgreementType> agreementTypeMapper)
    {
        super(operateAPI);
        this.agreementTypeMapper = agreementTypeMapper;
    }

    @Override
    public List<AgreementType> getAll()
    {
        List<AgreementType> agreementTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllAgreementTypes());

        if (response != null)
        {
            JsonNode agreementTypesNode = response.get("lookupagreementtypess");

            if (agreementTypesNode != null)
                agreementTypesNode.iterator().forEachRemaining(agreementType -> agreementTypes.add(agreementTypeMapper.fromJson(agreementType)));
        }

        return agreementTypes;
    }

    @Override
    public AgreementType getById(String id)
    {
        AgreementType agreementType = null;

        JsonNode response = getResponse(operateAPI.getAgreementType(id));

        if (response != null)
        {
            JsonNode agreementTypeNode = response.get("lookupagreementtypess");

            if (agreementTypeNode != null)
                agreementType = agreementTypeMapper.fromJson(agreementTypeNode.get(0));
        }

        return agreementType;
    }
}
