package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.CheckOutType;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class CheckOutTypeServiceImpl extends BaseServiceImpl implements JsonReadService<CheckOutType>
{
    private final Mapper<CheckOutType> checkOutTypeMapper;

    public CheckOutTypeServiceImpl(OperateAPI operateAPI, Mapper<CheckOutType> checkOutTypeMapper)
    {
        super(operateAPI);
        this.checkOutTypeMapper = checkOutTypeMapper;
    }

    @Override
    public List<CheckOutType> getAll()
    {
        List<CheckOutType> checkOutTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllCheckOutTypes());

        if (response != null)
        {
            JsonNode checkOutTypesNode = response.get("lookupcheckouttypess");

            if (checkOutTypesNode != null)
                checkOutTypesNode.iterator().forEachRemaining(checkOutType -> checkOutTypes.add(checkOutTypeMapper.fromJson(checkOutType)));
        }

        return checkOutTypes;
    }

    @Override
    public CheckOutType getById(String id)
    {
        CheckOutType checkOutType = null;

        JsonNode response = getResponse(operateAPI.getCheckOutType(id));

        if (response != null)
        {
            JsonNode checkOutTypeNode = response.get("lookupcheckouttypess");

            if (checkOutTypeNode != null)
                checkOutType = checkOutTypeMapper.fromJson(checkOutTypeNode.get(0));
        }

        return checkOutType;
    }
}
