package com.zentive.oregaforms.services.organization;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.GLCode;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class GLCodeServiceImpl extends BaseServiceImpl implements GLCodeService
{
    private final Mapper<GLCode> glCodeMapper;

    public GLCodeServiceImpl(OperateAPI operateAPI, Mapper<GLCode> glCodeMapper)
    {
        super(operateAPI);
        this.glCodeMapper = glCodeMapper;
    }

    @Override
    public List<GLCode> getAllByOrganization(String organizationId)
    {
        return getGLCodesFromResponse(getResponse(operateAPI.getAllGLCodesByOrganization(organizationId)));
    }

    @Override
    public List<GLCode> getAll()
    {
        return getGLCodesFromResponse(getResponse(operateAPI.getAllGLCodes()));
    }

    @Override
    public GLCode getById(String id)
    {
        GLCode glCode = null;

        JsonNode response = getResponse(operateAPI.getGLCode(id));

        if (response != null)
        {
            JsonNode glCodeNode = response.get("glcodes");

            if (glCodeNode != null)
                glCode = glCodeMapper.fromJson(glCodeNode.get(0));
        }

        return glCode;
    }

    private List<GLCode> getGLCodesFromResponse(JsonNode response)
    {
        List<GLCode> glCodes = new ArrayList<>();

        if (response != null)
        {
            JsonNode glCodesNode = response.get("glcodes");

            if (glCodesNode != null)
                glCodesNode.iterator().forEachRemaining(glCode -> glCodes.add(glCodeMapper.fromJson(glCode)));
        }

        return glCodes;
    }
}
