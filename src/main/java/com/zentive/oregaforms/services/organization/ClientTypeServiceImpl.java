package com.zentive.oregaforms.services.organization;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.ClientType;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class ClientTypeServiceImpl extends BaseServiceImpl implements ClientTypeService
{
    private final Mapper<ClientType> clientTypeMapper;

    public ClientTypeServiceImpl(OperateAPI operateAPI, Mapper<ClientType> clientTypeMapper)
    {
        super(operateAPI);
        this.clientTypeMapper = clientTypeMapper;
    }

    @Override
    public List<ClientType> getAllByOrganization(String organizationId)
    {
        return getClientTypesFromResponse(getResponse(operateAPI.getAllClientTypesByOrganization(organizationId)));
    }

    @Override
    public List<ClientType> getAll()
    {
        return getClientTypesFromResponse(getResponse(operateAPI.getAllClientTypes()));
    }

    @Override
    public ClientType getById(String id)
    {
        ClientType clientType = null;

        JsonNode response = getResponse(operateAPI.getClientType(id));

        if (response != null)
        {
            JsonNode clientTypeNode = response.get("clienttypes");

            if (clientTypeNode != null)
                clientType = clientTypeMapper.fromJson(clientTypeNode.get(0));
        }

        return clientType;
    }

    private List<ClientType> getClientTypesFromResponse(JsonNode response)
    {
        List<ClientType> clientTypes = new ArrayList<>();

        if (response != null)
        {
            JsonNode clientTypesNode = response.get("clienttypes");

            if (clientTypesNode != null)
                clientTypesNode.iterator().forEachRemaining(clientType -> clientTypes.add(clientTypeMapper.fromJson(clientType)));
        }

        return clientTypes;
    }
}
