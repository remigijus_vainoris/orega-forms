package com.zentive.oregaforms.services.organization;

import java.util.List;

import com.zentive.oregaforms.models.ClientType;
import com.zentive.oregaforms.services.JsonReadService;

public interface ClientTypeService extends JsonReadService<ClientType>
{
    List<ClientType> getAllByOrganization(String organizationId);
}
