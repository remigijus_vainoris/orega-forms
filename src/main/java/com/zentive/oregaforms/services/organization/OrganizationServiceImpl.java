package com.zentive.oregaforms.services.organization;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.Organization;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class OrganizationServiceImpl extends BaseServiceImpl implements JsonReadService<Organization>
{
    private final Mapper<Organization> organizationMapper;

    public OrganizationServiceImpl(OperateAPI operateAPI, Mapper<Organization> organizationMapper)
    {
        super(operateAPI);
        this.organizationMapper = organizationMapper;
    }

    @Override
    public List<Organization> getAll()
    {
        List<Organization> organizations = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllOrganizations());

        if (response != null)
        {
            JsonNode organizationsNode = response.get("organizations");

            if (organizationsNode != null)
                organizationsNode.iterator().forEachRemaining(organization -> organizations.add(organizationMapper.fromJson(organization)));
        }

        return organizations;
    }

    @Override
    public Organization getById(String id)
    {
        Organization organization = null;

        JsonNode response = getResponse(operateAPI.getOrganization(id));

        if (response != null)
        {
            JsonNode organizationNode = response.get("organizations");

            if (organizationNode != null)
                organization = organizationMapper.fromJson(organizationNode.get(0));
        }

        return organization;
    }
}
