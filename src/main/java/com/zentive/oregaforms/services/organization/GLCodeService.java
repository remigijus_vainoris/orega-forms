package com.zentive.oregaforms.services.organization;

import java.util.List;

import com.zentive.oregaforms.models.GLCode;
import com.zentive.oregaforms.services.JsonReadService;

public interface GLCodeService extends JsonReadService<GLCode>
{
    List<GLCode> getAllByOrganization(String organizationId);
}
