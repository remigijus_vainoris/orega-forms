package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.Town;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class TownServiceImpl extends BaseServiceImpl implements JsonReadService<Town>
{
    private final Mapper<Town> townMapper;

    public TownServiceImpl(OperateAPI operateAPI, Mapper<Town> townMapper)
    {
        super(operateAPI);
        this.townMapper = townMapper;
    }

    @Override
    public List<Town> getAll()
    {
        List<Town> towns = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllTowns());

        if (response != null)
        {
            JsonNode townsNode = response.get("lookuptownss");

            if (townsNode != null)
                townsNode.iterator().forEachRemaining(town -> towns.add(townMapper.fromJson(town)));
        }

        return towns;
    }

    @Override
    public Town getById(String id)
    {
        Town town = null;

        JsonNode response = getResponse(operateAPI.getTown(id));

        if (response != null)
        {
            JsonNode townNode = response.get("lookuptownss");

            if (townNode != null)
                town = townMapper.fromJson(townNode.get(0));
        }

        return town;
    }
}
