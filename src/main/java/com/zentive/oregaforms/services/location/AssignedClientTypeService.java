package com.zentive.oregaforms.services.location;

import java.util.List;

import com.zentive.oregaforms.models.AssignedClientType;
import com.zentive.oregaforms.services.JsonReadService;

public interface AssignedClientTypeService extends JsonReadService<AssignedClientType>
{
    List<AssignedClientType> getAllByLocation(String locationId);
}
