package com.zentive.oregaforms.services.location;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.AssignedClientType;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class AssignedClientTypeServiceImpl extends BaseServiceImpl implements AssignedClientTypeService
{
    private final Mapper<AssignedClientType> assignedClientTypeMapper;

    public AssignedClientTypeServiceImpl(OperateAPI operateAPI, Mapper<AssignedClientType> assignedClientTypeMapper)
    {
        super(operateAPI);

        this.assignedClientTypeMapper = assignedClientTypeMapper;
    }

    @Override
    public List<AssignedClientType> getAllByLocation(String locationId)
    {

        List<AssignedClientType> assignedClientTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllAssignedClientTypesByLocation(locationId));

        if (response != null)
        {
            JsonNode assignedClientTypesNode = response.get("assignedclienttypes");

            if (assignedClientTypesNode != null)
                assignedClientTypesNode.iterator().forEachRemaining(assignedClientType -> assignedClientTypes.add(assignedClientTypeMapper.fromJson(assignedClientType)));
        }

        return assignedClientTypes;
    }

    @Override
    public List<AssignedClientType> getAll()
    {
        return null;
    }

    @Override
    public AssignedClientType getById(String id)
    {
        return null;
    }
}
