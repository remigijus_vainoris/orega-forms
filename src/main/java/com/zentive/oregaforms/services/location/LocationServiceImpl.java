package com.zentive.oregaforms.services.location;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.AssignedClientType;
import com.zentive.oregaforms.models.Location;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class LocationServiceImpl extends BaseServiceImpl implements LocationService
{
    private final Mapper<Location> locationMapper;
    private final Mapper<AssignedClientType> assignedClientTypeMapper;

    public LocationServiceImpl(OperateAPI operateAPI, Mapper<Location> locationMapper, Mapper<AssignedClientType> assignedClientTypeMapper)
    {
        super(operateAPI);
        this.locationMapper = locationMapper;
        this.assignedClientTypeMapper = assignedClientTypeMapper;
    }

    @Override
    public List<Location> getAll()
    {
        List<Location> locations = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllLocations());

        if (response != null)
        {
            JsonNode locationsNode = response.get("locations");

            if (locationsNode != null)
                locationsNode.iterator().forEachRemaining(location -> locations.add(locationMapper.fromJson(location)));
        }

        return locations;
    }

    @Override
    public Location getById(String id)
    {
        Location location = null;

        JsonNode response = getResponse(operateAPI.getLocation(id));

        if (response != null)
        {
            JsonNode locationNode = response.get("locations");

            if (locationNode != null)
                location = locationMapper.fromJson(locationNode.get(0));
        }

        return location;
    }

}
