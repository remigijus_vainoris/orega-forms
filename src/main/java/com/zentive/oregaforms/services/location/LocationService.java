package com.zentive.oregaforms.services.location;

import com.zentive.oregaforms.models.Location;
import com.zentive.oregaforms.services.JsonReadService;

public interface LocationService extends JsonReadService<Location>
{
}