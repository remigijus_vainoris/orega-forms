package com.zentive.oregaforms.services.location;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.Region;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class RegionServiceImpl extends BaseServiceImpl implements JsonReadService<Region>
{
    private final Mapper<Region> regionMapper;

    public RegionServiceImpl(OperateAPI operateAPI, Mapper<Region> regionMapper)
    {
        super(operateAPI);
        this.regionMapper = regionMapper;
    }

    @Override
    public List<Region> getAll()
    {
        List<Region> regions = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllRegions());

        if (response != null)
        {
            JsonNode regionsNode = response.get("regions");

            if (regionsNode != null)
                regionsNode.iterator().forEachRemaining(region -> regions.add(regionMapper.fromJson(region)));
        }

        return regions;
    }

    @Override
    public Region getById(String id)
    {
        Region region = null;

        JsonNode response = getResponse(operateAPI.getRegion(id));

        if (response != null)
        {
            JsonNode regionNode = response.get("regions");

            if (regionNode != null)
                region = regionMapper.fromJson(regionNode.get(0));
        }

        return region;
    }
}
