package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.SetupChecklist;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class SetupChecklistServiceImpl extends BaseServiceImpl implements JsonReadService<SetupChecklist>
{
    private final Mapper<SetupChecklist> setupChecklistMapper;

    public SetupChecklistServiceImpl(OperateAPI operateAPI, Mapper<SetupChecklist> setupChecklistMapper)
    {
        super(operateAPI);
        this.setupChecklistMapper = setupChecklistMapper;
    }

    @Override
    public List<SetupChecklist> getAll()
    {
        List<SetupChecklist> setupChecklists = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllSetupChecklists());

        if (response != null)
        {
            JsonNode setupChecklistsNode = response.get("setupchecklists");

            if (setupChecklistsNode != null)
                setupChecklistsNode.iterator().forEachRemaining(setupChecklist -> setupChecklists.add(setupChecklistMapper.fromJson(setupChecklist)));
        }

        return setupChecklists;
    }

    @Override
    public SetupChecklist getById(String id)
    {
        SetupChecklist setupChecklist = null;

        JsonNode response = getResponse(operateAPI.getSetupChecklist(id));

        if (response != null)
        {
            JsonNode setupChecklistNode = response.get("setupchecklists");

            if (setupChecklistNode != null)
                setupChecklist = setupChecklistMapper.fromJson(setupChecklistNode.get(0));
        }

        return setupChecklist;
    }
}
