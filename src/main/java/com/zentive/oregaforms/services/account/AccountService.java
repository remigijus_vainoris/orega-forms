package com.zentive.oregaforms.services.account;

import java.util.List;

import com.zentive.oregaforms.models.Account;

public interface AccountService
{
    Account getById(String id);

    List<Account> getAllByLocation(String locationId);
}