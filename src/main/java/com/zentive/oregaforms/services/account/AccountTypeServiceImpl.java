package com.zentive.oregaforms.services.account;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.AccountType;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;
import com.zentive.oregaforms.services.JsonReadService;

@Service
public class AccountTypeServiceImpl extends BaseServiceImpl implements JsonReadService<AccountType>
{
    private final Mapper<AccountType> accountTypeMapper;

    public AccountTypeServiceImpl(OperateAPI operateAPI, Mapper<AccountType> accountTypeMapper)
    {
        super(operateAPI);
        this.accountTypeMapper = accountTypeMapper;
    }

    @Override
    public List<AccountType> getAll()
    {
        List<AccountType> accountTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllAccountTypes());

        if (response != null)
        {
            JsonNode accountTypesNode = response.get("accounttypes");

            if (accountTypesNode != null)
                accountTypesNode.iterator().forEachRemaining(accountType -> accountTypes.add(accountTypeMapper.fromJson(accountType)));
        }

        return accountTypes;
    }

    @Override
    public AccountType getById(String id)
    {
        AccountType accountType = null;

        JsonNode response = getResponse(operateAPI.getAccountType(id));

        if (response != null)
        {
            JsonNode accountTypeNode = response.get("accounttypes");

            if (accountTypeNode != null)
                accountType = accountTypeMapper.fromJson(accountTypeNode.get(0));
        }

        return accountType;
    }
}
