package com.zentive.oregaforms.services.account;

import java.util.List;

import com.zentive.oregaforms.models.AccountCode;
import com.zentive.oregaforms.services.JsonReadService;

public interface AccountCodeService extends JsonReadService<AccountCode>
{
    List<AccountCode> getAllByAccount(String accountId);
}
