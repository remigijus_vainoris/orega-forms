package com.zentive.oregaforms.services.account;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.AccountCode;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

@Service
public class AccountCodeServiceImpl extends BaseServiceImpl implements AccountCodeService
{
    private final Mapper<AccountCode> accountCodeMapper;

    public AccountCodeServiceImpl(OperateAPI operateAPI, Mapper<AccountCode> accountCodeMapper)
    {
        super(operateAPI);
        this.accountCodeMapper = accountCodeMapper;
    }

    @Override
    public List<AccountCode> getAllByAccount(String accountId)
    {
        return getAccountCodesFromResponse(getResponse(operateAPI.getAccountCodesByAccount(accountId)));

    }

    @Override
    public List<AccountCode> getAll()
    {
        return getAccountCodesFromResponse(getResponse(operateAPI.getAllAccountCodes()));
    }

    @Override
    public AccountCode getById(String id)
    {
        AccountCode accountCode = null;

        JsonNode response = getResponse(operateAPI.getAccountCode(id));

        if (response != null)
        {
            JsonNode accountCodeNode = response.get("accountcodes");

            if (accountCodeNode != null)
                accountCode = accountCodeMapper.fromJson(accountCodeNode.get(0));
        }

        return accountCode;
    }

    private List<AccountCode> getAccountCodesFromResponse(JsonNode response)
    {
        List<AccountCode> accountCodes = new ArrayList<>();

        if (response != null)
        {
            JsonNode accountCodesNode = response.get("accountcodes");

            if (accountCodesNode != null)
                accountCodesNode.iterator().forEachRemaining(accountCode -> accountCodes.add(accountCodeMapper.fromJson(accountCode)));
        }

        return accountCodes;
    }
}
