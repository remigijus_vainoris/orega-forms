package com.zentive.oregaforms.services.account;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.Account;
import com.zentive.oregaforms.models.AccountCode;
import com.zentive.oregaforms.operate.OperateAPI;
import com.zentive.oregaforms.services.BaseServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AccountServiceImpl extends BaseServiceImpl implements AccountService
{
    private final Mapper<AccountCode> accountCodeMapper;
    private final Mapper<Account> accountMapper;

    public AccountServiceImpl(OperateAPI operateAPI, Mapper<AccountCode> accountCodeMapper, Mapper<Account> accountMapper)
    {
        super(operateAPI);
        this.accountCodeMapper = accountCodeMapper;
        this.accountMapper = accountMapper;
    }

    @Override
    public List<Account> getAllByLocation(String locationId)
    {
        List<Account> accounts = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllAccountsByLocation(locationId));

        if (response != null)
        {
            JsonNode accountsNode = response.get("accounts");

            if (accountsNode != null)
                accountsNode.iterator().forEachRemaining(account -> accounts.add(accountMapper.fromJson(account)));
        }

        return accounts;
    }

    @Override
    public Account getById(String id)
    {
        Account account = null;

        JsonNode response = getResponse(operateAPI.getAccount(id));

        if (response != null)
        {
            JsonNode accountNode = response.get("accounts");

            if (accountNode != null)
                account = accountMapper.fromJson(accountNode.get(0));
        }

        return account;
    }
}
