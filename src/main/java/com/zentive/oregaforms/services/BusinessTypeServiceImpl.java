package com.zentive.oregaforms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.mappers.Mapper;
import com.zentive.oregaforms.models.BusinessType;
import com.zentive.oregaforms.operate.OperateAPI;

@Service
public class BusinessTypeServiceImpl extends BaseServiceImpl implements JsonReadService<BusinessType>
{
    private final Mapper<BusinessType> businessTypeMapper;

    public BusinessTypeServiceImpl(OperateAPI operateAPI, Mapper<BusinessType> businessTypeMapper)
    {
        super(operateAPI);
        this.businessTypeMapper = businessTypeMapper;
    }

    @Override
    public List<BusinessType> getAll()
    {
        List<BusinessType> businessTypes = new ArrayList<>();

        JsonNode response = getResponse(operateAPI.getAllBusinessTypes());

        if (response != null)
        {
            JsonNode businessTypesNode = response.get("businesstypes");

            if (businessTypesNode != null)
                businessTypesNode.iterator().forEachRemaining(businessType -> businessTypes.add(businessTypeMapper.fromJson(businessType)));
        }

        return businessTypes;
    }

    @Override
    public BusinessType getById(String id)
    {
        BusinessType businessType = null;

        JsonNode response = getResponse(operateAPI.getBusinessType(id));

        if (response != null)
        {
            JsonNode businessTypeNode = response.get("businesstypes");

            if (businessTypeNode != null)
                businessType = businessTypeMapper.fromJson(businessTypeNode.get(0));
        }

        return businessType;
    }
}
