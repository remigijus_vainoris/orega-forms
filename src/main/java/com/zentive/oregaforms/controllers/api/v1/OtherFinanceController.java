package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.models.CreditCard;
import com.zentive.oregaforms.models.Currency;
import com.zentive.oregaforms.models.ExchangeRate;
import com.zentive.oregaforms.models.OnlineTransaction;
import com.zentive.oregaforms.services.JsonReadService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class OtherFinanceController extends BaseController
{
    private final JsonReadService<CreditCard> creditCardService;
    private final JsonReadService<Currency> currencyService;
    private final JsonReadService<ExchangeRate> exchangeRateService;
    private final JsonReadService<OnlineTransaction> onlineTransactionService;

    public OtherFinanceController(JsonReadService<CreditCard> creditCardService, JsonReadService<Currency> currencyService, JsonReadService<ExchangeRate> exchangeRateService,
            JsonReadService<OnlineTransaction> onlineTransactionService)
    {
        this.creditCardService = creditCardService;
        this.currencyService = currencyService;
        this.exchangeRateService = exchangeRateService;
        this.onlineTransactionService = onlineTransactionService;
    }

    @GetMapping("/creditCards")
    public ResponseEntity<?> getCreditCards(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditCardService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/creditCards/{id}")
    public ResponseEntity<?> getCreditCard(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditCardService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/currencies")
    public ResponseEntity<?> getCurrencies(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(currencyService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/currencies/{id}")
    public ResponseEntity<?> getCurrency(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(currencyService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/exchangeRates")
    public ResponseEntity<?> getExchangeRates(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(exchangeRateService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/exchangeRates/{id}")
    public ResponseEntity<?> getExchangeRate(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(exchangeRateService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/onlineTransactions")
    public ResponseEntity<?> getOnlineTransactions(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(onlineTransactionService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/onlineTransactions/{id}")
    public ResponseEntity<?> getOnlineTransaction(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(onlineTransactionService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
