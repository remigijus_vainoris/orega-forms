package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.models.CreditNoteType;
import com.zentive.oregaforms.services.JsonReadService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/creditNoteTypes")
public class CreditNoteTypeController extends BaseController
{
    private final JsonReadService<CreditNoteType> creditNoteTypeService;

    public CreditNoteTypeController(JsonReadService<CreditNoteType> creditNoteTypeService)
    {
        this.creditNoteTypeService = creditNoteTypeService;
    }

    @GetMapping
    public ResponseEntity<?> getCreditNoteTypes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditNoteTypeService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCreditNoteType(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditNoteTypeService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
