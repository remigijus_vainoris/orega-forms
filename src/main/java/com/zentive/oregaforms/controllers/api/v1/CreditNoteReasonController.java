package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.models.CreditNoteReason;
import com.zentive.oregaforms.services.JsonReadService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/creditNoteReasons")
public class CreditNoteReasonController extends BaseController
{
    private final JsonReadService<CreditNoteReason> creditNoteReasonService;

    public CreditNoteReasonController(JsonReadService<CreditNoteReason> creditNoteReasonService)
    {
        this.creditNoteReasonService = creditNoteReasonService;
    }

    @GetMapping
    public ResponseEntity<?> getCreditNoteReasons(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditNoteReasonService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCreditNoteReason(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditNoteReasonService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
