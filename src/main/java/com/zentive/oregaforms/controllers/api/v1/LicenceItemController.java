package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.licence.LicenceItemScheduleService;
import com.zentive.oregaforms.services.licence.LicenceItemService;
import com.zentive.oregaforms.services.licence.LicenceMemberService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/licenceItems")
public class LicenceItemController extends BaseController
{
    private final LicenceItemService licenceItemService;
    private final LicenceMemberService licenceMemberService;
    private final LicenceItemScheduleService licenceItemScheduleService;

    public LicenceItemController(LicenceItemService licenceItemService, LicenceMemberService licenceMemberService, LicenceItemScheduleService licenceItemScheduleService)
    {
        this.licenceItemService = licenceItemService;
        this.licenceMemberService = licenceMemberService;
        this.licenceItemScheduleService = licenceItemScheduleService;
    }

    @GetMapping
    public ResponseEntity<?> getLicenceItems(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceItemService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getLicenceItem(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceItemService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/licenceMembers")
    public ResponseEntity<?> getLicenceMembersByLicenceItem(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceMemberService.getAllByLicenceItem(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/licenceItemSchedules")
    public ResponseEntity<?> getLicenceItemSchedulesByLicenceItem(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceItemScheduleService.getAllByLicenceItem(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
