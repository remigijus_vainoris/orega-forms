package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.models.LicenceStatus;
import com.zentive.oregaforms.services.JsonReadService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/licenceStatuses")
public class LicenceStatusController extends BaseController
{
    private final JsonReadService<LicenceStatus> licenceStatusService;

    public LicenceStatusController(JsonReadService<LicenceStatus> licenceStatusService)
    {
        this.licenceStatusService = licenceStatusService;
    }

    @GetMapping
    public ResponseEntity<?> getLicenceStatuses(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceStatusService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getLicenceStatus(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceStatusService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

}
