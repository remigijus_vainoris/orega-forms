package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.account.AccountService;
import com.zentive.oregaforms.services.contact.ContactService;
import com.zentive.oregaforms.services.creditnotes.CreditNoteService;
import com.zentive.oregaforms.services.invoice.InvoiceLineService;
import com.zentive.oregaforms.services.licence.LicenceService;
import com.zentive.oregaforms.services.licence.MovementHistoryService;
import com.zentive.oregaforms.services.location.AssignedClientTypeService;
import com.zentive.oregaforms.services.location.LocationService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/locations")
public class LocationController extends BaseController
{
    private final LocationService locationService;
    private final AccountService accountService;
    private final ContactService contactService;
    private final AssignedClientTypeService assignedClientTypeService;
    private final CreditNoteService creditNoteService;
    private final InvoiceLineService invoiceLineService;
    private final MovementHistoryService movementHistoryService;
    private final LicenceService licenceService;

    public LocationController(LocationService locationService, AccountService accountService, ContactService contactService, AssignedClientTypeService assignedClientTypeService,
            CreditNoteService creditNoteService, InvoiceLineService invoiceLineService, MovementHistoryService movementHistoryService, LicenceService licenceService)
    {
        this.locationService = locationService;
        this.accountService = accountService;
        this.contactService = contactService;
        this.assignedClientTypeService = assignedClientTypeService;
        this.creditNoteService = creditNoteService;
        this.invoiceLineService = invoiceLineService;
        this.movementHistoryService = movementHistoryService;
        this.licenceService = licenceService;
    }

    @GetMapping
    public ResponseEntity<?> getLocations(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(locationService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getLocation(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(locationService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/accounts")
    public ResponseEntity<?> getAccountsByLocation(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(accountService.getAllByLocation(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/contacts")
    public ResponseEntity<?> getContactsByAccount(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(contactService.getAllByLocation(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/assignedClientTypes")
    public ResponseEntity<?> getAssignedClientTypesByLocation(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(assignedClientTypeService.getAllByLocation(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/creditNotes")
    public ResponseEntity<?> getCreditNotesByLocation(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditNoteService.getAllByLocation(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/invoiceLines")
    public ResponseEntity<?> getInvoiceLinesByLocation(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(invoiceLineService.getAllByLocation(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/movementHistory")
    public ResponseEntity<?> getMovementHistoriesByLocation(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(movementHistoryService.getAllByLocation(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/licences")
    public ResponseEntity<?> getLicencesByLocation(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceService.getAllByLocation(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

}
