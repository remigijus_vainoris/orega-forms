package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.licence.BreakDateService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/breakDates")
public class BreakDatesController extends BaseController
{
    private final BreakDateService breakDateService;

    public BreakDatesController(BreakDateService breakDateService)
    {
        this.breakDateService = breakDateService;
    }

    @GetMapping
    public ResponseEntity<?> getBreakDates(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(breakDateService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getBreakDate(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(breakDateService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
