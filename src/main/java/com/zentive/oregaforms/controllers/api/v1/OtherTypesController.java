package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.models.*;
import com.zentive.oregaforms.services.JsonReadService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class OtherTypesController extends BaseController
{
    private final JsonReadService<AgreementType> agreementTypeService;
    private final JsonReadService<BusinessType> businessTypeService;
    private final JsonReadService<CheckInType> checkInTypeService;
    private final JsonReadService<CheckOutType> checkOutTypeService;
    private final JsonReadService<PropertyType> propertyTypeService;
    private final JsonReadService<RecordType> recordTypeService;
    private final JsonReadService<ScheduleType> scheduleTypeService;

    public OtherTypesController(JsonReadService<AgreementType> agreementTypeService, JsonReadService<BusinessType> businessTypeService,
            JsonReadService<CheckInType> checkInTypeService,
            JsonReadService<CheckOutType> checkOutTypeService, JsonReadService<PropertyType> propertyTypeService, JsonReadService<RecordType> recordTypeService,
            JsonReadService<ScheduleType> scheduleTypeService)
    {
        this.agreementTypeService = agreementTypeService;
        this.businessTypeService = businessTypeService;
        this.checkInTypeService = checkInTypeService;
        this.checkOutTypeService = checkOutTypeService;
        this.propertyTypeService = propertyTypeService;
        this.recordTypeService = recordTypeService;
        this.scheduleTypeService = scheduleTypeService;
    }

    @GetMapping("/agreementTypes")
    public ResponseEntity<?> getAgreementTypes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(agreementTypeService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/agreementTypes/{id}")
    public ResponseEntity<?> getAgreementType(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(agreementTypeService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/businessTypes")
    public ResponseEntity<?> getBusinessTypes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(businessTypeService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/businessTypes/{id}")
    public ResponseEntity<?> getBusinessType(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(businessTypeService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/checkInTypes")
    public ResponseEntity<?> getCheckInTypes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(checkInTypeService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/checkInTypes/{id}")
    public ResponseEntity<?> getCheckInType(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(checkInTypeService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/checkOutTypes")
    public ResponseEntity<?> getCheckOutTypes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(checkOutTypeService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/checkOutTypes/{id}")
    public ResponseEntity<?> getCheckOutType(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(checkOutTypeService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/propertyTypes")
    public ResponseEntity<?> getPropertyTypes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(propertyTypeService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/propertyTypes/{id}")
    public ResponseEntity<?> getPropertytype(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(propertyTypeService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/recordTypes")
    public ResponseEntity<?> getRecordTypes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(recordTypeService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/recordTypes/{id}")
    public ResponseEntity<?> getRecordType(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(recordTypeService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/scheduleTypes")
    public ResponseEntity<?> getScheduleTypes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(scheduleTypeService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/scheduleTypes/{id}")
    public ResponseEntity<?> getScheduleType(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(scheduleTypeService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
