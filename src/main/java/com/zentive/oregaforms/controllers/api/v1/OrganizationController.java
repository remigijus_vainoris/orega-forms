package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.models.Organization;
import com.zentive.oregaforms.services.JsonReadService;
import com.zentive.oregaforms.services.organization.ClientTypeService;
import com.zentive.oregaforms.services.organization.GLCodeService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/organizations")
public class OrganizationController extends BaseController
{
    private final JsonReadService<Organization> organizationService;
    private final ClientTypeService clientTypeService;
    private final GLCodeService glCodeService;

    public OrganizationController(JsonReadService<Organization> organizationService, ClientTypeService clientTypeService, GLCodeService glCodeService)
    {
        this.organizationService = organizationService;
        this.clientTypeService = clientTypeService;
        this.glCodeService = glCodeService;
    }

    @GetMapping
    public ResponseEntity<?> getOrganizations(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(organizationService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getOrganization(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(organizationService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/clientTypes")
    public ResponseEntity<?> getClientTypesByOrganization(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(clientTypeService.getAllByOrganization(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/glCodes")
    public ResponseEntity<?> getGLCodesByOrganization(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(glCodeService.getAllByOrganization(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
