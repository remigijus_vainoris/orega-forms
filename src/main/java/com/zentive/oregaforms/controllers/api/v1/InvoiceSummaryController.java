package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.invoice.InvoiceSummaryService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/invoiceSummaries")
public class InvoiceSummaryController extends BaseController
{
    private final InvoiceSummaryService invoiceSummaryService;

    public InvoiceSummaryController(InvoiceSummaryService invoiceSummaryService)
    {
        this.invoiceSummaryService = invoiceSummaryService;
    }

    @GetMapping
    public ResponseEntity<?> getInvoiceSummaries(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(invoiceSummaryService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getInvoiceSummary(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(invoiceSummaryService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
