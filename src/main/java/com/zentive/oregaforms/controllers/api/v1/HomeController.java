package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController 
{
	@GetMapping("/api")
	public @ResponseBody String status()
	{
		return "OK";
	}
}