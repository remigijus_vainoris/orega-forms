package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.organization.GLCodeService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/glCodes")
public class GLCodeController extends BaseController
{
    private final GLCodeService glCodeService;

    public GLCodeController(GLCodeService glCodeService)
    {
        this.glCodeService = glCodeService;
    }

    @GetMapping
    public ResponseEntity<?> getGLCodes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(glCodeService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getGLCode(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(glCodeService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
