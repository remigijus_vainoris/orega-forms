package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.invoice.InvoiceLineService;
import com.zentive.oregaforms.services.invoice.InvoiceService;
import com.zentive.oregaforms.services.invoice.InvoiceSummaryService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/invoices")
public class InvoiceController extends BaseController
{
    private final InvoiceService invoiceService;
    private final InvoiceLineService invoiceLineService;
    private final InvoiceSummaryService invoiceSummaryService;

    public InvoiceController(InvoiceService invoiceService, InvoiceLineService invoiceLineService, InvoiceSummaryService invoiceSummaryService)
    {
        this.invoiceService = invoiceService;
        this.invoiceLineService = invoiceLineService;
        this.invoiceSummaryService = invoiceSummaryService;
    }

    @GetMapping
    public ResponseEntity<?> getInvoices(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(invoiceService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getInvoice(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(invoiceService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/invoiceLines")
    public ResponseEntity<?> getInvoiceLinesByInvoice(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(invoiceLineService.getAllByInvoice(id), HttpStatus.OK)
                : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/invoiceSummaries")
    public ResponseEntity<?> getInvoiceSummariesByInvoice(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(invoiceSummaryService.getAllByInvoice(id), HttpStatus.OK)
                : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

}
