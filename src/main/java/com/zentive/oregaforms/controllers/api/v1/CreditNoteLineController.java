package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.creditnotes.CreditNoteLineService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/creditNoteLines")
public class CreditNoteLineController extends BaseController
{
    private final CreditNoteLineService creditNoteLineService;

    public CreditNoteLineController(CreditNoteLineService creditNoteLineService)
    {
        this.creditNoteLineService = creditNoteLineService;
    }

    @GetMapping
    public ResponseEntity<?> getCreditNoteLines(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditNoteLineService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCreditNoteLine(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditNoteLineService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
