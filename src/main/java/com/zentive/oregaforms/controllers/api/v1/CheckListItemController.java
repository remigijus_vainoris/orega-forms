package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.models.CheckListItem;
import com.zentive.oregaforms.services.JsonReadService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/checkListItems")
public class CheckListItemController extends BaseController
{
    private final JsonReadService<CheckListItem> checkListItemService;

    public CheckListItemController(JsonReadService<CheckListItem> checkListItemService)
    {
        this.checkListItemService = checkListItemService;
    }

    @GetMapping
    public ResponseEntity<?> getCheckListItems(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(checkListItemService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCheckListItem(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(checkListItemService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
