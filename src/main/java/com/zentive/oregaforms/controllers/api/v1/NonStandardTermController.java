package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.models.NonStandardTerm;
import com.zentive.oregaforms.services.JsonReadService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/nonStandardTerms")
public class NonStandardTermController extends BaseController
{
    private final JsonReadService<NonStandardTerm> nonStandardTermService;

    public NonStandardTermController(JsonReadService<NonStandardTerm> nonStandardTermService)
    {
        this.nonStandardTermService = nonStandardTermService;
    }

    @GetMapping
    public ResponseEntity<?> getNonStandardTerms(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(nonStandardTermService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getNonStandardTerm(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(nonStandardTermService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

}
