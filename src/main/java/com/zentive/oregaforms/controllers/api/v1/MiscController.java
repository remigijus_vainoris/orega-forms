package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.models.*;
import com.zentive.oregaforms.services.JsonReadService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class MiscController extends BaseController
{

    private final JsonReadService<Industry> industryService;
    private final JsonReadService<Note> noteService;
    private final JsonReadService<OnlinePlan> onlinePlanJsonReadService;
    private final JsonReadService<QualityOfSpace> qualityOfSpaceService;
    private final JsonReadService<SetupChecklist> setupChecklistService;
    private final JsonReadService<User> userService;

    public MiscController(JsonReadService<Industry> industryService, JsonReadService<Note> noteService, JsonReadService<OnlinePlan> onlinePlanJsonReadService,
            JsonReadService<QualityOfSpace> qualityOfSpaceService, JsonReadService<SetupChecklist> setupChecklistService, JsonReadService<User> userService)
    {
        this.industryService = industryService;
        this.noteService = noteService;
        this.onlinePlanJsonReadService = onlinePlanJsonReadService;
        this.qualityOfSpaceService = qualityOfSpaceService;
        this.setupChecklistService = setupChecklistService;
        this.userService = userService;
    }

    @GetMapping("/industries")
    public ResponseEntity<?> getIndustries(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(industryService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/industries/{id}")
    public ResponseEntity<?> getIndustry(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(industryService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/notes")
    public ResponseEntity<?> getNotes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(noteService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/notes/{id}")
    public ResponseEntity<?> getNote(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(noteService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/onlinePlans")
    public ResponseEntity<?> getOnlinePlans(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(onlinePlanJsonReadService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/onlinePlans/{id}")
    public ResponseEntity<?> getOnlinePlan(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(onlinePlanJsonReadService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/qualityOfSpaces")
    public ResponseEntity<?> getQualityOfSpaces(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(qualityOfSpaceService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/qualityOfSpaces/{id}")
    public ResponseEntity<?> getQualityOfSpace(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(qualityOfSpaceService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/setupChecklists")
    public ResponseEntity<?> getSetupChecklists(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(setupChecklistService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/setupChecklists/{id}")
    public ResponseEntity<?> getSetupChecklist(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(setupChecklistService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/users")
    public ResponseEntity<?> getUsers(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(userService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<?> getUser(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(userService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
