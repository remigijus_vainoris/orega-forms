package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.licence.LicenceMemberService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/licenceMembers")
public class LicenceMemberController extends BaseController
{
    private final LicenceMemberService licenceMemberService;

    public LicenceMemberController(LicenceMemberService licenceMemberService)
    {
        this.licenceMemberService = licenceMemberService;
    }

    @GetMapping
    public ResponseEntity<?> getLicenceMembers(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceMemberService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getLicenceMember(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceMemberService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
