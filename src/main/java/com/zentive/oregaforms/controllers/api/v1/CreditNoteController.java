package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.creditnotes.CreditNoteLineService;
import com.zentive.oregaforms.services.creditnotes.CreditNoteService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/creditNotes")
public class CreditNoteController extends BaseController
{
    private final CreditNoteService creditNoteService;
    private final CreditNoteLineService creditNoteLineService;

    public CreditNoteController(CreditNoteService creditNoteService, CreditNoteLineService creditNoteLineService)
    {
        this.creditNoteService = creditNoteService;
        this.creditNoteLineService = creditNoteLineService;
    }

    @GetMapping
    public ResponseEntity<?> getCreditNotes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditNoteLineService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCreditNote(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditNoteService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/creditNoteLines")
    public ResponseEntity<?> getCreditNoteLinesByCreditNote(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditNoteLineService.getAllByCreditNote(id), HttpStatus.OK)
                : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

}
