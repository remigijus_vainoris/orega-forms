package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.licence.LicenceCheckListItemService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/licenceCheckListItems")
public class LicenceCheckListItemController extends BaseController
{
    private final LicenceCheckListItemService licenceCheckListItemService;

    public LicenceCheckListItemController(LicenceCheckListItemService licenceCheckListItemService)
    {
        this.licenceCheckListItemService = licenceCheckListItemService;
    }

    @GetMapping
    public ResponseEntity<?> getLicenceCheckListItems(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceCheckListItemService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getLicenceCheckListItem(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceCheckListItemService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
