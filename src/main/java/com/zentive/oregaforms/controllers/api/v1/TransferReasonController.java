package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.models.TransferReason;
import com.zentive.oregaforms.services.JsonReadService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/transferReasons")
public class TransferReasonController extends BaseController
{
    private final JsonReadService<TransferReason> transferReasonService;

    public TransferReasonController(JsonReadService<TransferReason> transferReasonService)
    {
        this.transferReasonService = transferReasonService;
    }

    @GetMapping
    public ResponseEntity<?> getTransferReasons(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(transferReasonService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getTransferReason(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(transferReasonService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
