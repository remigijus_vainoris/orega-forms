package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.licence.LicenceItemScheduleService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/licenceItemSchedules")
public class LicenceItemScheduleController extends BaseController
{
    private final LicenceItemScheduleService licenceItemScheduleService;

    public LicenceItemScheduleController(LicenceItemScheduleService licenceItemScheduleService)
    {
        this.licenceItemScheduleService = licenceItemScheduleService;
    }

    @GetMapping
    public ResponseEntity<?> getLicenceItemSchedules(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceItemScheduleService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getLicenceItemSchedule(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceItemScheduleService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
