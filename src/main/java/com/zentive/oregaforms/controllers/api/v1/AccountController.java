package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.account.AccountCodeService;
import com.zentive.oregaforms.services.account.AccountService;
import com.zentive.oregaforms.services.contact.ContactService;
import com.zentive.oregaforms.services.creditnotes.CreditNoteService;
import com.zentive.oregaforms.services.invoice.InvoiceLineService;
import com.zentive.oregaforms.services.invoice.InvoiceService;
import com.zentive.oregaforms.services.licence.LicenceService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/accounts")
public class AccountController extends BaseController
{
    private final AccountService accountService;
    private final InvoiceService invoiceService;
    private final ContactService contactService;
    private final AccountCodeService accountCodeService;
    private final CreditNoteService creditNoteService;
    private final InvoiceLineService invoiceLineService;
    private final LicenceService licenceService;

    public AccountController(AccountService accountService, InvoiceService invoiceService, ContactService contactService, AccountCodeService accountCodeService,
            CreditNoteService creditNoteService, InvoiceLineService invoiceLineService, LicenceService licenceService)
    {
        this.accountService = accountService;
        this.invoiceService = invoiceService;
        this.contactService = contactService;
        this.accountCodeService = accountCodeService;
        this.creditNoteService = creditNoteService;
        this.invoiceLineService = invoiceLineService;
        this.licenceService = licenceService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getAccount(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(accountService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/invoices")
    public ResponseEntity<?> getInvoicesByAccount(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(invoiceService.getAllByAccount(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/accountCodes")
    public ResponseEntity<?> getAccountCodesByAccount(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(accountCodeService.getAllByAccount(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/contacts")
    public ResponseEntity<?> getContactsByAccount(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(contactService.getAllByAccount(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/creditNotes")
    public ResponseEntity<?> getCreditNotesByAccount(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(creditNoteService.getAllByAccount(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/invoiceLines")
    public ResponseEntity<?> getInvoiceLinesByAccount(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(invoiceLineService.getAllByAccount(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/licences")
    public ResponseEntity<?> getLicencesByAccount(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceService.getAllByAccount(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
