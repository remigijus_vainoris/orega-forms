package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpHeaders;

import lombok.Setter;

@ConfigurationProperties("auth")
public abstract class BaseController
{
	@Setter
    private Boolean enabled;
	
	@Setter
    private String token;

    protected boolean isAuthenticated(HttpHeaders headers)
    {
    	return enabled != null && enabled ? isTokenValid(token, headers) : true;
    }
    
    private boolean isTokenValid(String token, HttpHeaders headers)
    {
    	boolean tokenValid = false;
    	
    	if (token != null && headers.get(HttpHeaders.AUTHORIZATION) != null)
        {
            for (String header : headers.get(HttpHeaders.AUTHORIZATION))
            {
                if (token.equals(header))
                	tokenValid = true;
            }
        }
    	
    	return tokenValid;
    }
}