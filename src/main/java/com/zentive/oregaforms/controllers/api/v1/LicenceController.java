package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.licence.BreakDateService;
import com.zentive.oregaforms.services.licence.LicenceCheckListItemService;
import com.zentive.oregaforms.services.licence.LicenceItemService;
import com.zentive.oregaforms.services.licence.LicenceService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/licences")
public class LicenceController extends BaseController
{
    private final LicenceService licenceService;
    private final LicenceCheckListItemService licenceCheckListItemService;
    private final BreakDateService breakDateService;
    private final LicenceItemService licenceItemService;

    public LicenceController(LicenceService licenceService, LicenceCheckListItemService licenceCheckListItemService, BreakDateService breakDateService,
            LicenceItemService licenceItemService)
    {
        this.licenceService = licenceService;
        this.licenceCheckListItemService = licenceCheckListItemService;
        this.breakDateService = breakDateService;
        this.licenceItemService = licenceItemService;
    }

    @GetMapping
    public ResponseEntity<?> getLicences(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getLicence(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/licenceCheckListItems")
    public ResponseEntity<?> getLicenceCheckListItemsByLicence(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceCheckListItemService.getAllByLicence(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/breakDates")
    public ResponseEntity<?> getBreakDatesByLicence(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(breakDateService.getAllByLicence(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}/licenceItems")
    public ResponseEntity<?> getLicenceItemsByLicence(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(licenceItemService.getAllByLicence(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

}
