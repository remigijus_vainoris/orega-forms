package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.organization.ClientTypeService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/clientTypes")
public class ClientTypeController extends BaseController
{
    private final ClientTypeService clientTypeService;

    public ClientTypeController(ClientTypeService clientTypeService)
    {
        this.clientTypeService = clientTypeService;
    }

    @GetMapping
    public ResponseEntity<?> getClientTypes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(clientTypeService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getClientType(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(clientTypeService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
