package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.account.AccountCodeService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/accountCodes")
public class AccountCodeController extends BaseController
{
    private final AccountCodeService accountCodeService;

    public AccountCodeController(AccountCodeService accountCodeService)
    {
        this.accountCodeService = accountCodeService;
    }

    @GetMapping
    public ResponseEntity<?> getAccountCodes(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(accountCodeService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getAccountCode(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(accountCodeService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
