package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.services.licence.MovementHistoryService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/movementHistory")
public class MovementHistoryController extends BaseController
{
    private final MovementHistoryService movementHistoryService;

    public MovementHistoryController(MovementHistoryService movementHistoryService)
    {
        this.movementHistoryService = movementHistoryService;
    }

    @GetMapping
    public ResponseEntity<?> getMovementHistories(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(movementHistoryService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getMovementHistory(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(movementHistoryService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
