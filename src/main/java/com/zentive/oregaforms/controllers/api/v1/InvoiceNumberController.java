package com.zentive.oregaforms.controllers.api.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zentive.oregaforms.models.InvoiceNumber;
import com.zentive.oregaforms.services.JsonReadService;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/invoiceNumbers")
public class InvoiceNumberController extends BaseController
{
    private final JsonReadService<InvoiceNumber> invoiceNumberService;

    public InvoiceNumberController(JsonReadService<InvoiceNumber> invoiceNumberService)
    {
        this.invoiceNumberService = invoiceNumberService;
    }

    @GetMapping
    public ResponseEntity<?> getInvoiceNumbers(@RequestHeader HttpHeaders headers)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(invoiceNumberService.getAll(), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getInvoiceNumber(@RequestHeader HttpHeaders headers, @PathVariable String id)
    {
        return isAuthenticated(headers) ? new ResponseEntity<>(invoiceNumberService.getById(id), HttpStatus.OK) : new ResponseEntity<>("Unauthorised!", HttpStatus.UNAUTHORIZED);
    }
}
