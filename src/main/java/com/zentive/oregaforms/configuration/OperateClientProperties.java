package com.zentive.oregaforms.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
@ConfigurationProperties("operate")
public class OperateClientProperties
{
    private String endpoint;
    private String username;
    private String password;
    private String secret;
}