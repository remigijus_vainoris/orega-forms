package com.zentive.oregaforms.configuration;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.CommonsRequestLoggingFilter;

public class RequestLoggingFilter extends CommonsRequestLoggingFilter
{
    @Override
    protected void beforeRequest(HttpServletRequest request, String message)
    {
        super.beforeRequest(request, message);
    }

    @Override
    protected void afterRequest(HttpServletRequest request, String message)
    {
        // do nothing, for now
    }
}
