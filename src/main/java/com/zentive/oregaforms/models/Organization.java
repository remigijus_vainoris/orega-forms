package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Organization extends BaseEntity implements Serializable
{
    private String aba;
    private String achAccountName;
    private String achAccountNo;
    private String achBankAddress;
    private String achBankName;
    private String virtualAchURL;
    private String addressLine1;
    private String lglAddress1;
    private String addressLine2;
    private String lglAddress3;
    private String addressLine3;
    private String accountName;
    private String accountNo;
    private String bankAddress;
    private String bankName;
    private String sortCode;
    private String companyNo;
    private String lglCountry;
    private String lglCounty;
    private String virtualCreditCardURL;
    private String ddFifthDigit;
    private String ddFirstDigit;
    private String ddForthDigit;
    private String ddOriginator1;
    private String ddOriginator2;
    private String ddOriginator3;
    private String ddSecondDigit;
    private String ddSixthDigit;
    private String ddThirdDigit;
    private String earliestInvoiceDate;
    private String exportFileTag;
    private String fullAddress;
    private String iban;
    private String logoURL;
    private String merchantId;
    private String legalEntityCode;
    private String paymentProcessor;
    private String lglPostcode;
    private String processorCurrencyCode;
    private String enableRecurringPayments;
    private String processorSignature;
    private String processorUseSandbox;
    private String processorUserId;
    private String recordType;
    private String singleLineAddress;
    private String swiftCode;
    private String vatNumber;
    private String lglTown;
}
