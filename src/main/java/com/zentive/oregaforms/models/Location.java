package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Location extends BaseEntity implements Serializable
{
    private String onlineAlias;
    private String currency;
    private String email;
    private String map;
}