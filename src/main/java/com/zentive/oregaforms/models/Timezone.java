package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Timezone extends BaseEntity implements Serializable
{
    private String timezoneOffset;
}
