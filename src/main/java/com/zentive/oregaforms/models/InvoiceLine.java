package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvoiceLine extends BaseEntity implements Serializable
{
    private String accountId;
    private String bundledProduct;
    private String discountFromList;
    private String from;
    private String glCode;
    private String invoiceId;
    private String listPrice;
    private String locationId;
    private String location;
    private String markup;
    private String markupType;
    private String order;
    private String originalBundle;
    private String productId;
    private String product;
    private String productGroupId;
    private String productGroup;
    private String quantity;
    private String referenceText;
    private String taxId;
    private String tax;
    private String taxCode;
    private String taxRate;
    private String to;
}
