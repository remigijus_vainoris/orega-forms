package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransferReason extends BaseEntity implements Serializable
{
    private String reason;
}
