package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Invoice extends BaseEntity implements Serializable
{
    private String accountId;
    private String account;
    private String approved;
    private String bfwd;
    private String clientTypeId;
    private String clientType;
    private String invoiceDescription;
    private String dueDate;
    private String emailInvoices;
    private String invoiceEmailAddress;
    private String emailAddress;
    private String endingBalance;
    private String gross;
    private String invoiceDate;
    private String invoiceNumber;
    private String invoiceTemplateId;
    private String invoiceTemplate;
    private String invoiceText;
    private String invoiceText1;
    private String locationId;
    private String location;
    private String net;
    private String organizationId;
    private String organization;
    private String paid;
    private String paymentsCredits;
    private String recordType;
    private String status;
    private String tax;

}