package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Currency extends BaseEntity implements Serializable
{
    private String currencyCode;
    private String currencyFormat;
    private String numberOfDecimalDigits;
}
