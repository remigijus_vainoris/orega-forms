package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LicenceItemSchedule extends BaseEntity implements Serializable
{
    private String endDate;
    private String forcePrice;
    private String fullAmount;
    private String invoiceNow;
    private String invoiced;
    private String licenceItemId;
    private String listPrice;
    private String discountFromList;
    private String proRata;
    private String productId;
    private String product;
    private String quantity;
    private String referenceText;
    private String scheduleId;
    private String schedule;
    private String startDate;
    private String unitPrice;
}
