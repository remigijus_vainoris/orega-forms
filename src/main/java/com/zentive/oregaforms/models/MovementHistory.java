package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovementHistory extends BaseEntity implements Serializable
{
    private String accountId;
    private String account;
    private String checkInOut;
    private String locationId;
    private String location;
    private String movementTypeId;
    private String movementType;
    private String office;
    private String officeSize;
    private String productId;
    private String product;
    private String rentalAmount;
    private String salespersonId;
    private String salesperson;
    private String startDate;
    private String term;
    private String workstations;
}
