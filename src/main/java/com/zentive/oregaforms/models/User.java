package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User extends BaseEntity implements Serializable
{
    private String approvalPricePercent;
    private String canApproveLicences;
    private String canCounterSign;
    private String canSeeProspects;
    private String checklistDate;
    private String checklistTasks;
    private String checklistTasksComplete;
    private String country;
    private String county;
    private String currencyId;
    private String currencyCode;
    private String currentLocation;
    private String discontinued;
    private String emailAddress;
    private String firstName;
    private String fullName;
    private String jeffEnabled;
    private String jeffUsername;
    private String jobFunction;
    private String lastName;
    private String locationIds;
    private String locations;
    private String ntAccount;
    private String pictureURL;
    private String userAccessLevel;
    private String showChecklist;
    private String tier3;
    private String title;
    private String town;
    private String userGroup;
    private String username;
}
