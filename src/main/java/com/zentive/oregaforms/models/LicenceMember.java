package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LicenceMember extends BaseEntity implements Serializable
{
    private String licenceItemId;
    private String contactId;
    private String endDate;
    private String startDate;
}
