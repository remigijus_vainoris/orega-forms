package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BreakDate extends BaseEntity implements Serializable
{
    private String breakDate;
    private String interval;
    private String licenceId;
    private String licence;
}
