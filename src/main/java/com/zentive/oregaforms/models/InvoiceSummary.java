package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvoiceSummary extends BaseEntity implements Serializable
{
    private String creditNoteId;
    private String draftCreditNoteId;
    private String draftInvoiceId;
    private String invoiceId;
    private String order;
    private String recordType;
    private String summaryTotal;
}
