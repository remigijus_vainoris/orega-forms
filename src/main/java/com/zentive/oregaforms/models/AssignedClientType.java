package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssignedClientType extends BaseEntity implements Serializable
{
    private String clientType;
    private String location;
    private String organizationName;
}
