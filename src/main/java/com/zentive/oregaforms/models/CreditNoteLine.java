package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreditNoteLine extends BaseEntity implements Serializable
{
    private String bundledProduct;
    private String creditNoteId;
    private String discountFromList;
    private String from;
    private String listPrice;
    private String markup;
    private String markupType;
    private String order;
    private String productId;
    private String product;
    private String productGroupId;
    private String productGroup;
    private String quantity;
    private String referenceText;
    private String taxId;
    private String taxRate;
    private String to;
    private String unitPrice;
}
