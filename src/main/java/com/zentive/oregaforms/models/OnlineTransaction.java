package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnlineTransaction extends BaseEntity implements Serializable
{
    private String amount;
    private String description;
    private String email;
    private String firstName;
    private String lastName;
    private String status;
    private String telephone;
    private String title;
    private String transactionDate;
}
