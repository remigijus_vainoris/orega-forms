package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientType extends BaseEntity implements Serializable
{
    private String chargeExternalRates;
    private String clientTypeAndOrganization;
    private String organizationId;
    private String webResCreditCard;
    private String residentialClassification;
    private String useInvoicePrefix;
    private String virtualClassification;
}
