package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LicenceCheckListItem extends BaseEntity implements Serializable
{
    private String checked;
    private String checkedBy;
    private String licenceId;
    private String licence;
    private String checkListItemId;
}
