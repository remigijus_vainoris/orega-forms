package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Account extends BaseEntity implements Serializable
{
    private String clientName;
    private String accountTypeId;
    private String accountType;
    private String balance;
    private String broker;
    private String agentContact;
    private String brokerFirstName;
    private String brokerLastName;
    private String campaignId;
    private String chargeVAT;
    private String clientTypeId;
    private String clientType;
    private String companyLogoURL;
    private String earliestTransactionDateAllowed;
    private String emailInvoice;
    private String endDate;
    private String fullAddress;
    private String hasActiveLicence;
    private String includeBfwd;
    private String showcAllDetails;
    private String includeItemisedBill;
    private String includeStatement;
    private String isExternal;
    private String isResidential;
    private String isVirtual;
    private String leaddadiAgent;
    private String leaddadiId;
    private String location;
    private String meetingPackageId;
    private String onAccount;
    private String onlineBill;
    private String organizationId;
    private String organization;
    private String overrideDiscount;
    private String userId;
    private String printInvoice;
    private String recordTypeId;
    private String recordType;
    private String registeredAddressSingleLine;
    private String registeredFullAddress;
    private String singleLineAddress;
    private String startDate;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String county;
    private String postcode;
    private String country;
}