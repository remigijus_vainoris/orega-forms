package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SetupChecklist extends BaseEntity implements Serializable
{
    private String complete;
    private String description;
    private String editButtonText;
    private String checklistGroup;
    private String iconUrl;
    private String linkTo;
    private String moreInfo;
    private String notRightNow;
    private String sortOrder;
    private String spa;
    private String subject;
    private String subtitle;
}
