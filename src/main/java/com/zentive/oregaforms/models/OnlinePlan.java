package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnlinePlan extends BaseEntity implements Serializable
{
    private String choosePackageText;
    private String choosePackageTitle;
    private String description;
    private String onlinePlanLink;
    private String planType;
}
