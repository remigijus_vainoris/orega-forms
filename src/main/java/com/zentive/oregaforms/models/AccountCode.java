package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountCode extends BaseEntity implements Serializable
{
    private String accountId;
    private String account;
    private String accountCode;
    private String deviceImport;
}
