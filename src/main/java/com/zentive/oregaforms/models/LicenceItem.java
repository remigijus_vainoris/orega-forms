package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LicenceItem extends BaseEntity implements Serializable
{
    private String autoCharge;
    private String autoEscalate;
    private String billinAdvance;
    private String anniversary;
    private String bundledProduct;
    private String mandatorySqftCalc;
    private String mandatoryToVirtualClients;
    private String deposit;
    private String depositHeld;
    private String depositReturned;
    private String discountPercent;
    private String discountAmount;
    private String endDate;
    private String scheduleId;
    private String schedule;
    private String licenceId;
    private String licence;
    private String listPrice;
    private String listPriceTotal;
    private String mandatoryCharge;
    private String mandatoryChargeType;
    private String officeSize;
    private String mandatoryChargePercentOfAll;
    private String mandatoryPercentOfRent;
    private String proRata2ndPayment;
    private String productId;
    private String product;
    private String quantity;
    private String referenceText;
    private String retainerProductId;
    private String retainerProduct;
    private String retainerReferenceText;
    private String retainerTaxId;
    private String retainerTax;
    private String startDate;
    private String subTotal;
    private String taxId;
    private String tax;
    private String terminated;
    private String total;
    private String totalDeposit;
    private String type;
    private String unitPrice;
    private String canEditMandatoryCharge;
    private String workstations;

}
