package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Contact extends BaseEntity implements Serializable
{
    private String addressId;
    private String address;
    private String checkedIn;
    private String clientTypeId;
    private String clientType;
    private String communityProfileActive;
    private String companyLogoURL;
    private String fullName;
    private String emailAddress;
    private String optOut;
    private String firstName;
    private String hasActiveLicence;
    private String privatePhoneNo;
    private String contactImageURL;
    private String invitedFromPortal;
    private String lastName;
    private String leadId;
    private String lead;
    private String locationId;
    private String location;
    private String newsletter;
    private String numberBeingFollowed;
    private String numberOfFollowers;
    private String portalAccessLevel;
    private String profilePercentComplete;
    private String companyProfileActive;
    private String receiveShoutOutEmails;
    private String receiveStatusUpdateEmails;
    private String remainingHours;
    private String phoneNo;
    private String title;

}
