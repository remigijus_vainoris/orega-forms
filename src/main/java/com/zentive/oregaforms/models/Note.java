package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Note extends BaseEntity implements Serializable
{
    private String accountId;
    private String account;
    private String comments;
    private String contactId;
    private String filename;
    private String leadId;
    private String lead;
    private String opportunityId;
    private String opportunity;
    private String subject;
}
