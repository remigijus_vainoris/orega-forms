package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BaseEntity implements Serializable
{
    private String id;
    private String name;
    private String createdBy;
    private String creationDate;
    private String updatedBy;
    private String updateDate;
}
