package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GLCode extends BaseEntity implements Serializable
{
    private String clientRetainer;
    private String controlAccount;
    private String disableDelete;
    private String forfeitCode;
    private String goCardless;
    private String goCardlessFee;
    private String organizationId;
    private String retainerLiability;
}
