package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreditNote extends BaseEntity implements Serializable
{
    private String accountId;
    private String approved;
    private String comment;
    private String date;
    private String number;
    private String reasonId;
    private String text;
    private String typeId;
    private String gross;
    private String locationId;
    private String net;
    private String status;
    private String tax;
}
