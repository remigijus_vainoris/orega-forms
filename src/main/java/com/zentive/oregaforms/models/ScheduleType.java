package com.zentive.oregaforms.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduleType extends BaseEntity implements Serializable
{
    private String interval;
    private String intervalNumber;
    private String numberOfDays;
    private String numberOfMonths;
    private String sortOrder;
}
