package com.zentive.oregaforms.operate;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zentive.oregaforms.configuration.OperateClientProperties;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
class OperateHttpClient
{
    private final OperateClientProperties operateClientProperties;
    private final CloseableHttpClient httpClient;
    private final ObjectMapper mapper;

    private String accessToken;
    private String refreshToken;

    public OperateHttpClient(OperateClientProperties operateClientProperties)
    {
        this.operateClientProperties = operateClientProperties;

        this.httpClient = HttpClients.custom().setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()).build();

        this.mapper = new ObjectMapper();
        this.mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        fetchAccessToken();
    }

    protected JsonNode postForEntity(URI uri, List<NameValuePair> requestParams, Header... headers)
    {
        JsonNode response = null;

        try
        {
            HttpPost httpPost = new HttpPost(uri);

            addHeadersToRequest(httpPost, headers);

            httpPost.setEntity(new UrlEncodedFormEntity(requestParams));

            log.debug("Executing POST request: " + httpPost.getRequestLine());

            try (CloseableHttpResponse httpResponse = this.httpClient.execute(httpPost))
            {
                if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
                    response = mapper.readTree(httpResponse.getEntity().getContent());
            }
        }
        catch (UnsupportedEncodingException ex1)
        {
            log.error("Unable to set the request payload: " + ex1.getMessage());
        }
        catch (IOException ex3)
        {
            log.error("Unable to execute the POST request to [" + uri + "]: " + ex3.getMessage());
        }
        catch (Exception ex)
        {
            log.error("Unexpected exception occurred: " + ex.getMessage());
        }

        if (response != null)
            log.debug("POST response: " + response);

        return response;
    }

    protected JsonNode getForEntity(URI uri, Header... headers)
    {
        JsonNode response = null;

        try
        {
            HttpGet httpGet = new HttpGet(uri);

            addHeadersToRequest(httpGet, headers);

            httpGet.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + this.accessToken);

            log.debug("Executing GET request: " + httpGet.getRequestLine());

            try (CloseableHttpResponse httpResponse = this.httpClient.execute(httpGet))
            {
                if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
                    response = mapper.readTree(httpResponse.getEntity().getContent());
            }
        }
        catch (UnsupportedEncodingException ex1)
        {
            log.error("Unable to set the request payload: " + ex1.getMessage());
        }
        catch (IOException ex3)
        {
            log.error("Unable to execute the GET request to [" + uri + "]: " + ex3.getMessage());
        }
        catch (Exception ex)
        {
            log.error("Unexpected exception occurred: " + ex.getMessage());
        }

        if (response != null)
            log.debug("GET response: " + response);

        return response;
    }

    protected boolean isAuthenticated()
    {
        if (accessToken == null)
            fetchAccessToken();

        refreshAccessToken();

        return accessToken != null;
    }

    private void fetchAccessToken()
    {
        List<NameValuePair> requestParams = new ArrayList<>();

        requestParams.add(new BasicNameValuePair("username", operateClientProperties.getUsername()));
        requestParams.add(new BasicNameValuePair("password", operateClientProperties.getPassword()));
        requestParams.add(new BasicNameValuePair("grant_type", "password"));

        tokenRequest(requestParams);
    }

    private void refreshAccessToken()
    {
        List<NameValuePair> requestParams = new ArrayList<>();

        requestParams.add(new BasicNameValuePair("grant_type", "refresh_token"));
        requestParams.add(new BasicNameValuePair("refresh_token", refreshToken));

        tokenRequest(requestParams);
    }

    private void tokenRequest(List<NameValuePair> requestParams)
    {
        requestParams.add(new BasicNameValuePair("client_secret", operateClientProperties.getSecret()));
        requestParams.add(new BasicNameValuePair("client_id", "trustedclient"));
        requestParams.add(new BasicNameValuePair("scope", "hubapi"));

        log.debug("Token request params: " + requestParams.toString());

        try
        {
            JsonNode tokenResponse = postForEntity(new URIBuilder(operateClientProperties.getEndpoint() + "oauth2/token").build(), requestParams);

            if (tokenResponse != null)
            {
                this.accessToken = tokenResponse.get("access_token").asText();
                this.refreshToken = tokenResponse.get("refresh_token").asText();
            }
        }
        catch (URISyntaxException ex2)
        {
            log.error("Unable to parse the URI: " + ex2.getMessage());
        }
    }

    private void addHeadersToRequest(HttpUriRequest request, Header... headers)
    {
        for (Header header : headers)
        {
            request.addHeader(header);
        }
    }
}