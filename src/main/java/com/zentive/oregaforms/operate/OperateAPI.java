package com.zentive.oregaforms.operate;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zentive.oregaforms.configuration.OperateClientProperties;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OperateAPI
{
    private final OperateHttpClient operateHttpClient;
    private final String baseEndpoint;

    public OperateAPI(OperateClientProperties operateClientProperties, OperateHttpClient operateHttpClient)
    {
        this.operateHttpClient = operateHttpClient;

        this.baseEndpoint = operateClientProperties.getEndpoint();
    }

    public JsonNode getAllLocations()
    {
        return executeRequest("location");
    }

    public JsonNode getLocation(String locationId)
    {
        return executeRequest("location", "[locationid]='" + locationId + "'");
    }

    public JsonNode getAccount(String accountId)
    {
        return executeRequest("account", "[accountid]='" + accountId + "'");
    }

    public JsonNode getAllAccountsByLocation(String locationId)
    {
        return executeRequest("account", "[location]='" + locationId + "'AND[accounttype_id]='-1'");
    }

    public JsonNode getAllInvoices()
    {
        return executeRequest("invoice");
    }

    public JsonNode getInvoice(String invoiceId)
    {
        return executeRequest("invoice", "[invoiceid]='" + invoiceId + "'");
    }

    public JsonNode getAllInvoicesByAccount(String accountId)
    {
        return executeRequest("invoice", "[accountname]='" + accountId + "'");

    }

    public JsonNode getAccountCodesByAccount(String accountId)
    {
        return executeRequest("accountcode", "[account]='" + accountId + "'");
    }

    public JsonNode getAllAccountTypes()
    {
        return executeRequest("accounttype");
    }

    public JsonNode getAccountType(String accountTypeId)
    {
        return executeRequest("accounttype", "[accounttypeid]='" + accountTypeId + "'");
    }

    public JsonNode getAllGroupAccounts()
    {
        return executeRequest("groupaccount");
    }

    public JsonNode getGroupAccount(String groupAccountId)
    {
        return executeRequest("groupaccount", "[groupaccountid]='" + groupAccountId + "'");
    }

    public JsonNode getAllAccountCodes()
    {
        return executeRequest("accountcode");
    }

    public JsonNode getAccountCode(String accountCodeId)
    {
        return executeRequest("accountcode", "[accountcodeid]='" + accountCodeId + "'");
    }

    public JsonNode getAllContacts()
    {
        return executeRequest("contact");
    }

    public JsonNode getContact(String contactId)
    {
        return executeRequest("contact", "[contactid]='" + contactId + "'");
    }

    public JsonNode getAllContactsByAccount(String accountId)
    {
        return executeRequest("contact", "[user_id]='" + accountId + "'");
    }

    public JsonNode getAllContactsByLocation(String locationId)
    {
        return executeRequest("contact", "[locationname]='" + locationId + "'");
    }

    public JsonNode getAllContactTypes()
    {
        return executeRequest("contacttype");
    }

    public JsonNode getContactType(String contactTypeId)
    {
        return executeRequest("contacttype", "[contacttypeid]='" + contactTypeId + "'");
    }

    public JsonNode getAllJobTitles()
    {
        return executeRequest("lookupjobtitles");
    }

    public JsonNode getJobTitle(String jobTitleId)
    {
        return executeRequest("lookupjobtitles", "[jobtitlesid]='" + jobTitleId + "'");
    }

    public JsonNode getAllAgreementTypes()
    {
        return executeRequest("lookupagreementtypes");
    }

    public JsonNode getAgreementType(String agreementTypeId)
    {
        return executeRequest("lookupagreementtypes", "[agreementtypesid]='" + agreementTypeId + "'");
    }

    public JsonNode getAllBusinessTypes()
    {
        return executeRequest("businesstype");
    }

    public JsonNode getBusinessType(String businessTypeId)
    {
        return executeRequest("businesstype", "[businesstypeid]='" + businessTypeId + "'");
    }

    public JsonNode getAllCheckInTypes()
    {
        return executeRequest("lookupcheckintypes");
    }

    public JsonNode getCheckInType(String checkInTypeId)
    {
        return executeRequest("lookupcheckintypes", "[checkintypesid]='" + checkInTypeId + "'");
    }

    public JsonNode getAllCheckOutTypes()
    {
        return executeRequest("lookupcheckouttypes");
    }

    public JsonNode getCheckOutType(String checkOutTypeId)
    {
        return executeRequest("lookupcheckouttypes", "[checkouttypesid]='" + checkOutTypeId + "'");
    }

    public JsonNode getAllCounties()
    {
        return executeRequest("lookupcounties");
    }

    public JsonNode getCounty(String countyId)
    {
        return executeRequest("lookupcounties", "[countiesid]='" + countyId + "'");
    }

    public JsonNode getAllPropertyTypes()
    {
        return executeRequest("propertytypes");
    }

    public JsonNode getPropertyType(String propertyTypeId)
    {
        return executeRequest("propertytypes", "[propertytypesid]='" + propertyTypeId + "'");
    }

    public JsonNode getAllRecordTypes()
    {
        return executeRequest("recordtype");
    }

    public JsonNode getRecordType(String recordTypeId)
    {
        return executeRequest("recordtype", "[recordtypeid]='" + recordTypeId + "'");
    }

    public JsonNode getAllScheduleTypes()
    {
        return executeRequest("scheduletype");
    }

    public JsonNode getScheduleType(String scheduleTypeId)
    {
        return executeRequest("scheduletype", "[scheduletypeid]='" + scheduleTypeId + "'");
    }

    public JsonNode getAllTimezones()
    {
        return executeRequest("timezone");
    }

    public JsonNode getTimezone(String timezoneId)
    {
        return executeRequest("timezone", "[timezoneid]='" + timezoneId + "'");
    }

    public JsonNode getAllCreditCards()
    {
        return executeRequest("creditcard");
    }

    public JsonNode getCreditCard(String creditCardId)
    {
        return executeRequest("creditcard", "[creditcardid]='" + creditCardId + "'");
    }

    public JsonNode getAllIndustries()
    {
        return executeRequest("industry");
    }

    public JsonNode getIndustry(String industryId)
    {
        return executeRequest("industry", "[industryid]='" + industryId + "'");
    }

    public JsonNode getAllQualityOfSpaces()
    {
        return executeRequest("qualityofspace");
    }

    public JsonNode getQualityOfSpace(String qualityOfSpaceId)
    {
        return executeRequest("qualityofspace", "[qualityofspaceid]='" + qualityOfSpaceId + "'");
    }

    public JsonNode getAllTowns()
    {
        return executeRequest("lookuptowns");
    }

    public JsonNode getTown(String townId)
    {
        return executeRequest("lookuptowns", "[townsid]='" + townId + "'");
    }

    public JsonNode getAllCurrencies()
    {
        return executeRequest("currency");
    }

    public JsonNode getCurrency(String currencyId)
    {
        return executeRequest("currency", "[currencyid]='" + currencyId + "'");
    }

    public JsonNode getAllExchangeRates()
    {
        return executeRequest("exchangerate");
    }

    public JsonNode getExchangeRate(String exchangeRateId)
    {
        return executeRequest("exchangerate", "[exchangerateid]='" + exchangeRateId + "'");
    }

    public JsonNode getAllOnlinePlans()
    {
        return executeRequest("onlineplan");
    }

    public JsonNode getOnlinePlan(String onlinePlanId)
    {
        return executeRequest("onlineplan", "[onlineplanid]='" + onlinePlanId + "'");
    }

    public JsonNode getOnlineTransactions()
    {
        return executeRequest("portaltransactions");
    }

    public JsonNode getOnlineTransaction(String onlineTransactionId)
    {
        return executeRequest("portaltransactions", "[paymenttransactionid]='" + onlineTransactionId + "'");
    }

    public JsonNode getAllSetupChecklists()
    {
        return executeRequest("setupchecklist");
    }

    public JsonNode getSetupChecklist(String setupChecklistId)
    {
        return executeRequest("setupchecklist", "[setupchecklistid]='" + setupChecklistId + "'");
    }

    public JsonNode getAllUsers()
    {
        return executeRequest("User");
    }

    public JsonNode getUser(String userId)
    {
        return executeRequest("User", "[userid]='" + userId + "'");
    }

    public JsonNode getAllAssignedClientTypesByLocation(String locationId)
    {
        return executeRequest("assignedclienttype", "[location]='" + locationId + "'");
    }

    public JsonNode getAllCreditNotes()
    {
        return executeRequest("creditnote");
    }

    public JsonNode getCreditNote(String creditNoteId)
    {
        return executeRequest("creditnote", "[creditnoteid]='" + creditNoteId + "'");
    }

    public JsonNode getAllCreditNotesByAccount(String accountId)
    {
        return executeRequest("creditnote", "[accountname]='" + accountId + "'");
    }

    public JsonNode getAllCreditNotesByLocation(String locationId)
    {
        return executeRequest("creditnote", "[location]='" + locationId + "'");
    }

    public JsonNode getAllCreditNotesByAccountAndLocation(String accountId, String location)
    {
        return executeRequest("creditnote", "[accountname]='" + accountId + "'AND[location]='" + location + "'");
    }

    public JsonNode getAllRegions()
    {
        return executeRequest("region");
    }

    public JsonNode getRegion(String regionId)
    {
        return executeRequest("region", "[regionid]='" + regionId + "'");
    }

    public JsonNode getAllInvoiceNumbers()
    {
        return executeRequest("invoicenumber");
    }

    public JsonNode getInvoiceNumber(String invoiceNumberId)
    {
        return executeRequest("invoicenumber", "[invoicenumberid]='" + invoiceNumberId + "'");
    }

    public JsonNode getAllCreditNoteReasons()
    {
        return executeRequest("lookupcreditnotereason");
    }

    public JsonNode getCreditNoteReason(String creditNoteReasonId)
    {
        return executeRequest("lookupcreditnotereason", "[creditnotereasonid]='" + creditNoteReasonId + "'");
    }

    public JsonNode getAllCreditNoteTypes()
    {
        return executeRequest("lookupcreditnotetype");
    }

    public JsonNode getCreditNoteType(String creditNoteReasonId)
    {
        return executeRequest("lookupcreditnotetype", "[creditnotetypeid]='" + creditNoteReasonId + "'");
    }

    public JsonNode getAllTransferReasons()
    {
        return executeRequest("lookuptransferreasons");
    }

    public JsonNode getTransferReason(String transferReasonId)
    {
        return executeRequest("lookuptransferreasons", "[transferreasonsid]='" + transferReasonId + "'");
    }

    public JsonNode getAllTerminationReasons()
    {
        return executeRequest("lookupterminationresons");
    }

    public JsonNode getTerminationReason(String terminationReasonId)
    {
        return executeRequest("lookupterminationresons", "[terminationresonsid]='" + terminationReasonId + "'");
    }

    public JsonNode getAllNonStandardTerms()
    {
        return executeRequest("lookupnonstandardterms");
    }

    public JsonNode getNonStandardTerm(String nonStandardTermId)
    {

        return executeRequest("lookupnonstandardterms", "[nonstandardtermsid]='" + nonStandardTermId + "'");
    }

    public JsonNode getAllMovementTypes()
    {
        return executeRequest("movementtype");
    }

    public JsonNode getMovementType(String movementTypeId)
    {
        return executeRequest("movementtype", "[movementtypeid]='" + movementTypeId + "'");
    }

    public JsonNode getAllLicenceStatuses()
    {
        return executeRequest("LicenceStatus");
    }

    public JsonNode getLicenceStatus(String licenceStatusId)
    {
        return executeRequest("LicenceStatus", "[licencestatusid]='" + licenceStatusId + "'");
    }

    public JsonNode getAllCheckListItems()
    {
        return executeRequest("checklistitem");
    }

    public JsonNode getCheckListItem(String checkListItemId)
    {
        return executeRequest("checklistitem", "[checklistitemsid]='" + checkListItemId + "'");
    }

    public JsonNode getAllCreditNoteLines()
    {
        return executeRequest("creditnoteline");
    }

    public JsonNode getCreditNoteLine(String creditNoteLineId)
    {
        return executeRequest("creditnoteline", "[creditnotelineid]='" + creditNoteLineId + "'");
    }

    public JsonNode getAllCreditNoteLinesByCreditNote(String creditNoteId)
    {
        return executeRequest("creditnoteline", "[creditnote]='" + creditNoteId + "'");
    }

    public JsonNode getAllInvoiceLines()
    {
        return executeRequest("invoiceline");
    }

    public JsonNode getAllInvoiceLine(String invoiceLineId)
    {
        return executeRequest("invoiceline", "[draftinvoicelineid]='" + invoiceLineId + "'");
    }

    public JsonNode getAllInvoiceLinesByInvoice(String invoiceId)
    {
        return executeRequest("invoiceline", "[invoice]='" + invoiceId + "'");
    }

    public JsonNode getAllInvoiceLinesByLocation(String locationId)
    {
        return executeRequest("invoiceline", "[location]='" + locationId + "'");
    }

    public JsonNode getAllInvoiceLinesByAccount(String accountId)
    {
        return executeRequest("invoiceline", "[accountsid]='" + accountId + "'");
    }

    public JsonNode getAllInvoiceSummaries()
    {
        return executeRequest("invoicesummary");
    }

    public JsonNode getInvoiceSummary(String invoiceSummaryId)
    {
        return executeRequest("invoicesummary", "[invoicesummaryid]='" + invoiceSummaryId + "'");
    }

    public JsonNode getAllInvoiceSummariesByInvoice(String invoiceId)
    {
        return executeRequest("invoicesummary", "[invoiceid]='" + invoiceId + "'");
    }

    public JsonNode getAllNotes()
    {
        return executeRequest("note");
    }

    public JsonNode getNote(String noteId)
    {
        return executeRequest("note", "[noteid]='" + noteId + "'");
    }

    public JsonNode getAllOrganizations()
    {
        return executeRequest("organization");
    }

    public JsonNode getOrganization(String organizationId)
    {
        return executeRequest("organization", "[organizationid]='" + organizationId + "'");
    }

    public JsonNode getAllClientTypes()
    {
        return executeRequest("clienttype");
    }

    public JsonNode getClientType(String clientTypeId)
    {
        return executeRequest("clienttype", "[clienttypeid]='" + clientTypeId + "'");
    }

    public JsonNode getAllClientTypesByOrganization(String organizationId)
    {
        return executeRequest("clienttype", "[organization]='" + organizationId + "'");
    }

    public JsonNode getAllGLCodes()
    {
        return executeRequest("glcode");
    }

    public JsonNode getGLCode(String glCodeId)
    {
        return executeRequest("glcode", "[glcodeid]='" + glCodeId + "'");
    }

    public JsonNode getAllGLCodesByOrganization(String organizationId)
    {
        return executeRequest("glcode", "[organization]='" + organizationId + "'");
    }

    public JsonNode getAllLicenceMembers()
    {
        return executeRequest("licencemember");
    }

    public JsonNode getLicenceMember(String licenceMemberId)
    {
        return executeRequest("licencemember", "[licencememberid]='" + licenceMemberId + "'");
    }

    public JsonNode getAllLicenceMembersByLicenceItem(String licenceItemId)
    {
        return executeRequest("licencemember", "[licenceitem]='" + licenceItemId + "'");
    }

    public JsonNode getAllMovementHistories()
    {
        return executeRequest("movementhistory");
    }

    public JsonNode getMovementHistory(String movementHistoryId)
    {
        return executeRequest("movementhistory", "[movementId]='" + movementHistoryId + "'");
    }

    public JsonNode getAllMovementHistoriesByLocation(String locationId)
    {
        return executeRequest("movementhistory", "[location]='" + locationId + "'");
    }

    public JsonNode getAllLicenceCheckListItems()
    {
        return executeRequest("licencechecklistitem");
    }

    public JsonNode getLicenceCheckListItem(String licenceCheckListItemId)
    {
        return executeRequest("licencechecklistitem", "[licencechecklistitemid]='" + licenceCheckListItemId + "'");
    }

    public JsonNode getAllLicenceCheckListItemsByLicence(String licenceId)
    {
        return executeRequest("licencechecklistitem", "[licence]='" + licenceId + "'");
    }

    public JsonNode getAllBreakDates()
    {
        return executeRequest("breakdate");
    }

    public JsonNode getBreadDate(String breakDateId)
    {
        return executeRequest("breakdate", "[breakdateid]='" + breakDateId + "'");
    }

    public JsonNode getAllBreakDatesByLicence(String licenceId)
    {
        return executeRequest("breakdate", "[licence]='" + licenceId + "'");
    }

    public JsonNode getAllLicenceItemSchedules()
    {
        return executeRequest("licenceitemschedule");
    }

    public JsonNode getLicenceItemSchedule(String licenceItemScheduleId)
    {
        return executeRequest("licenceitemschedule", "[licenceitemscheduleid]='" + licenceItemScheduleId + "'");
    }

    public JsonNode getAllLicenceItemSchedulesByLicenceItem(String licenceItemId)
    {
        return executeRequest("licenceitemschedule", "[licenceitem]='" + licenceItemId + "'");
    }

    public JsonNode getAllLicenceItems()
    {
        return executeRequest("LicenceItem");
    }

    public JsonNode getLicenceItem(String licenceItemId)
    {
        return executeRequest("LicenceItem", "[licenceitemid]='" + licenceItemId + "'");
    }

    public JsonNode getAllLicenceItemsByLicence(String licenceId)
    {
        return executeRequest("LicenceItem", "[licence]='" + licenceId + "'");
    }

    public JsonNode getAllLicences()
    {
        return executeRequest("licence");
    }

    public JsonNode getLicence(String licenceId)
    {
        return executeRequest("licence", "[licenceid]='" + licenceId + "'");
    }

    public JsonNode getAllLicencesByAccount(String accountId)
    {
        return executeRequest("licence", "[accountname]='" + accountId + "'");
    }

    public JsonNode getAllLicencesByLocation(String locationId)
    {
        return executeRequest("licence", "[locationname]='" + locationId + "'");
    }

    private JsonNode executeRequest(String endpoint)
    {
        return executeRequest(endpoint, null);
    }

    private JsonNode executeRequest(String endpoint, String params)
    {
        JsonNode response = null;

        try
        {
            URIBuilder uriBuilder = new URIBuilder(this.baseEndpoint + "api/1.0/" + endpoint);

            if (params != null)
                uriBuilder.addParameter("where", params);

            if (operateHttpClient.isAuthenticated())
                response = operateHttpClient.getForEntity(uriBuilder.build());

        }
        catch (Exception ex)
        {
            log.error("Could not build the URI for the request: " + ex.getMessage());
        }

        return response;
    }
}
