/*********************************
Load up the accounts - page (Request the items) and load the data.
********************************/
var endPointLocation = "https://uat.sl-labs.com/OregaForms/api/v1/"
function initialiseOnLoadItem(divName, containerName ,  urlIndexName){if (containerName == undefined){containerName = null;}
/*************************************************
Location - Control Block. For the location -
Seek to initialize the location in a drop-down list
**************************************************/
const controlBlockLocation = {locationEndPoint:endPointLocation,
containerSelectDivID:containerName,
selectItemID : divName,
loadingText: "Loading...",
selectText: "Please select...",
loadOnInitialisation:true,
appendDirectory: urlIndexName,
responseListCallback:onChangeEventItemListResponse,
responseDataCallback:onChangeEventItemDataResponse,
initialClickCallBack:onClickEventAction
}
return new initialiseData(controlBlockLocation);}
/*********************************
Load up the accounts - page (Request the items) and load the data.
********************************/
function initialiseAccountsPage(divName, containerName, urlIndexName){if (containerName == undefined){containerName = null;}
const controlBlockCustomer = {locationEndPoint:endPointLocation,
containerSelectDivID:containerName,
selectItemID : divName,
loadingText: "Loading...",
selectText: "Please select...",
loadOnInitialisation:false,
appendDirectory: urlIndexName,
responseListCallback:onChangeEventItemListResponse,
responseDataCallback:onChangeEventItemDataResponse,
initialClickCallBack:onClickEventAction
}
return new initialiseData(controlBlockCustomer);}
/*********************************
Load For a custom item. Initialize and create the item.
This is usually based on a select tag which has been found on the page.
********************************/
function initialiseCustomElement(divName, containerName, urlIndexName){const controlBlockCustomItem = {locationEndPoint:endPointLocation,
containerSelectDivID:containerName,
selectItemID : divName,
loadingText: "Loading...",
selectText: "Please select...",
loadOnInitialisation:false,
appendDirectory: urlIndexName,
responseListCallback:onChangeEventItemListResponse,
responseDataCallback:onChangeEventItemDataResponse,
initialClickCallBack:onClickEventAction
}
return new initialiseData(controlBlockCustomItem);}
/***************************************************************
Setup and load the drop down items - visible items for display on the page.
***************************************************************/
function initialiseAccountsContactsDropDownItems(locationsDataControl , accountsDetailConfiguration){if (accountsDetailConfiguration != null){accountDataControl = initialiseAccountsPage(
accountsDetailConfiguration.tagName ,
accountsDetailConfiguration.containerTagName ,
accountsDetailConfiguration.indexName)
locationsDataControl.setNextElement(accountDataControl)
requestTypeFound = searchRequestTypeForSelectBox()
if (requestTypeFound != null){customItemControl = initialiseCustomElement(
requestTypeFound.tagName ,
requestTypeFound.containerTagName,
requestTypeFound.indexName )
accountDataControl.setNextElement(customItemControl)
initialise4thSubItem(customItemControl)
}
}
}
function initialise4thSubItem(customItemControl){var subItemControl = searchRequestTypeForSelectBox2()
if ( subItemControl != null ){subCustomItemControl = initialiseCustomElement(
subItemControl.tagName ,
requestTypeFound.containerTagName,
subItemControl.indexName )
customItemControl.setNextElement(subCustomItemControl)
}
}
/***************************************************************
Initialize the locations drop down item (detect if available)
***************************************************************/
function initialiseLocationsDropDownItems(locationDetails){locationsDataControl = initialiseOnLoadItem(
locationDetails.tagName,
locationDetails.containerTagName,
locationDetails.indexName)
var accountsDetailConfiguration = searchClientTypeDetails()
var contactsDetailConfiguration = searchContactsDetails()
var creditNotesDetailsConfiguration = searchLocationsCreditNotesDetails()
var invoiceLinesDetails = searchLocationsInvoiceLinesDetails()
var searchLocationLicenses = searchLocationsLicensesDetails()
var searchLienseItemDetails = searchLocationsLicenseItemsDetails()
var searchOrganizations = searchOrganizationsDetails()
var searchOrganisationClientTypeDetails =
searchOrganizationClientTypesDetails()
if (accountsDetailConfiguration != null){initialiseAccountsContactsDropDownItems(locationsDataControl , accountsDetailConfiguration)
}
else if (contactsDetailConfiguration != null){initialiseAccountsContactsDropDownItems(locationsDataControl , contactsDetailConfiguration)
}
else if (creditNotesDetailsConfiguration != null){initialiseAccountsContactsDropDownItems(locationsDataControl , creditNotesDetailsConfiguration)
}
else if (invoiceLinesDetails != null){initialiseAccountsContactsDropDownItems(locationsDataControl , invoiceLinesDetails)
}
else if (searchLocationLicenses != null){initialiseAccountsContactsDropDownItems(locationsDataControl , searchLocationLicenses)
}
else if (searchLienseItemDetails != null){initialiseAccountsContactsDropDownItems(locationsDataControl , searchLienseItemDetails)
}
else if (searchOrganizations != null){initialiseAccountsContactsDropDownItems(locationsDataControl , searchOrganizations)
}
else if (searchOrganisationClientTypeDetails != null){initialiseAccountsContactsDropDownItems(locationsDataControl , searchOrganisationClientTypeDetails)
}
}
jQuery(document).ready(function () {var locationDetails = searchLocationDropDown()
var accountTypeDetails = searchAccountTypesSelectBox()
var jobTitlesDetails = searchJobTitlesSelectBox()
var contactTypesDetails = searchContactTypesSelectBox()
var groupAccountsDetails = searchGroupAccountsSelectBox()
var agreementDetails  = searchAgreementTypesSelectBox()
var buisnessTypesDetails = searchBuisnessTypesSelectBox()
var checkInTypesDetails = searchCheckInTypesSelectBox()
var checkOutTypesDetails = searchCheckOutTypesSelectBox()
var countiesDetails = searchCountiesSelectBox()
var creditCardDetails = searchCreditCardSelectBox()
var currenciesDetails = searchCurrencySelectBox()
var exchangeRateDetails = searchExchangeRateSelectBox()
var industryDetails = searchIndustrySelectBox()
var noteDetails = searchNoteSelectBox()
var onlinePlanDetails = searchOnlinePlanSelectBox()
var onlineTransactionsDetails = searchOnlineTransactionSelectBox()
var propertyTypeDetails = searchPropertyTypesSelectBox()
var qualityOfServiceDetails = searchQualityOfServiceSelectBox()
var recordTypeDetails = searchRecordTypeSelectBox()
var scheduleTypeDetails = searchScheduleTypeSelectBox()
var checkListItemDetails = searchSetupCheckListSelectBox()
var timezoneDetails = searchTimezoneSelectBox()
var townDetails = searchTownSelectBox()
var userDetails = searchUserSelectBox()
var qualityOfSpacesDetails = searchQualityOfSpacesSelectBox()
var breakDatesDetails = searchBreakDatesSelectBox()
var searchCreditNoteReasonsDetails = searchCreditNoteReasonsSelectBox()
var glCodesDetails = searchGloCodesSelectBox()
var invoiceLines2Details = searchInvoiceLines2SelectBox()
var invoiceNumbersDetails = searchInvoiceNumbersSelectBox()
var invoiceSummariesDetails = searchInvoiceSummariesSelectBox()
var	invoiceLicense2Details = searchLicenses2SelectBox()
var licenseItems = searchLicensesItemsSelectBox()
var searchLicenseSchedules = searchLicensesSchedulesSelectBox()
var searchLicenseMembers   = searchLicensesMembersSelectBox()
var searchLicenseStatus    = searchLicensesStatusSelectBox()
var searchCreditNoteLine = searchCreditNoteLineSelectBox()
var searchCreditNoteTypes = searchCreditNoteTypeSelectBox()
var searchMovementHistory = searchLocationsMovementHistoryDetails()
var searchMovementType = searchLocationsMovementTypeDetails()
var searchNonStandardTerms = searchNonStandardTermsDetails()
var searchTerminationReasons = searchTerminationReasonsDetails()
var searchTransferReasons = searchTransferReasonsDetails()
var searchRegions = searchRegionsDetails()
var searchOrganizations = searchOrganizationsDetails()
var searchContactsDetailConfiguration = searchContactsDetails()
var searchCheckListItems = searchCheckListItemsSelectBox()
if (locationDetails != null){initialiseLocationsDropDownItems(locationDetails)	}
else if (accountTypeDetails != null){accountTypeControl =
initialiseOnLoadItem(accountTypeDetails.tagName,
accountTypeDetails.containerTagName ,
accountTypeDetails.indexName)	}
else if (jobTitlesDetails != null){jobTitlesControl = initialiseOnLoadItem(jobTitlesDetails.tagName,
jobTitlesDetails.containerTagName ,
jobTitlesDetails.indexName)
}
else if (contactTypesDetails != null){contactTypesControl = initialiseOnLoadItem(contactTypesDetails.tagName,
contactTypesDetails.containerTagName ,
contactTypesDetails.indexName)
}
else if (groupAccountsDetails != null){groupAccountsControl = initialiseOnLoadItem(groupAccountsDetails.tagName,
groupAccountsDetails.containerTagName ,
groupAccountsDetails.indexName)
}
else if (agreementDetails != null){initialiseOnLoadItem(agreementDetails.tagName,
agreementDetails.containerTagName ,
agreementDetails.indexName)
}
else if (buisnessTypesDetails != null){initialiseOnLoadItem(buisnessTypesDetails.tagName,
buisnessTypesDetails.containerTagName ,
buisnessTypesDetails.indexName)
}
else if (checkInTypesDetails != null){initialiseOnLoadItem(checkInTypesDetails.tagName,
checkInTypesDetails.containerTagName ,
checkInTypesDetails.indexName)
}
else if (checkOutTypesDetails != null){initialiseOnLoadItem(checkOutTypesDetails.tagName,
checkOutTypesDetails.containerTagName ,
checkOutTypesDetails.indexName)
}
else if (countiesDetails != null){initialiseOnLoadItem(countiesDetails.tagName,
countiesDetails.containerTagName ,
countiesDetails.indexName)
}
else if (creditCardDetails != null){initialiseOnLoadItem(creditCardDetails.tagName,
creditCardDetails.containerTagName ,
creditCardDetails.indexName)
}
else if (currenciesDetails != null){initialiseOnLoadItem(currenciesDetails.tagName,
currenciesDetails.containerTagName ,
currenciesDetails.indexName)
}
else if (exchangeRateDetails != null){initialiseOnLoadItem(exchangeRateDetails.tagName,
exchangeRateDetails.containerTagName ,
exchangeRateDetails.indexName)
}
else if (industryDetails != null){initialiseOnLoadItem(industryDetails.tagName,
industryDetails.containerTagName ,
industryDetails.indexName)
}
else if (noteDetails != null){initialiseOnLoadItem(noteDetails.tagName,
noteDetails.containerTagName ,
noteDetails.indexName)
}
else if (onlinePlanDetails != null){initialiseOnLoadItem(onlinePlanDetails.tagName,
onlinePlanDetails.containerTagName ,
onlinePlanDetails.indexName)
}
else if (onlineTransactionsDetails != null){initialiseOnLoadItem(
onlineTransactionsDetails.tagName,
onlineTransactionsDetails.containerTagName ,
onlineTransactionsDetails.indexName)
}
else if (propertyTypeDetails != null){initialiseOnLoadItem(
propertyTypeDetails.tagName,
propertyTypeDetails.containerTagName ,
propertyTypeDetails.indexName)
}
else if (qualityOfServiceDetails != null){initialiseOnLoadItem(
qualityOfServiceDetails.tagName,
qualityOfServiceDetails.containerTagName ,
qualityOfServiceDetails.indexName)
}
else if (recordTypeDetails != null){initialiseOnLoadItem(
recordTypeDetails.tagName,
recordTypeDetails.containerTagName ,
recordTypeDetails.indexName)
}
else if (scheduleTypeDetails != null){initialiseOnLoadItem(
scheduleTypeDetails.tagName,
scheduleTypeDetails.containerTagName ,
scheduleTypeDetails.indexName)
}
else if (checkListItemDetails != null){initialiseOnLoadItem(
checkListItemDetails.tagName,
checkListItemDetails.containerTagName ,
checkListItemDetails.indexName)
}
else if (timezoneDetails != null){initialiseOnLoadItem(
timezoneDetails.tagName,
timezoneDetails.containerTagName ,
timezoneDetails.indexName)
}
else if (townDetails != null){initialiseOnLoadItem(
townDetails.tagName,
townDetails.containerTagName ,
townDetails.indexName)
}
else if (userDetails != null){initialiseOnLoadItem(
userDetails.tagName,
userDetails.containerTagName ,
userDetails.indexName)
}
else if (qualityOfSpacesDetails != null){initialiseOnLoadItem(
qualityOfSpacesDetails.tagName,
qualityOfSpacesDetails.containerTagName ,
qualityOfSpacesDetails.indexName)
}
else if (breakDatesDetails != null){initialiseOnLoadItem(
breakDatesDetails.tagName,
breakDatesDetails.containerTagName ,
breakDatesDetails.indexName)
}
else if (glCodesDetails != null){initialiseOnLoadItem(
glCodesDetails.tagName,
glCodesDetails.containerTagName ,
glCodesDetails.indexName)
}
else if (invoiceLines2Details != null){initialiseOnLoadItem(
invoiceLines2Details.tagName,
invoiceLines2Details.containerTagName ,
invoiceLines2Details.indexName)
}
else if (invoiceNumbersDetails != null){initialiseOnLoadItem(
invoiceNumbersDetails.tagName,
invoiceNumbersDetails.containerTagName ,
invoiceNumbersDetails.indexName)
}
else if (invoiceSummariesDetails != null){initialiseOnLoadItem(
invoiceSummariesDetails.tagName,
invoiceSummariesDetails.containerTagName ,
invoiceSummariesDetails.indexName)
}
else if( invoiceLicense2Details != null ){initialiseOnLoadItem(
invoiceLicense2Details.tagName,
invoiceLicense2Details.containerTagName ,
invoiceLicense2Details.indexName)
}
else if( licenseItems != null ){initialiseOnLoadItem(
licenseItems.tagName,
licenseItems.containerTagName ,
licenseItems.indexName)
}
else if( searchLicenseSchedules != null ){initialiseOnLoadItem(
searchLicenseSchedules.tagName,
searchLicenseSchedules.containerTagName ,
searchLicenseSchedules.indexName)
}
else if( searchLicenseMembers != null ){initialiseOnLoadItem(
searchLicenseMembers.tagName,
searchLicenseMembers.containerTagName ,
searchLicenseMembers.indexName)
}
else if( searchLicenseStatus != null ){initialiseOnLoadItem(
searchLicenseStatus.tagName,
searchLicenseStatus.containerTagName ,
searchLicenseStatus.indexName)
}
else if( searchCreditNoteLine != null ){initialiseOnLoadItem(
searchCreditNoteLine.tagName,
searchCreditNoteLine.containerTagName ,
searchCreditNoteLine.indexName)
}
else if(searchCreditNoteTypes != null){initialiseOnLoadItem(
searchCreditNoteTypes.tagName,
searchCreditNoteTypes.containerTagName ,
searchCreditNoteTypes.indexName)
}
else if(searchMovementHistory != null){initialiseOnLoadItem(
searchMovementHistory.tagName,
searchMovementHistory.containerTagName ,
searchMovementHistory.indexName)
}
else if(searchMovementType != null){initialiseOnLoadItem(
searchMovementType.tagName,
searchMovementType.containerTagName ,
searchMovementType.indexName)
}
else if(searchNonStandardTerms != null){initialiseOnLoadItem(
searchNonStandardTerms.tagName,
searchNonStandardTerms.containerTagName ,
searchNonStandardTerms.indexName)
}
else if(searchTerminationReasons != null){initialiseOnLoadItem(
searchTerminationReasons.tagName,
searchTerminationReasons.containerTagName ,
searchTerminationReasons.indexName)
}
else if(searchTransferReasons != null){initialiseOnLoadItem(
searchTransferReasons.tagName,
searchTransferReasons.containerTagName ,
searchTransferReasons.indexName)
}
else if(searchRegions != null){initialiseOnLoadItem(
searchRegions.tagName,
searchRegions.containerTagName ,
searchRegions.indexName)
}
else if(searchOrganizations != null){initialiseOnLoadItem(
searchOrganizations.tagName,
searchOrganizations.containerTagName ,
searchOrganizations.indexName)
}
else if(searchContactsDetailConfiguration != null){initialiseOnLoadItem(
searchContactsDetailConfiguration.tagName,
searchContactsDetailConfiguration.containerTagName ,
searchContactsDetailConfiguration.indexName)
}
else if(searchCreditNoteReasonsDetails != null){initialiseOnLoadItem(
searchCreditNoteReasonsDetails.tagName,
searchCreditNoteReasonsDetails.containerTagName ,
searchCreditNoteReasonsDetails.indexName)
}
else if(searchCheckListItems != null){initialiseOnLoadItem(
searchCheckListItems.tagName,
searchCheckListItems.containerTagName ,
searchCheckListItems.indexName)
}
if (window.setupTesting != undefined){setupTesting()
}
});/************************************************************************
Example Interface Functions - For use as part of the deliveried change.
*************************************************************************/
var listItemListResponseData = null;var listItemDataResponseData = null;var requestURLLastUsed = null;var requestActionCode = null
var onClickElement = null
var onClickElementValue =  null
var onclickActionCode = null
function onClickEventAction(elementCode, itemValue, actionCode){onClickElement = elementCode
onClickElementValue = itemValue
onclickActionCode = actionCode
}
function onChangeEventItemListResponse(data , requestURL , actionCode){listItemListResponseData = data
requestURLLastUsed = requestURL
requestActionCode = actionCode
}
function onChangeEventItemDataResponse(data, requestURL , actionCode){listItemDataResponseData = data
requestURLLastUsed = requestURL
requestActionCode = actionCode
}
var currentRequestInProgress = false
function getToken(){return '5jeV4vt9q3h3YdWDwdm3jzAXv7QR2pNWWbCJLN86tf5DPyqs6kAErFdjnCqATgQx';}
/********************************************************
Function to handle the requests to and from the network.
Parameters:	RequestObject Representing the request.
requestObject.requestEndPoint - End Point for the request.
requestObject.completionCallBack - Completion Callback function.
********************************************************/
function networkRequestHander(requestObject){if (requestObject.requestEndPoint == undefined){throw "Request Type Unknown";}
else if (requestObject.completionCallBack == undefined ||
(typeof requestObject.completionCallBack) != "function"){throw "Callback completion is not a function";}
console.log("Request: "+requestObject.requestEndPoint)
currentRequestInProgress = true
jQuery.ajax({method: "GET",
dataType :"json",
url: requestObject.requestEndPoint,
headers: {"Authorization":getToken()},
success: function(data) {requestObject.completionCallBack(data , requestObject.requestEndPoint)
currentRequestInProgress = false
},
error: function(error) {console.error( JSON.stringify(error) )
console.error("Get Request to "+requestObject.requestEndPoint+" was unsuccessful")
currentRequestInProgress = false
}
})
}
/*************************************************
Initialize and Load Data for the page.
**************************************************/
function initialiseData(controlBlock){if (controlBlock == undefined){throw 'Input Parameters must be provided to call the function initialiseData';}
if ( !(this instanceof initialiseData) ){throw 'initialiseData needs to be initialised as an Object with it\'s own scope.';}
/************************************
If the select div is null - flag this.
************************************/
var selectContainerDiv = null
if (controlBlock.containerSelectDivID != null){selectContainerDiv = '#'+controlBlock.containerSelectDivID;}
var selectItem = controlBlock.selectItemID;var initializationLoading = controlBlock.loadOnInitialisation;var appendDirectory = controlBlock.appendDirectory;var selectText = controlBlock.selectText;var downStreamObject = null;var upStreamObject = null;var itemInitialised = false;var lastEndPointUsed = '';var previousDirectoryItem = null;var parentLocationValue = null;var patentEndPointUsed = null;var responseDataCallbackHandler = null;if (controlBlock.responseDataCallback != null &&
(typeof controlBlock.responseDataCallback) == "function"){responseDataCallbackHandler = controlBlock.responseDataCallback;}
var responseListCallbackHandler = null
if (controlBlock.responseListCallback != null &&
(typeof controlBlock.responseListCallback) == "function"){responseListCallbackHandler = controlBlock.responseListCallback;}
var selectedItemCallbackHandler = null
if (controlBlock.initialClickCallBack != null &&
(typeof controlBlock.initialClickCallBack) == "function"){selectedItemCallbackHandler = controlBlock.initialClickCallBack;}
var locationLoaded = function(responseItem , requestURL){if (responseItem instanceof Array){jQuery('#'+selectItem).empty();jQuery('#'+selectItem).append( jQuery('<option>', {value: '',text:selectText}) );for (var count = 0 ; count < responseItem.length ; count++ ){var itemElement = responseItem[count]
jQuery('#'+selectItem).append(
jQuery('<option>', {value: itemElement.id,
text : itemElement.name
})
)
}
/*******************************
In the event that a response callback procedure
is provided then pass the payload to the response item.
********************************/
if (responseListCallbackHandler != null){responseListCallbackHandler(responseItem , requestURL , controlBlock.appendDirectory);}
}else{console.error("Response Data is not an array.");}
}
/*****************************
Function to load the corresponding element.
******************************/
var loadElement = function( adjustedLocation , locationPredone ){if (adjustedLocation == undefined){if (controlBlock.locationEndPoint != null){if (controlBlock.locationEndPoint.substr(controlBlock.locationEndPoint.length-1,1) == '/'){lastEndPointUsed = controlBlock.locationEndPoint + appendDirectory;adjustedLocation = controlBlock.locationEndPoint + appendDirectory;}else{lastEndPointUsed = controlBlock.locationEndPoint + "/" + appendDirectory;adjustedLocation = controlBlock.locationEndPoint + "/" + appendDirectory;}
}
else{throw 'No known location';}
}else{if (locationPredone == undefined){adjustedLocation = adjustedLocation +"/" +appendDirectory;}
}
/***********************************************
Initialize the loading items.
(Well, create an empty box and place the loading text inside it.)
If the container ID is provided use this item
***********************************************/
if (selectContainerDiv != null &&
jQuery(selectContainerDiv).length > -1){jQuery(selectContainerDiv).empty();jQuery(selectContainerDiv).append(
jQuery('<select>', {id: selectItem})
);jQuery('#'+selectItem).append(
jQuery('<option>', {value: "",text : controlBlock.loadingText})
);networkRequestHander({requestEndPoint:adjustedLocation,
completionCallBack:locationLoaded	});jQuery('#'+selectItem).change(onChangeFunction);}
/*********************************************
Load up the next item in the sequence.
**********************************************/
else if (selectItem != null &&
jQuery('#'+selectItem).length > 0 ){jQuery('#'+selectItem).empty();jQuery('#'+selectItem).append(	jQuery('<option>', {value: "",text : controlBlock.loadingText})	);networkRequestHander({requestEndPoint:adjustedLocation,
completionCallBack:locationLoaded	});jQuery('#'+selectItem).off("change");jQuery('#'+selectItem).change(onChangeFunction);}
else if (selectContainerDiv == null && selectItem != null){var concatDiv = "#"+selectItem + "Div";jQuery(concatDiv).empty();jQuery(concatDiv).append(	jQuery('<select>', {id: selectItem}) );jQuery(concatDiv).append(	jQuery('<option>', {value: "",text : controlBlock.loadingText})	);networkRequestHander({requestEndPoint:adjustedLocation,
completionCallBack:locationLoaded	});jQuery(concatDiv).change(onChangeFunction);}
itemInitialised = true;}
var unloadElement = function(){jQuery('#'+selectItem).off("change");jQuery('#'+selectItem).empty();}
/***************************************
Make a data request to load the form details.
****************************************/
var makeDataRequest = function(endPoint){function dataCompletionCallback(data , requestURL){if (data instanceof Object){if (responseDataCallbackHandler != null){responseDataCallbackHandler(data ,requestURL , controlBlock.appendDirectory);}
}
}
networkRequestHander({requestEndPoint:endPoint,
completionCallBack:dataCompletionCallback	});}
/************************************
Function for the on-change event.
************************************/
var onChangeFunction = function(){var selectedItemValue = encodeURIComponent(jQuery('#'+selectItem).val());if (selectedItemCallbackHandler != null){selectedItemCallbackHandler('#'+selectItem,selectedItemValue,
controlBlock.appendDirectory );}
if (downStreamObject != null){if (downStreamObject instanceof initialiseData){if (jQuery('#'+selectItem).val() == null){downStreamObject.undoItemRequest();return;}
if (lastEndPointUsed.length > 0){if (selectedItemValue.length > 0){downStreamObject.undoItemRequest();downStreamObject.requestChangeItem(
lastEndPointUsed+'/'+selectedItemValue,
controlBlock.appendDirectory+'/'+selectedItemValue,
selectedItemValue);}
else{downStreamObject.undoItemRequest();}
}else{if (selectedItemValue.length){downStreamObject.undoItemRequest();downStreamObject.requestChangeItem(
patentEndPointUsed+'/'+
controlBlock.appendDirectory+
'/'+selectedItemValue,
controlBlock.appendDirectory+
'/'+selectedItemValue,
selectedItemValue
);}else{downStreamObject.undoItemRequest();}
}
}
}
else{if (jQuery('#'+selectItem).val() == null){return;}
if (jQuery('#'+selectItem).val().length == 0){return;}
if (lastEndPointUsed.length > 0){makeDataRequest(lastEndPointUsed+'/'+selectedItemValue);}else{var directoryLocationNames = Array("invoices","accounts","licences","invoiceLines","creditNotes","contacts","accountCode","clientTypes","invoiceSummaries","accountCodes");if (directoryLocationNames.indexOf(controlBlock.appendDirectory) > -1)
{makeDataRequest(controlBlock.locationEndPoint +
controlBlock.appendDirectory+'/'+
selectedItemValue);}
else{makeDataRequest(
patentEndPointUsed+'/'+
controlBlock.appendDirectory+
'/'+selectedItemValue);}
}
}
}
if (initializationLoading){loadElement();}
/***************************************************************************
External Functions.
Functions defined in the following section are for interaction outside
of the object.
***************************************************************************/
/***********************************
In the event that there is more than one element in a line
then allow this object to know about that object,
actions (initialize and unselected items can occur.)
***********************************/
this.setNextElement = function(nextElement){if ( !(nextElement instanceof initialiseData ) ){throw 'setNextElement is not an instance of initialiseData';}
downStreamObject = nextElement;}
/***********************************
Set the response callback function.
This is for handling the data payloads as a result of the request/response.
***********************************/
this.setResponseDataCallBack = function(callbackFunction){if (callbackFunction == undefined ||
(typeof callbackFunction) != "function"){console.error("setResponseDataCallBack - Function not passed in"+callbackFunction );}
else{responseDataCallbackHandler = callbackFunction;}
}
/***********************************
Set the response item for handling request/responses.
***********************************/
this.setResponseDataListCallback = function(callbackFunction){if (callbackFunction == undefined ||
(typeof callbackFunction) != "function"){console.error("setResponseDataListCallback - Function not passed in"+callbackFunction );}
else{responseListCallbackHandler = callbackFunction;}
}
/***********************************
Item is used use for linking the initialiseData objects
together so that multiple lists can be linked together.
Pass in the requested item. (Well, (set/change) the item.
The end point should be passed from the parent item.
************************************/
this.requestChangeItem = function(inputLastEndPointUsed , relativeItemPath, itemValueInput){if (controlBlock.locationEndPoint != null){var appendDirectoryItems = Array("accountTypes","licenceItems");if (appendDirectoryItems.indexOf(
controlBlock.appendDirectory)>-1){patentEndPointUsed = controlBlock.locationEndPoint + controlBlock.appendDirectory+"/";loadElement( patentEndPointUsed , true );}
else{patentEndPointUsed = controlBlock.locationEndPoint+relativeItemPath;loadElement( patentEndPointUsed  );}
}else{patentEndPointUsed = inputLastEndPointUsed;loadElement( patentEndPointUsed  );}
}
this.undoItemRequest = function(){unloadElement();if (downStreamObject != null){downStreamObject.undoItemRequest();}
}
}
/************************************
Search for the locations drop-down items.
*************************************/
function searchLocationDropDown(){if (jQuery('select[id=locationSelect]').length > 0){return {tagName: 'locationSelect' ,
containerTagName: 'locationSelectDiv',
indexName: 'locations'};		}
else if (jQuery('#locationSelectDiv').length > 0){return {tagName: 'locationSelect' ,
containerTagName: 'locationSelectDiv',
indexName: 'locations'};		}
else if (jQuery('select[id=organization2Select]').length > 0){return {tagName: 'organization2Select' ,
containerTagName: 'organization2SelectDiv',
indexName: 'organizations'};		}
else if (jQuery('#organization2SelectDiv').length > 0){return {tagName: 'organization2Select' ,
containerTagName: 'organization2SelectDiv',
indexName: 'organizations'};		}
return null
}
/***************************************
Search to see if a 'client/customer' drop down box is available
*****************************************/
function searchClientTypeDetails(){if (jQuery('select[id=clientSelect]').length > 0){return {tagName: 'clientSelect' ,
containerTagName: 'clientSelectDiv' ,
indexName: 'accounts'};	}
if (jQuery('#clientSelectDiv').length > 0){return {tagName: 'clientSelect' ,
containerTagName: 'clientSelectDiv' ,
indexName: 'accounts'};	}
return null
}
/***************************************
Search to see if a 'contact' drop down box is available.
this should be under the location.
*****************************************/
function searchContactsDetails(){if (jQuery('select[id=contactsSelect]').length > 0){return {tagName: 'contactsSelect' ,
containerTagName: 'contactsSelectDiv' ,
indexName: 'contacts'};	}
if (jQuery('#contactsSelectDiv').length > 0){return {tagName: 'contactsSelect' ,
containerTagName: 'contactsSelectDiv' ,
indexName: 'contacts'};	}
return null
}
/***************************************
Search to see if a 'locationsCreditNotesSelect' drop down box is available.
this should be under the location.
*****************************************/
function searchLocationsCreditNotesDetails(){if (jQuery('select[id=locationsCreditNotesSelect]').length > 0){return {tagName: 'locationsCreditNotesSelect' ,
containerTagName: 'locationsCreditNotesSelectDiv' ,
indexName: 'creditNotes'};	}
if (jQuery('#locationsCreditNotesSelectDiv').length > 0){return {tagName: 'locationsCreditNotesSelect' ,
containerTagName: 'locationsCreditNotesSelectDiv' ,
indexName: 'creditNotes'};	}
return null
}
/***************************************
Search to see if a 'locationsInvoiceLinesSelect' drop down box is available.
this should be under the location.
*****************************************/
function searchLocationsInvoiceLinesDetails(){if (jQuery('select[id=locationsInvoiceLinesSelect]').length > 0){return {tagName: 'locationsInvoiceLinesSelect' ,
containerTagName: 'locationsInvoiceLinesSelectDiv' ,
indexName: 'invoiceLines'};	}
if (jQuery('#locationsInvoiceLinesSelectDiv').length > 0){return {tagName: 'locationsInvoiceLinesSelect' ,
containerTagName: 'locationsInvoiceLinesSelectDiv' ,
indexName: 'invoiceLines'};	}
return null
}
/***************************************
Search to see if a 'locationLicensesSelect' drop down box is available.
this should be under the location.
*****************************************/
function searchLocationsLicensesDetails(){if (jQuery('select[id=locationLicensesSelect]').length > 0){return {tagName: 'locationLicencesSelect' ,
containerTagName: 'locationLicencesSelectDiv' ,
indexName: 'licences'};	}
if (jQuery('#locationLicencesSelectDiv').length > 0){return {tagName: 'locationLicencesSelect' ,
containerTagName: 'locationLicencesSelectDiv' ,
indexName: 'licences'};	}
return null
}
/***************************************
Search to see if a 'assignedClientTypes' drop down box is available.
this should be under the location.
*****************************************/
function searchLocationsContactsDetails(){if (jQuery('select[id=assignedClientTypesSelect]').length > 0){return {tagName: 'assignedClientTypesSelect' ,
containerTagName: 'assignedClientTypesSelectDiv' ,
indexName: 'assignedClientTypes'};	}
if (jQuery('#assignedClientTypesSelectDiv').length > 0){return {tagName: 'assignedClientTypesSelect' ,
containerTagName: 'assignedClientTypesSelectDiv' ,
indexName: 'assignedClientTypes'};	}
return null
}
function searchLocationsLicenseItemsDetails(){if (jQuery('select[id=licenceItemsSelect]').length > 0){return {tagName: 'licenceItemsSelect' ,
containerTagName: 'licenceItemsSelectDiv' ,
indexName: 'licenceItems'};	}
if (jQuery('#licenceItemsSelectDiv').length > 0){return {tagName: 'licenceItemsSelect' ,
containerTagName: 'licenceItemsSelectDiv' ,
indexName: 'licenceItems'};	}
return null
}
/***************************************
For a generic text box try to search out these
on the screen. - This is for something at the third level in the
structure.
*****************************************/
function searchRequestTypeForSelectBox(){if (jQuery('select[id=invoicesSelect]').length > 0){return {tagName: 'invoicesSelect' ,
containerTagName: 'invoicesSelectDiv' ,
indexName: 'invoices'};	}
else if (jQuery('#invoicesSelectDiv').length > 0){return {tagName: 'invoicesSelect' ,
containerTagName: 'invoicesSelectDiv' ,
indexName: 'invoices'};	}
else if (jQuery('#invoiceSelectDiv').length > 0){return {tagName: 'invoicesSelect' ,
containerTagName: 'invoiceSelectDiv' ,
indexName: 'invoices'};	}
else if(jQuery('select[id=accountCodeSelect]').length > 0){return {tagName: 'accountCodeSelect' ,
containerTagName: 'accountCodeSelectDiv' ,
indexName: 'accountCodes'};	}
else if(jQuery('#accountCodeSelectDiv').length > 0){return {tagName: 'accountCodeSelect' ,
containerTagName: 'accountCodeSelectDiv' ,
indexName: 'accountCodes'};	}
else if(jQuery('select[id=accountTypesSelect]').length > 0){return {tagName: 'accountTypesSelect' ,
containerTagName: 'accountTypesSelectDiv' ,
indexName: 'accountTypes'};	}
else if(jQuery('#accountTypesSelectDiv').length > 0){return {tagName: 'accountTypesSelect' ,
containerTagName: 'accountTypesSelectDiv' ,
indexName: 'accountTypes'};	}
else if(jQuery('select[id=accountContactsSelect]').length > 0){return {tagName: 'accountContactsSelect' ,
containerTagName: 'accountContactsSelectDiv' ,
indexName: 'contacts'};	}
else if(jQuery('#accountContactsSelectDiv').length > 0){return {tagName: 'accountContactsSelect' ,
containerTagName: 'accountContactsSelectDiv' ,
indexName: 'contacts'};	}
else if(jQuery('select[id=creditNotesSelect]').length > 0){return {tagName: 'creditNotesSelect' ,
containerTagName: 'creditNotesSelectDiv' ,
indexName: 'creditNotes'};	}
else if(jQuery('#creditNotesSelectDiv').length > 0){return {tagName: 'creditNotesSelect' ,
containerTagName: 'creditNotesSelectDiv' ,
indexName: 'creditNotes'};	}
else if(jQuery('select[id=invoiceLinesSelect]').length > 0){return {tagName: 'invoiceLinesSelect' ,
containerTagName: 'invoiceLinesSelectDiv' ,
indexName: 'invoiceLines'};	}
else if(jQuery('#invoiceLinesSelectDiv').length > 0){return {tagName: 'invoiceLinesSelect' ,
containerTagName: 'invoiceLinesSelectDiv' ,
indexName: 'invoiceLines'};	}
else if(jQuery('select[id=licencesSelect]').length > 0){return {tagName: 'licencesSelect' ,
containerTagName: 'licencesSelectDiv' ,
indexName: 'licences'};	}
else if(jQuery('licencesSelectDiv').length > 0){return {tagName: 'licencesSelect' ,
containerTagName: 'licencesSelectDiv' ,
indexName: 'licences'};	}
else if(jQuery('select[id=licenceItemsSelect]').length > 0){return {tagName: 'licenceItemsSelect' ,
containerTagName: 'licenceItemsSelectDiv' ,
indexName: 'licenceItems'};	}
else if(jQuery('licenceItemsSelectDiv').length > 0){return {tagName: 'licenceItemsSelect' ,
containerTagName: 'licenceItemsSelectDiv' ,
indexName: 'licenceItems'};	}
return null;}
function searchRequestTypeForSelectBox2(){if (jQuery('select[id=invoiceLinesSelect]').length > 0){return {tagName: 'invoiceLinesSelect' ,
containerTagName: 'invoiceLinesSelectDiv' ,
indexName: 'invoiceLines'};	}
else if (jQuery('#invoiceLinesSelectDiv').length > 0){return {tagName: 'invoiceLinesSelect' ,
containerTagName: 'invoiceLinesSelectDiv' ,
indexName: 'invoiceLines'};	}
else if (jQuery('select[id=invoiceSummariesSelect]').length > 0){return {tagName: 'invoiceSummariesSelect' ,
containerTagName: 'invoiceSummariesSelectDiv' ,
indexName: 'invoiceSummaries'};	}
else if (jQuery('#invoiceSummariesSelectDiv').length > 0){return {tagName: 'invoiceSummariesSelect' ,
containerTagName: 'invoiceSummariesSelectDiv' ,
indexName: 'invoiceSummaries'};	}
else if (jQuery('select[id=licenceMembersSelect]').length > 0){return {tagName: 'licenceMembersSelect' ,
containerTagName: 'licenceMembersSelectDiv' ,
indexName: 'licenceMembers'};	}
else if (jQuery('#licenceMembersSelectDiv').length > 0){return {tagName: 'licenceMembersSelect' ,
containerTagName: 'licenceMembersSelectDiv' ,
indexName: 'licenceMembers'};	}
else if (jQuery('select[id=licenceItemSchedulesSelect]').length > 0){return {tagName: 'licenceItemSchedulesSelect' ,
containerTagName: 'licenceItemSchedulesSelectDiv' ,
indexName: 'licenceItemSchedules'};	}
else if (jQuery('#licenceItemSchedulesSelectDiv').length > 0){return {tagName: 'licenceItemSchedulesSelect' ,
containerTagName: 'licenceItemSchedulesSelectDiv' ,
indexName: 'licenceItemSchedules'};	}
return null;}
/**************************************
Search the account types drop down item.
***************************************/
function searchAccountTypesSelectBox(){if (jQuery('select[id=accountTypesSelect]').length > 0){return {tagName: 'accountTypesSelect' ,
containerTagName: 'accountTypesSelectDiv' ,
indexName: 'accountTypes'};	}
if (jQuery('#accountTypesSelectDiv').length > 0){return {tagName: 'accountTypesSelect' ,
containerTagName: 'accountTypesSelectDiv' ,
indexName: 'accountTypes'};	}
return null;}
/**************************************
Search the break dates drop down item.
***************************************/
function searchBreakDatesSelectBox(){if (jQuery('select[id=breakDatesSelect]').length > 0){return {tagName: 'breakDatesSelect' ,
containerTagName: 'breakDatesSelectDiv' ,
indexName: 'breakDates'};	}
if (jQuery('#breakDatesSelectDiv').length > 0){return {tagName: 'breakDatesSelect' ,
containerTagName: 'breakDatesSelectDiv' ,
indexName: 'breakDates'};	}
return null;}
/**************************************
Search the check list drop down item.
***************************************/
function searchCheckListItemsSelectBox(){if (jQuery('select[id=setupChecklistsSelect]').length > 0){return {tagName: 'setupChecklistsSelect' ,
containerTagName: 'setupChecklistsSelectDiv' ,
indexName: 'checkListItems'};	}
if (jQuery('#setupChecklistsSelectDiv').length > 0){return {tagName: 'setupChecklistsSelect' ,
containerTagName: 'setupChecklistsSelectDiv' ,
indexName: 'checkListItems'};	}
return null;}
/**************************************
Search the account types drop down item.
***************************************/
function searchJobTitlesSelectBox(){if (jQuery('select[id=jobTitlesSelect]').length > 0){return {tagName: 'jobTitlesSelect' ,
containerTagName: 'jobTitlesSelectDiv' ,
indexName: 'jobTitles'};	}
if (jQuery('#jobTitlesSelectDiv').length > 0){return {tagName: 'jobTitlesSelect' ,
containerTagName: 'jobTitlesSelectDiv' ,
indexName: 'jobTitles'};	}
return null;}
/**************************************
Search the contact types drop down item.
***************************************/
function searchContactTypesSelectBox(){if (jQuery('select[id=contactTypesSelect]').length > 0){return {tagName: 'contactTypesSelect' ,
containerTagName: 'contactTypesSelectDiv' ,
indexName: 'contactTypes'};	}
if (jQuery('#contactTypesSelectDiv').length > 0){return {tagName: 'contactTypesSelect' ,
containerTagName: 'contactTypesSelectDiv' ,
indexName: 'contactTypes'};	}
return null;}
/**************************************
Search the group accounts drop down item.
***************************************/
function searchGroupAccountsSelectBox(){if (jQuery('select[id=groupAccountsSelect]').length > 0){return {tagName: 'groupAccountsSelect' ,
containerTagName: 'groupAccountsSelectDiv' ,
indexName: 'groupAccounts'};	}
if (jQuery('#groupAccountsSelectDiv').length > 0){return {tagName: 'groupAccountsSelect' ,
containerTagName: 'groupAccountsSelectDiv' ,
indexName: 'groupAccounts'};	}
return null;}
/**************************************
Search the group agreements types drop down item.
***************************************/
function searchAgreementTypesSelectBox(){if (jQuery('select[id=agreementTypesSelect]').length > 0){return {tagName: 'agreementTypesSelect' ,
containerTagName: 'agreementTypesSelectDiv' ,
indexName: 'agreementTypes'};	}
if (jQuery('#agreementTypesSelectDiv').length > 0){return {tagName: 'agreementTypesSelect' ,
containerTagName: 'agreementTypesSelectDiv' ,
indexName: 'agreementTypes'};	}
return null;}
/**************************************
Search the group agreements types drop down item.
***************************************/
function searchBuisnessTypesSelectBox(){if (jQuery('select[id=businessTypesSelect]').length > 0){return {tagName: 'businessTypesSelect' ,
containerTagName: 'businessTypesSelectDiv' ,
indexName: 'businessTypes'};	}
if (jQuery('#businessTypesSelectDiv').length > 0){return {tagName: 'businessTypesSelect' ,
containerTagName: 'businessTypesSelectDiv' ,
indexName: 'businessTypes'};	}
return null;}
/**************************************
Search the group check in types drop down item.
***************************************/
function searchCheckInTypesSelectBox(){if (jQuery('select[id=checkInTypesSelect]').length > 0){return {tagName: 'checkInTypesSelect' ,
containerTagName: 'checkInTypesSelectDiv' ,
indexName: 'checkInTypes'};	}
if (jQuery('#checkInTypesSelectDiv').length > 0){return {tagName: 'checkInTypesSelect' ,
containerTagName: 'checkInTypesSelectDiv' ,
indexName: 'checkInTypes'};	}
return null;}
/**************************************
Search the group check out types drop down item.
***************************************/
function searchCheckOutTypesSelectBox(){if (jQuery('select[id=checkOutTypesSelect]').length > 0){return {tagName: 'checkOutTypesSelect' ,
containerTagName: 'checkOutTypesSelectDiv' ,
indexName: 'checkOutTypes'};	}
if (jQuery('#checkOutTypesSelectDiv').length > 0){return {tagName: 'checkOutTypesSelect' ,
containerTagName: 'checkOutTypesSelectDiv' ,
indexName: 'checkOutTypes'};	}
return null;}
/**************************************
Search the group counties drop down item.
***************************************/
function searchCountiesSelectBox(){if (jQuery('select[id=countiesSelect]').length > 0){return {tagName: 'countiesSelect' ,
containerTagName: 'countiesSelectDiv' ,
indexName: 'counties'};	}
if (jQuery('#countiesSelectDiv').length > 0){return {tagName: 'countiesSelect' ,
containerTagName: 'countiesSelectDiv' ,
indexName: 'counties'};	}
return null;}
/**************************************
Search the group credit card drop down item.
***************************************/
function searchCreditCardSelectBox(){if (jQuery('select[id=creditCardsSelect]').length > 0){return {tagName: 'creditCardsSelect' ,
containerTagName: 'creditCardsSelectDiv' ,
indexName: 'creditCards'};	}
if (jQuery('#creditCardsSelectDiv').length > 0){return {tagName: 'creditCardsSelect' ,
containerTagName: 'creditCardsSelectDiv' ,
indexName: 'creditCards'};	}
return null;}
/**************************************
Search the group currency drop down item.
***************************************/
function searchCurrencySelectBox(){if (jQuery('select[id=currenciesSelect]').length > 0){return {tagName: 'currenciesSelect' ,
containerTagName: 'currenciesSelectDiv' ,
indexName: 'currencies'};	}
if (jQuery('#currenciesSelectDiv').length > 0){return {tagName: 'currenciesSelect' ,
containerTagName: 'currenciesSelectDiv' ,
indexName: 'currencies'};	}
return null;}
/**************************************
Search the group exchange rate drop down item.
***************************************/
function searchExchangeRateSelectBox(){if (jQuery('select[id=exchangeRatesSelect]').length > 0){return {tagName: 'exchangeRatesSelect' ,
containerTagName: 'exchangeRatesSelectDiv' ,
indexName: 'exchangeRates'};	}
if (jQuery('#exchangeRatesSelectDiv').length > 0){return {tagName: 'exchangeRatesSelect' ,
containerTagName: 'exchangeRatesSelectDiv' ,
indexName: 'exchangeRates'};	}
return null;}
/**************************************
Search the group industry drop down item.
***************************************/
function searchIndustrySelectBox(){if (jQuery('select[id=industriesSelect]').length > 0){return {tagName: 'industriesSelect' ,
containerTagName: 'industriesSelectDiv' ,
indexName: 'industries'};	}
if (jQuery('#industriesSelectDiv').length > 0){return {tagName: 'industriesSelect' ,
containerTagName: 'industriesSelectDiv' ,
indexName: 'industries'};	}
return null;}
/**************************************
Search the group note drop down item.
***************************************/
function searchNoteSelectBox(){if (jQuery('select[id=noteSelect]').length > 0){return {tagName: 'noteSelect' ,
containerTagName: 'noteSelectDiv' ,
indexName: 'notes'};	}
if (jQuery('#noteSelectDiv').length > 0){return {tagName: 'noteSelect' ,
containerTagName: 'noteSelectDiv' ,
indexName: 'notes'};	}
return null;}
/**************************************
Search the group on-line plan drop down item.
***************************************/
function searchOnlinePlanSelectBox(){if (jQuery('select[id=onlinePlansSelect]').length > 0){return {tagName: 'onlinePlansSelect' ,
containerTagName: 'onlinePlansSelectDiv' ,
indexName: 'onlinePlans'};	}
if (jQuery('#onlinePlansSelectDiv').length > 0){return {tagName: 'onlinePlansSelect' ,
containerTagName: 'onlinePlansSelectDiv' ,
indexName: 'onlinePlans'};	}
return null;}
/**************************************
Search the group on-transaction plan drop down item.
***************************************/
function searchOnlineTransactionSelectBox(){if (jQuery('select[id=onlineTransactionsSelect]').length > 0){return {tagName: 'onlineTransactionsSelect' ,
containerTagName: 'onlineTransactionsSelectDiv' ,
indexName: 'onlineTransactions'};	}
if (jQuery('#onlineTransactionsSelectDiv').length > 0){return {tagName: 'onlineTransactionsSelect' ,
containerTagName: 'onlineTransactionsSelectDiv' ,
indexName: 'onlineTransactions'};	}
return null;}
/**************************************
Search the group property types drop down item.
***************************************/
function searchPropertyTypesSelectBox(){if (jQuery('select[id=propertyTypesSelect]').length > 0){return {tagName: 'propertyTypesSelect' ,
containerTagName: 'propertyTypesSelectDiv' ,
indexName: 'propertyTypes'};	}
if (jQuery('#propertyTypesSelectDiv').length > 0){return {tagName: 'propertyTypesSelect' ,
containerTagName: 'propertyTypesSelectDiv' ,
indexName: 'propertyTypes'};	}
return null;}
/**************************************
Search the group quality of service drop down item.
***************************************/
function searchQualityOfServiceSelectBox(){if (jQuery('select[id=qualityOfServiceSelect]').length > 0){return {tagName: 'qualityOfServiceSelect' ,
containerTagName: 'qualityOfServiceSelectDiv' ,
indexName: 'qualityOfService'};	}
if (jQuery('#qualityOfServiceSelectDiv').length > 0){return {tagName: 'qualityOfServiceSelect' ,
containerTagName: 'qualityOfServiceSelectDiv' ,
indexName: 'qualityOfService'};	}
return null;}
/**************************************
Search the group record type drop down item.
***************************************/
function searchRecordTypeSelectBox(){if (jQuery('select[id=recordTypesSelect]').length > 0){return {tagName: 'recordTypesSelect' ,
containerTagName: 'recordTypesSelectDiv' ,
indexName: 'recordTypes'};	}
if (jQuery('#recordTypesSelectDiv').length > 0){return {tagName: 'recordTypesSelect' ,
containerTagName: 'recordTypesSelectDiv' ,
indexName: 'recordTypes'};	}
return null;}
/**************************************
Search the group schedule type drop down item.
***************************************/
function searchScheduleTypeSelectBox(){if (jQuery('select[id=scheduleTypesSelect]').length > 0){return {tagName: 'scheduleTypesSelect' ,
containerTagName: 'scheduleTypesSelectDiv' ,
indexName: 'scheduleTypes'};	}
if (jQuery('#scheduleTypesSelectDiv').length > 0){return {tagName: 'scheduleTypesSelect' ,
containerTagName: 'scheduleTypesSelectDiv' ,
indexName: 'scheduleTypes'};	}
return null;}
/**************************************
Search the group setup check list type drop down item.
***************************************/
function searchSetupCheckListSelectBox(){if (jQuery('select[id=checkListItemsSelect]').length > 0){return {tagName: 'checkListItemsSelect' ,
containerTagName: 'checkListItemsSelectDiv' ,
indexName: 'checkListItems'};	}
if (jQuery('#checkListItemsSelectDiv').length > 0){return {tagName: 'checkListItemsSelect' ,
containerTagName: 'checkListItemsSelectDiv' ,
indexName: 'checkListItems'};	}
return null;}
/**************************************
Search the group timezone type drop down item.
***************************************/
function searchTimezoneSelectBox(){if (jQuery('select[id=timezonesSelect]').length > 0){return {tagName: 'timezonesSelect' ,
containerTagName: 'timezonesSelectDiv' ,
indexName: 'timezones'};	}
if (jQuery('#timezonesSelectDiv').length > 0){return {tagName: 'timezonesSelect' ,
containerTagName: 'timezonesSelectDiv' ,
indexName: 'timezones'};	}
return null;}
/**************************************
Search the group town drop down item.
***************************************/
function searchTownSelectBox(){if (jQuery('select[id=townsSelect]').length > 0){return {tagName: 'townsSelect' ,
containerTagName: 'townsSelectDiv' ,
indexName: 'towns'};	}
if (jQuery('#townsSelectDiv').length > 0){return {tagName: 'townsSelect' ,
containerTagName: 'townsSelectDiv' ,
indexName: 'towns'};	}
return null;}
/**************************************
Search the group user drop down item.
***************************************/
function searchUserSelectBox(){if (jQuery('select[id=usersSelect]').length > 0){return {tagName: 'usersSelect' ,
containerTagName: 'usersSelectDiv' ,
indexName: 'users'};	}
if (jQuery('#usersSelectDiv').length > 0){return {tagName: 'usersSelect' ,
containerTagName: 'usersSelectDiv' ,
indexName: 'users'};	}
return null;}
/**************************************
Search the group quality of spaces drop down item.
***************************************/
function searchQualityOfSpacesSelectBox(){if (jQuery('select[id=qualityOfSpacesSelect]').length > 0){return {tagName: 'qualityOfSpacesSelect' ,
containerTagName: 'qualityOfSpacesSelectDiv' ,
indexName: 'qualityOfSpaces'};	}
if (jQuery('#qualityOfSpacesSelectDiv').length > 0){return {tagName: 'qualityOfSpacesSelect' ,
containerTagName: 'qualityOfSpacesSelectDiv' ,
indexName: 'qualityOfSpaces'};	}
return null;}
/**************************************
Search the client type drop down item.
***************************************/
function searchCreditNoteReasonsSelectBox(){if (jQuery('select[id=creditNoteReasonSelect]').length > 0){return {tagName: 'creditNoteReasonSelect' ,
containerTagName: 'creditNoteReasonSelectDiv' ,
indexName: 'creditNoteReasons'};	}
if (jQuery('#creditNoteReasonSelectDiv').length > 0){return {tagName: 'creditNoteReasonSelect' ,
containerTagName: 'creditNoteReasonSelectDiv' ,
indexName: 'creditNoteReasons'};	}
return null;}
/**************************************
Search the GL Code drop down item.
***************************************/
function searchGloCodesSelectBox(){if (jQuery('select[id=glCodesSelect]').length > 0){return {tagName: 'glCodesSelect' ,
containerTagName: 'glCodesSelectDiv' ,
indexName: 'glCodes'};	}
if (jQuery('#glCodesSelectDiv').length > 0){return {tagName: 'glCodesSelect' ,
containerTagName: 'glCodesSelectDiv' ,
indexName: 'glCodes'};	}
return null;}
/**************************************
Search the GL Code drop down item.
***************************************/
function searchInvoiceLines2SelectBox(){if (jQuery('select[id=invoiceLines2Select]').length > 0){return {tagName: 'invoiceLines2Select' ,
containerTagName: 'invoiceLines2SelectDiv' ,
indexName: 'invoiceLines'};	}
if (jQuery('#invoiceLines2SelectDiv').length > 0){return {tagName: 'invoiceLines2Select' ,
containerTagName: 'invoiceLines2SelectDiv' ,
indexName: 'invoiceLines'};	}
return null;}
/**************************************
Search the Invoice Numbers drop down item.
***************************************/
function searchInvoiceNumbersSelectBox(){if (jQuery('select[id=invoiceNumbersSelect]').length > 0){return {tagName: 'invoiceNumbersSelect' ,
containerTagName: 'invoiceNumbersSelectDiv' ,
indexName: 'invoiceNumbers'};	}
if (jQuery('#invoiceNumbersSelectDiv').length > 0){return {tagName: 'invoiceNumbersSelect' ,
containerTagName: 'invoiceNumbersSelectDiv' ,
indexName: 'invoiceNumbers'};	}
return null;}
/**************************************
Search the Invoice Summary drop down item.
***************************************/
function searchInvoiceSummariesSelectBox(){if (jQuery('select[id=invoiceSummariesSelect]').length > 0){return {tagName: 'invoiceSummariesSelect' ,
containerTagName: 'invoiceSummariesSelectDiv' ,
indexName: 'invoiceSummaries'};	}
if (jQuery('#invoiceSummariesSelectDiv').length > 0){return {tagName: 'invoiceSummariesSelect' ,
containerTagName: 'invoiceSummariesSelectDiv' ,
indexName: 'invoiceSummaries'};	}
return null;}
/**************************************
Search the License Check drop down item.
***************************************/
function searchInvoiceSummariesSelectBox(){if (jQuery('select[id=licenceCheckListItemsSelect]').length > 0){return {tagName: 'licenceCheckListItemsSelect' ,
containerTagName: 'licenceCheckListItemsSelectDiv' ,
indexName: 'licenceCheckListItems'};	}
if (jQuery('#licenceCheckListItemsSelectDiv').length > 0){return {tagName: 'licenceCheckListItemsSelect' ,
containerTagName: 'licenceCheckListItemsSelectDiv' ,
indexName: 'licenceCheckListItems'};	}
return null;}
/**************************************
Search the GL Code drop down item.
***************************************/
function searchLicenses2SelectBox(){if (jQuery('select[id=licenses2Select]').length > 0){return {tagName: 'licenses2Select' ,
containerTagName: 'licenses2SelectDiv' ,
indexName: 'licences'};	}
if (jQuery('#licenses2SelectDiv').length > 0){return {tagName: 'licenses2Select' ,
containerTagName: 'licenses2SelectDiv' ,
indexName: 'licences'};	}
return null;}
/**************************************
Search the GL Code drop down item.
***************************************/
function searchLicensesItemsSelectBox(){if (jQuery('select[id=licenceItemsSelect]').length > 0){return {tagName: 'licenceItemsSelect' ,
containerTagName: 'licenceItemsSelectDiv' ,
indexName: 'licenceItems'};	}
if (jQuery('#licenceItemsSelectDiv').length > 0){return {tagName: 'licenceItemsSelect' ,
containerTagName: 'licenceItemsSelectDiv' ,
indexName: 'licenceItems'};	}
return null;}
/**************************************
Search the License Schedules drop down item.
***************************************/
function searchLicensesSchedulesSelectBox(){if (jQuery('select[id=licenceItemSchedulesSelect]').length > 0){return {tagName: 'licenceItemSchedulesSelect' ,
containerTagName: 'licenceItemSchedulesSelectDiv' ,
indexName: 'licenceItemSchedules'};	}
if (jQuery('#licenceItemSchedulesSelectDiv').length > 0){return {tagName: 'licenceItemSchedulesSelect' ,
containerTagName: 'licenceItemSchedulesSelectDiv' ,
indexName: 'licenceItemSchedules'};	}
return null;}
/**************************************
Search the License Schedules drop down item.
***************************************/
function searchLicensesMembersSelectBox(){if (jQuery('select[id=licenceMembersSelect]').length > 0){return {tagName: 'licenceMembersSelect' ,
containerTagName: 'licenceMembersSelectDiv' ,
indexName: 'licenceMembers'};	}
if (jQuery('#licenceMembersSelectDiv').length > 0){return {tagName: 'licenceMembersSelect' ,
containerTagName: 'licenceMembersSelectDiv' ,
indexName: 'licenceMembers'};	}
return null;}
/**************************************
Search the License Schedules drop down item.
***************************************/
function searchLicensesStatusSelectBox(){if (jQuery('select[id=licenceStatusesSelect]').length > 0){return {tagName: 'licenceStatusesSelect' ,
containerTagName: 'licenceStatusesSelectDiv' ,
indexName: 'licenceStatuses'};	}
if (jQuery('#licenceStatusesSelectDiv').length > 0){return {tagName: 'licenceStatusesSelect' ,
containerTagName: 'licenceStatusesSelectDiv' ,
indexName: 'licenceStatuses'};	}
return null;}
/**************************************
Search the License Schedules drop down item.
***************************************/
function searchCreditNoteLineSelectBox(){if (jQuery('select[id=creditNoteLineSelect]').length > 0){return {tagName: 'creditNoteLineSelect' ,
containerTagName: 'creditNoteLineSelectDiv' ,
indexName: 'creditNoteLines'};	}
if (jQuery('#creditNoteLineSelectDiv').length > 0){return {tagName: 'creditNoteLineSelect' ,
containerTagName: 'creditNoteLineSelectDiv' ,
indexName: 'creditNoteLines'};	}
return null;}
/**************************************
Search the License Schedules drop down item.
***************************************/
function searchCreditNoteTypeSelectBox(){if (jQuery('select[id=creditNoteTypesSelect]').length > 0){return {tagName: 'creditNoteTypesSelect' ,
containerTagName: 'creditNoteTypesSelectDiv' ,
indexName: 'creditNoteTypes'};	}
if (jQuery('#creditNoteTypesSelectDiv').length > 0){return {tagName: 'creditNoteTypesSelect' ,
containerTagName: 'creditNoteTypesSelectDiv' ,
indexName: 'creditNoteTypes'};	}
return null;}
/***************************************
Search to see if a 'movementHistorySelect' drop down box is available.
this should be under the location.
*****************************************/
function searchLocationsMovementHistoryDetails(){if (jQuery('select[id=movementHistorySelect]').length > 0){return {tagName: 'movementHistorySelect' ,
containerTagName: 'movementHistorySelectDiv' ,
indexName: 'movementHistory'};	}
if (jQuery('#movementHistorySelectDiv').length > 0){return {tagName: 'movementHistorySelect' ,
containerTagName: 'movementHistorySelectDiv' ,
indexName: 'movementHistory'};	}
return null;}
/***************************************
Search to see if a 'movementHistorySelect' drop down box is available.
this should be under the location.
*****************************************/
function searchLocationsMovementTypeDetails(){if (jQuery('select[id=movementTypeSelect]').length > 0){return {tagName: 'movementTypeSelect' ,
containerTagName: 'movementTypeSelectDiv' ,
indexName: 'movementTypes'};	}
if (jQuery('#movementTypeSelectDiv').length > 0){return {tagName: 'movementTypeSelect' ,
containerTagName: 'movementTypeSelectDiv' ,
indexName: 'movementTypes'};	}
return null;}
/***************************************
Search to see if a 'nonStandardTermsSelect' drop down box is available.
this should be under the location.
*****************************************/
function searchNonStandardTermsDetails(){if (jQuery('select[id=nonStandardTermsSelect]').length > 0){return {tagName: 'nonStandardTermsSelect' ,
containerTagName: 'nonStandardTermsSelectDiv' ,
indexName: 'nonStandardTerms'};	}
if (jQuery('#nonStandardTermsSelectDiv').length > 0){return {tagName: 'nonStandardTermsSelect' ,
containerTagName: 'nonStandardTermsSelectDiv' ,
indexName: 'nonStandardTerms'};	}
return null;}
/***************************************
Search to see if a 'terminationReasonsSelect' drop down box is available.
this should be under the location.
*****************************************/
function searchTerminationReasonsDetails(){if (jQuery('select[id=terminationReasonsSelect]').length > 0){return {tagName: 'terminationReasonsSelect' ,
containerTagName: 'terminationReasonsSelectDiv' ,
indexName: 'terminationReasons'};	}
if (jQuery('#terminationReasonsSelectDiv').length > 0){return {tagName: 'terminationReasonsSelect' ,
containerTagName: 'terminationReasonsSelectDiv' ,
indexName: 'terminationReasons'};	}
return null;}
/***************************************
Search to see if a 'terminationReasonsSelect' drop down box is available.
this should be under the location.
*****************************************/
function searchTransferReasonsDetails(){if (jQuery('select[id=transferReasonsSelect]').length > 0){return {tagName: 'transferReasonsSelect' ,
containerTagName: 'transferReasonsSelectDiv' ,
indexName: 'transferReasons'};	}
if (jQuery('#transferReasonsSelectDiv').length > 0){return {tagName: 'transferReasonsSelect' ,
containerTagName: 'transferReasonsSelectDiv' ,
indexName: 'transferReasons'};	}
return null;}
/***************************************
Search to see if a 'regionsSelect' drop down box is available.
this should be under the location.
*****************************************/
function searchRegionsDetails(){if (jQuery('select[id=regionsSelect]').length > 0){return {tagName: 'regionsSelect' ,
containerTagName: 'regionsSelectDiv' ,
indexName: 'regions'};	}
if (jQuery('#regionsSelectDiv').length > 0){return {tagName: 'regionsSelect' ,
containerTagName: 'regionsSelectDiv' ,
indexName: 'regions'};	}
return null;}
/***************************************
Search to see if a 'organizationSelect' drop down box is available.
this should be under the location.
*****************************************/
function searchOrganizationsDetails(){if (jQuery('select[id=organizationSelect]').length > 0){return {tagName: 'organizationSelect' ,
containerTagName: 'organizationSelectDiv' ,
indexName: 'organizations'};	}
if (jQuery('#organizationSelectDiv').length > 0){return {tagName: 'organizationSelect' ,
containerTagName: 'organizationSelectDiv' ,
indexName: 'organizations'};	}
return null;}
/***************************************
Search to see if a 'organizationSelect' drop down box is available.
this should be under the location.
*****************************************/
function searchOrganizationClientTypesDetails(){if (jQuery('select[id=clientTypesSelect]').length > 0){return {tagName: 'clientTypesSelect' ,
containerTagName: 'clientTypesSelectDiv' ,
indexName: 'clientTypes'};	}
if (jQuery('#clientTypesSelectDiv').length > 0){return {tagName: 'clientTypesSelect' ,
containerTagName: 'clientTypesSelectDiv' ,
indexName: 'clientTypes'};	}
return null;}
